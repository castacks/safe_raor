a_max = 1.0;
v_max = 10.0;
s_res = 0.05;
s = 7.5;
v0 = 3.0;
vf = 3.0;
min_time = 1.5;
close all
%[ t_vec, a_vec, v_vec, s_vec ] = bang_bang_double_integrator(s, v0, vf, a_max, v_max, s_res, min_time);
%[ t_vec1, a_vec1, v_vec1, s_vec1 ] = bang_bang_maxtime_double_integrator(s/2,v0,vf,a_max,v_max,s_res, min_time);
[ t_vec, a_vec, v_vec, s_vec ] = optimize_turning_point(s,v0,min_time,s/2,vf,min_time,vf,a_max,v_max,s_res);
plot(s_vec,v_vec);
%hold on
%plot(s_vec1,v_vec1);
%legend('min_time','max_time')
%disp('min_time, max_time')
%disp(t_vec(end))
%disp(t_vec1(end))


%lets list the cases
%Case 1 when the two points are close enough that it will take longer to
%turn in thr path driection and then turn to end directon. The end point is
%known

%Case2 Above point but the end point is unknown.

%Case 3 The end point is known but the complete turning might take less of a time, not sure.

%Case 4 The end point is unknown.

%case 5 When end point is unknown or far enough away to consider turning,
%we need to find out what velocity at the end are we looking for and what
%accelerations should we travel at.

%The turning point tells us what velocity is allowed at its absolute max
% We need to minimize time then.
%After turning point there is a fixed minimum time and before turning point there is a fixed minimum time.
% There is a constant eml boundary for s_dot.
% We have to find the controller type, the acceleration and v_tp and v_eml
% We should turn as fast one can(bang-bang), if one discovers that is not sufficient,
% then we have to start slowing down. (change acc limits, but first check
% the max_time on the same acceleration limit) and if max time on max
% acceleration limit does not work, decrease final speed to the lowest
% allowed value to see if it works.
%What is the lowest allowed value? 
% The lowest allowed value is the value that enables the other transition
% in the time we discuss. It could be anything less than stopping distance
% velocity, if just constraints are to be met.

% Now the velocity governing algorithm is to make sure that time is
% possible. We will reach this state knowing its possible to maintain the
% lower bound on time.
% Keep it slow enough so that time constraint is met.
% Since constant acceleration algorithms are used some of the 


% step 1 keep deccelerating and see if you can make it 




% there might be a time way to do this but lets table it for a second.
% so if the special case arise where there is not time for even maximum
% time bang bang controller
% then we for loop over velocities
% when s_dot touches zero, find the acceleration for which time constriant
% is met.
% Once one time constraint is met, only then go to the other constraint