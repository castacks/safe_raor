function [ t_vec, a_vec, v_vec, s_vec ] = bang_bang_maxtime_double_integrator(s,v0,vf,a_max,v_max,s_res, min_time)
%BANG_BANG_DOUBLE_INTEGRATOR Summary of this function goes here
stopping_distance = v0*v0/(2*a_max);
max_time = 0.;
if(stopping_distance < s)
    max_time = 100000;
else
    final_speed = sqrt(v0*v0 - 2*a_max*s);
    max_time = (v0 - final_speed)/a_max;
end
if(max_time < min_time)
    error(strcat('Infeasible. min time (', num2str(min_time),') is greate than max time ',num2str(max_time)));
end
s_vec = 0:s_res:s;
%fprintf('1. Size of s_vec = %d, s=%.2f. \n', size(s_vec))
if s<s_res
    s_vec = 0:s;
end

if s_vec(end) ~= s
    s_vec = [s_vec,s];
end

t_vec = [0]; a_vec = [0]; v_vec = [0];
% Step 0
% v_constraint_
a_res = 0.2;
for a = a_res:a_res:a_max
    display(size(s_vec))
    % Step 1, back integration
    v_vec = real(sqrt((vf*vf)-(2*a*(s-s_vec))));
    v_vec = max(0,v_vec);
    % Step 2,3 forward_integration
    v_vec1 = real(sqrt((v0*v0)-(2*a*s_vec)));
    v_vec1 = max(0, v_vec1);
    % Step 4, finding intersections between forward and backward integration
    merge_index=0;
    for i=1:length(v_vec1)
       if and(abs(v_vec1(i)-v_vec(i)) <= 0.2, ((v_vec1(i)-v_vec(i)) <= 0.0))  
           merge_index = i;
           break;
       end
    end
    if merge_index==0
        lowest_feasible_vf = num2str(real(sqrt(v0*v0 - 2*a*s)));
        highest_feasible_vf = num2str(real(sqrt(v0*v0 + 2*a*s)));
        vf_str = num2str(vf);
        disp(strcat('MaxTime:Not feasible. vf:', vf_str,'should be between:[',lowest_feasible_vf,',',highest_feasible_vf,'].'));
        continue;
    end
    if(v_vec1(merge_index) > 0.)
        % Step 5 - Merging
        display(merge_index);
        display(s_vec);
        v_vec = [v_vec1(1:merge_index),v_vec((merge_index+1):end)];
        [ t_vec, a_vec] = time_acceleration(s_vec,v_vec);
        if t_vec(end) >= min_time
            break;
        end
    else
        fprintf('Reached a zero speed, at acceleration:%.2f, distance:%.2f \n',a,s_vec(merge_index));
        time_epsilon = 0.5;
        time_taken = 0.;
        while abs(time_taken-min_time)>time_epsilon
            % Step 5 - handling 0 velocity case      
            dist_left = s-s_vec(merge_index); % Step1 find the 0 velocity while going forward
            [t_vec1, a_vec1] = time_acceleration(s_vec(1:merge_index),v_vec1(1:merge_index));
            min_time_temp = min_time - t_vec1(end);
            [ t_vec2, a_vec2, v_vec2, s_vec2 ] = bang_bang_double_integrator(dist_left, v_vec1(merge_index),vf, a_max, v_max, s_res, min_time_temp); % Step2 find a time optimal bang bang controller            
            t_vec2 = t_vec1(end) + t_vec2;
            time_taken = t_vec2(end);
            fprintf('1. Time t1:%.2f, t2:%.2f, time_taken:%.2f, min_time:%.2f \n',t_vec1(end),t_vec2(end),time_taken,min_time);
            if (time_taken < min_time)  && (v_vec1(merge_index) == 0.)
                t_vec2 = (min_time - time_taken) + t_vec2;
                time_taken = t_vec2(end);
            end
            fprintf('2. Time t1:%.2f, t2:%.2f, time_taken:%.2f, min_time:%.2f \n',t_vec1(end),t_vec2(end),time_taken,min_time);
            if abs(time_taken-min_time)<=time_epsilon
                t_vec = [t_vec1, t_vec2(2:end)];
                a_vec = [a_vec1(1:(end-1)), a_vec2];
                s_vec = [s_vec(1:merge_index),(s_vec2(2:end)+s_vec(merge_index+1))];
                v_vec = [v_vec1(1:merge_index),v_vec2(2:end)];
                time_taken = t_vec(end);                
            end
            fprintf('3. Time t1:%.2f, t2:%.2f, time_taken:%.2f, min_time:%.2f \n',t_vec1(end),t_vec2(end),time_taken,min_time);
            merge_index = merge_index - 1;            
        end
        break;
    end
end
    
end