function [ t_vec, a_vec] = time_acceleration(s_vec,v_vec)
if(length(s_vec) < 2)
    t_vec = 0; a_vec = 0;
end
diff_s = diff(s_vec);
u_vec = v_vec(1:end-1);
v_vec = v_vec(2:end);
a_vec = (v_vec.*v_vec - u_vec.*u_vec)./(2*diff_s);
t_vec = (v_vec - u_vec)./a_vec;
t_vec(a_vec==0) = diff_s(a_vec==0)./u_vec(a_vec==0);
for i=2:length(t_vec)
    if isnan(t_vec(i))
        t_vec(i) = t_vec(i-1);
    end
end
a_vec(end+1) = 0;
t_vec = cumsum([0,t_vec]);
end