function [ t_vec, a_vec, v_vec, s_vec ] = bang_bang_double_integrator(s,v0,vf,a_max,v_max,s_res, min_time)
%BANG_BANG_DOUBLE_INTEGRATOR Summary of this function goes here
stopping_distance = v0*v0/(2*a_max);
t_vec = [0]; a_vec = [0]; v_vec = [0];
max_time = 0.;
if(stopping_distance < s)
    max_time = 100000;
else
    final_speed = sqrt(v0*v0 - 2*a_max*s);
    max_time = (v0 - final_speed)/a_max;
end
if(max_time < min_time)
    display(strcat('Bro ... this is not feasible. min time should be greater than ',num2str(max_time)));
    t_vec = [0]; a_vec = [0]; v_vec = [0]; s_vec = [0];
    return;
end

s_vec = 0:s_res:s;
if s<s_res
    s_vec = 0:s;
end

if s_vec(end) ~= s
    s_vec = [s_vec,s];
end

% Step 0
% v_constraint_
% Step 1, back integration
a_res = 0.05;
for a = a_max:-a_res:0
    v_vec_temp = sqrt((vf*vf)+(2*a*(s-s_vec)));
    v_vec_temp = min(v_max,v_vec_temp);
    % Step 2,3 forward_integration
    v_vec1 = sqrt((v0*v0)+(2*a*s_vec));
    v_vec1 = min(v_max, v_vec1);
    % Step 4, finding intersections between forward and backward integration
    merge_index=0;
    for i=1:length(v_vec1)
       if and(abs(v_vec1(i)-v_vec_temp(i)) <= 0.1, (v_vec1(i)-v_vec_temp(i) >= 0.0))  
           merge_index = i;
           break;
       end
    end
    if merge_index==0
        lowest_feasible_vf = num2str(real(sqrt(v0*v0 - 2*a*s)));
        highest_feasible_vf = num2str(real(sqrt(v0*v0 + 2*a*s)));
        vf_str = num2str(vf);
        disp(strcat('MinTime:Not feasible. vf:', vf_str,'should be between:[',lowest_feasible_vf,',',highest_feasible_vf,'].'));
        return;
    end
    % Step 5 - Merging
    v_vec = [v_vec1(1:merge_index),v_vec_temp((merge_index+1):end)];
    [ t_vec, a_vec] = time_acceleration(s_vec,v_vec);
    fprintf('Time taken:%.2f, Min time:%.2f, Acceleration:%.2f \n', t_vec(end), min_time,a);    
    if (t_vec(end)-min_time)>0
        break;
    end
end
% this may not report the closest but that is what it should do.
end