function [ t_vec, a_vec, v_vec, s_vec ] = optimize_turning_point(s,v0,min_time_tp,s_tp,v_tp,min_time_f,vf,a_max,v_max,s_res)
% Checking if bang bang works to get the optimal time
init_v_tp = v_tp;
init_v_f = vf;
% say the min max velocities
v_tp_min = 0.2;
vf_min = 0.2;
v_res = 0.2;
t_vec_tp=[]; a_vec_tp=[]; v_vec_tp=[]; s_vec_tp =[];
t_vec_f=[]; a_vec_f=[]; v_vec_f=[]; s_vec_f=[];
t_vec = []; a_vec= []; v_vec = []; s_vec = [];
%find the limits
% if we dont find the limits, there will be watesd cycles, let there be
% wasted cycles.
found_v_tp = 0; found_v_f = 0;
for  v_tp = init_v_tp:-v_res:v_tp_min
    % check for feasibility here
    found_v_tp = 0; found_v_f = 0;
    fprintf('VTP1 \n');
    [ t_vec_tp, a_vec_tp, v_vec_tp, s_vec_tp ] = bang_bang_double_integrator(s_tp,v0,v_tp,a_max,v_max,s_res, min_time_tp);
    fprintf('VTP1 After bang bang min time v_tp, v_tp:%.2f, time taken:%2f, min_time:%2f \n',v_tp,t_vec_tp(end),min_time_tp);
    if (t_vec_tp(end) < min_time_tp)
        disp('Oh no, optimal trajectory TP is not feasible, lets try the max time bang for the same velocity. \n')
        fprintf('VTP2 \n');
        [ t_vec_tp, a_vec_tp, v_vec_tp, s_vec_tp ] = bang_bang_maxtime_double_integrator(s_tp,v0,v_tp,a_max,v_max,s_res, min_time_tp);
        fprintf('VTP2 After bang bang max time v_tp, v_tp:%.2f, time taken:%2f, min_time:%2f \n',v_tp,t_vec_tp(end),min_time_tp);

        if(t_vec_tp(end) >= min_time_tp)
            found_v_tp = 1;
        end
    else
        found_v_tp = 1;
    end
    if found_v_tp == 1
        for v_f = init_v_f:-v_res:vf_min
            %check for feasibility here
            [ t_vec_f, a_vec_f, v_vec_f, s_vec_f ] = bang_bang_double_integrator((s-s_tp),v_vec_tp(end),vf,a_max,v_max,s_res, min_time_f);
            fprintf('VF1 After bang bang min time v_f:%.2f, v_tp:%.2f, time taken:%2f, min_time:%2f \n',vf,v_vec_tp(end),t_vec_f(end),min_time_f);
            if t_vec_f(end) < min_time_f
                disp('Oh no, optimal trajectory  is not feasible, lets try the max time bang for the same velocity.')
                [ t_vec_f, a_vec_f, v_vec_f, s_vec_f ] = bang_bang_maxtime_double_integrator((s-s_tp),v_vec_tp(end),vf,a_max,v_max,s_res, min_time_f);
                fprintf('VF2 After bang bang min time v_f:%.2f, v_tp:%.2f, time taken:%2f, min_time:%2f \n',vf,v_tp,t_vec_tp(end),min_time_f);
                if(t_vec_f(end) >= min_time_f)
                    found_v_f = 1;
                    break;
                end
            else
                found_v_f = 1;
                break;
            end
        end
    end
    if found_v_tp==1 && found_v_f==1
        break;
    end
end
if found_v_tp==1 && found_v_f==1
    t_vec_f = t_vec_f(2:end) + t_vec_tp(end);
    s_vec_f = s_vec_f(2:end) + s_vec_tp(end);
    t_vec = [t_vec_tp,t_vec_f];
    a_vec = [a_vec_tp(1:(end-1)),a_vec_f];
    v_vec = [v_vec_tp,v_vec_f(2:end)];
    s_vec = [s_vec_tp,s_vec_f];
end
end
