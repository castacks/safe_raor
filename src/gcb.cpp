#include "safe_raor/gcb.h"
#include "ros/ros.h"

ca::GeneralizedCostBenefit::GeneralizedCostBenefit(SafeRAORViewPointOnPolarGridInterface *vg, std::vector<SafeRAORViewPoint> & vp_list):
    path_(vp_list),
    global_vp_list_(vp_list)
{
    vg_ = vg;
}

void ca::GeneralizedCostBenefit::SetupAndSolveTsp(std::vector<size_t> &node_order, size_t start_location_id, size_t end_node_id){
    // start_location_id and end_location_id are global_vp_lists
    // setup the D matrix according to start and end
    int start_id, end_id, fake_id;

    for(int i=0; i<node_order.size();i++){
        if(node_order[i] == start_location_id)
            start_id = i;
        if(node_order[i] == end_node_id)
            end_id = i;
    }    
    fake_id = node_order.size();
    //ROS_INFO_STREAM("Setting up D!::"<<node_order.size()<<"::"<<start_id<<"::"<<end_id);
    Eigen::MatrixXd D = DMatrix(node_order, start_id, end_id);
    //ROS_INFO_STREAM("Initializing!");
    tsp_.Initialize(D);
    std::vector<size_t> new_order;
    //ROS_INFO_STREAM("Solving!");
    tsp_.Solve(new_order); //New Order is the index of node_order, so are start_id and end_id
    //ROS_INFO_STREAM("New Order--");
    //for(size_t i=0; i< new_order.size(); i++){
    //    ROS_INFO_STREAM(new_order[i]<<"-->");
    //}
    //ROS_INFO_STREAM("Fixing Order!");
    std::vector<size_t> new_order1 = FixOrder(new_order, start_id, end_id, fake_id);
    path_.ResetOrder(new_order1);

}

std::vector<size_t> ca::GeneralizedCostBenefit::FixOrder(std::vector<size_t> &order, int start_id, int end_id, int fake_id){
    int s_id,e_id, f_id;
    //ROS_INFO_STREAM("TSP order::start_id::end_id::fake_id::"<<start_id<<"::"<<end_id<<"::"<<fake_id);
    for(size_t i=0; i< order.size();i++)
    {
        //ROS_INFO_STREAM(order[i]<<"-->");
        if(order[i] == start_id)
            s_id = i;

        if(order[i] == end_id)
            e_id = i;

        if(order[i] == fake_id)
            f_id = i;

    }

    std::vector<size_t> new_order;
    if(f_id == 0 || f_id==(order.size()-1)){
        if(s_id < e_id){
            for(int i=s_id; i<= (int)e_id; i++){
                new_order.push_back(order[i]);
            }
        }
        else{
            for(int i=s_id; i>=(int)e_id; i--){
                new_order.push_back(order[i]);
            }
        }
    }
    else{

        if(s_id < e_id){
            for(int i=s_id; i>=0; i--){
                new_order.push_back(order[i]);
            }
            for(int i=order.size()-1; i>=e_id; i--){
                new_order.push_back(order[i]);
            }
        }
        else{
            for(int i=s_id; i<order.size(); i++){
                new_order.push_back(order[i]);
            }
            for(int i=0; i<=e_id; i++){
                new_order.push_back(order[i]);
            }
        }
    }

    return new_order;
}

Eigen::MatrixXd ca::GeneralizedCostBenefit::DMatrix(std::vector<size_t> &points, int start_id, int end_id){
    Eigen::MatrixXd D((points.size()+1),(points.size()+1));
    //ROS_INFO_STREAM("Allocated D");
    size_t fake_id = points.size();    
    double distance;
    Eigen::Vector3d p1,p2;
     for(size_t i=0; i<D.rows();i++){
        if(i<points.size()){
            ca::ViewPoint& vp = global_vp_list_[points[i]];
            p1 = vp.vp_pose_.position_;}
        for(size_t j=0; j<D.cols();j++){ // This can be
            if(i==fake_id || j==fake_id){
                D(i,j) = 1000000.0;
                D(j,i) = 1000000.0;
            }
            else{
                ca::ViewPoint& vp = global_vp_list_[points[j]];
                p2 = vp.vp_pose_.position_;
                distance = (p1-p2).norm();
                D(i,j) =distance;
            }
        }
        D(i,i) = 0;
    }

    D(fake_id,start_id) = 0.0000001;
    D(start_id,fake_id) = 0.0000001;
    D(fake_id,end_id) = 0.0000001;
    D(end_id,fake_id) = 0.0000001;
    return D;
}

ca::SafeRAORPath ca::GeneralizedCostBenefit::GCB(Eigen::Vector3d& start_point, double start_heading, double start_fov,
        size_t end_node_id, ca_ri::Polar_2D_Representation* ri, double budget){    //add the start location to the
    path_.path_.clear();
    path_.setRepresentationState(ri);

    double range = 20;
    ca::SafeRAORViewPoint vp;
            vg_->MakeViewPoint(vp,start_point,start_heading,start_fov,range);
                    //MakeViewPoint(vp,start_point,start_heading,start_fov);
    global_vp_list_.push_back(vp);


    start_location_id_ = global_vp_list_.size()-1;
    path_.setRepresentationState(ri);
    path_.AddNode(start_location_id_,true);
    path_.AddNode(end_node_id,true);
    ROS_ERROR_STREAM("List Size2::"<<global_vp_list_.size()<<" Path nodes::"<<path_.path_.size());
    size_t dummy;
    double distance, reward;
    double benefit, max_benefit;
    double budget_left = budget - path_.CalculatePathLength(path_.path_,global_vp_list_);
    bool found_admissible = true;
    double total_reward=0;
    double new_reward = 0;

    while (budget_left>0 && found_admissible){
        ROS_ERROR_STREAM("In while::"<<path_.path_.size());
        max_benefit = 0.0;
        size_t best_id =0;
        found_admissible = false;
        for(size_t i=0; i<global_vp_list_.size();i++){
            if(!path_.IsInPath(i,dummy)){
                distance =
                    path_.CostOfAdding(path_.path_,global_vp_list_,
                                       global_vp_list_[i].vp_pose_.position_);
                distance = std::max(0.1,distance);
                //if(global_vp_list_[i].target_set_){
                //    distance = distance + M_2PI*target_radius_;
                //    ROS_ERROR_STREAM("Distance::"<<distance<<"::Budget Left::"<<budget_left);
                //}
                reward = reward_.RewardFunction(i,global_vp_list_,path_.ri_);
                ROS_ERROR_STREAM(i<<"::P::"<<global_vp_list_[i].vp_pose_.position_.x()<<
                                 "::"<<global_vp_list_[i].vp_pose_.position_.y()<<"::"<<global_vp_list_[i].vp_pose_.position_.z());
                benefit = reward/distance;
                ROS_ERROR_STREAM("R::"<<reward<<" Distance::"<<distance<<" Benefit::"<<benefit<<" BudgetL::"<<budget_left
                                 <<" MBenefit::"<<max_benefit<<"::"<<(int)(benefit > max_benefit)<<(int)(distance < budget_left));
                ca::ViewPoint &vp = global_vp_list_[i];
                for(size_t kk=0; kk < vp.visible_primitives_.size(); kk++){
                    ROS_ERROR_STREAM("Saw this::"<<vp.visible_primitives_[kk]);
                }
                if(benefit > max_benefit && distance < budget_left){
                    best_id = i;
                    found_admissible = true;
                    max_benefit = benefit;
                    new_reward = reward;
                }

            }
        }
        ROS_ERROR("Outside For:");
        if(found_admissible){
            ROS_ERROR_STREAM("\n\nFound a Node!\n\n");
            path_.AddNode(best_id,true);
            ROS_ERROR_STREAM("Added Node::"<<best_id);
            ROS_ERROR_STREAM("About to solve TSP");
            SetupAndSolveTsp(path_.path_,start_location_id_,end_node_id);
            total_reward = total_reward + new_reward;
            ROS_INFO_STREAM("Solved TSP");
        }

        budget_left = budget - path_.CalculatePathLength(path_.path_,global_vp_list_);
        ROS_ERROR_STREAM("Budget::"<<budget_left<<" Found::"<<(int)found_admissible);
    }

    for(size_t i=0; i<global_vp_list_.size();i++){
        ca::ViewPoint& vp =  global_vp_list_[i];
        Eigen::Vector3d & v = vp.vp_pose_.position_;
        double heading = vp.vp_pose_.heading_;
        ROS_INFO_STREAM("VP list3::"<<v.x()<<"::"<<v.y()<<"::"<<v.z()<<"::"<<heading);
    }

    ROS_ERROR_STREAM("Budget::"<<budget<<"Reward::"<<total_reward);
    return path_;
}


visualization_msgs::MarkerArray ca::GeneralizedCostBenefit::GetMarkers(ca_ri::Polar_2D_Representation* ri)
{
    visualization_msgs::MarkerArray ma;
    visualization_msgs::Marker sector = ri->GetSectors(0);
    ma.markers.push_back(sector);
    visualization_msgs::Marker cyl = ri->GetCylinder(0);
    ma.markers.push_back(cyl);

    ca::ViewPoint vp;

    visualization_msgs::Marker m, m_lines;
    m.header.frame_id = ri->get_frame();
    //ROS_INFO_STREAM("Frame::"<<ri->get_frame());
    m.header.stamp = ros::Time();
    m.ns = "points";
    m.frame_locked = true;
    m.id = 0;
    m.type = visualization_msgs::Marker::SPHERE_LIST;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = 0;
    m.pose.position.y = 0;
    m.pose.position.z = 0;
    m.pose.orientation.x = 0.0;
    m.pose.orientation.y = 0.0;
    m.pose.orientation.z = 0.0;
    m.pose.orientation.w = 1.0;
    m.scale.x = 1;
    m.scale.y = 1;
    m.scale.z = 1;
    m.color.a = 1.0; // Don't forget to set the alpha!
    m.color.r = 1.0;
    m.color.g = 1.0;
    m.color.b = 1.0;

    m_lines = m;
    m_lines.type = visualization_msgs::Marker::LINE_LIST;
    m_lines.ns = "lines";
    m_lines.scale.x = 0.2;
    std_msgs::ColorRGBA c_blue; c_blue.r=0; c_blue.b=1.0; c_blue.g=0; c_blue.a = 1.0;
    std_msgs::ColorRGBA c_green; c_green.r = 0; c_green.b = 0; c_green.g = 1.0; c_green.a = 1.0;
    geometry_msgs::Point p;
    double line_length = 2.0;
    //Eigen::Vector3d unit_vec(1,0,0);
    for(size_t i=0; i<global_vp_list_.size();i++){
        ca::ViewPoint& vp = global_vp_list_[i];
        Eigen::Vector3d pos; float fov; float heading;
        vp.get_pose(pos,fov,heading);
        geometry_msgs::Point p;
        p.x =pos.x() ; p.y = pos.y(); p.z = pos.z();
        m.points.push_back(p);

        m_lines.points.push_back(p);
        p.x = p.x + line_length*cos(heading);
        p.y = p.y + line_length*sin(heading);
        p.z = p.z;
        m_lines.points.push_back(p);
        if(vp.visible_primitives_.size()>0){
            m.colors.push_back(c_green);
            m_lines.colors.push_back(c_green);
            m_lines.colors.push_back(c_green);
        }
        else{
            m.colors.push_back(c_blue);
            m_lines.colors.push_back(c_blue);
            m_lines.colors.push_back(c_blue);
        }
    }
    ma.markers.push_back(m);
    ma.markers.push_back(m_lines);
    return ma;
}
