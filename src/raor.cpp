#include "safe_raor/raor.h"
#include "ros/ros.h"
#include <time.h>

ca::RAOR::RAOR(ViewPointGenerator *vg, std::vector<ViewPoint> & vp_list):
    sampled_path_(vp_list),
    best_path_(vp_list),
    global_vp_list_(vp_list)
{
    vg_ = vg;
    srand (time(NULL));
    runtime_visualization_ = false;
}

size_t ca::RAOR::SampleNode(size_t max_num){
    size_t random_node;
    do{
        random_node = (rand()%max_num);
    }while(random_node == start_location_id_ || random_node == end_node_id_);
    return random_node;
}

std::vector<size_t> ca::RAOR::SampleRandomRoute(size_t max_num){
    std::vector<size_t> return_route;
    return_route.push_back(start_location_id_);
    return_route.push_back(end_node_id_);
    size_t random_node=0;
    for(size_t i=0; i<=max_num; i++){
        random_node = (rand()%2);
        if(i== start_location_id_ || i==end_node_id_)
            random_node = 0;
        if(random_node==1)
            return_route.push_back(i);
    }
    return return_route;
}

bool ca::RAOR::FlipBelongingness(size_t node, ca::Path &path){
    //std::cout<<"\n--"<< node<<"--Flipping belongingness --";
    //std::cout<<"Input Route::\n";
    /*for(size_t i=0; i<path.path_.size(); i++){
        std::cout<<path.path_[i]<<"->";
    }*/

    if(!path.AddNode(node,false)){
        //std::cout<<"Deleting Node";
        path.DeleteNode(node,false);
    }

    /*std::cout<<"\nOutput Route::";
    for(size_t i=0; i<path.path_.size(); i++){
        std::cout<<path.path_[i]<<"->";
    }*/


    return true;
}

bool ca::RAOR::IsInRouteList(std::vector<size_t> &q_route){
    /*std::cout<<"IsInRouteList::";
    for(size_t i=0; i<q_route.size(); i++){
        std::cout<<q_route[i]<<"-->";
    }
    std::cout<<"\n";*/
    for(size_t i=0; i< path_vec_.size(); i++)
    {        
        //std::cout<<"\n Isse mila rahe hai::"<<i<<"::";
        std::vector<size_t>& p_vec = path_vec_[i].path_;
        //for(size_t j=0; j<p_vec.size(); j++){
        //    std::cout<<p_vec[j]<<"-->";
        //}
        //std::cout<<"\n";
       if (q_route.size() != p_vec.size()){
           //std::cout<<"Size hi nahin mila. q_route::"<<q_route.size()<<" p_vec::"<<p_vec.size()<<"\n";
          continue;
       }
       if(p_vec == q_route){
           //std::cout<<"Mil gya! \n";
           return true;
       }
       //std::cout<<"Match hi nahin hua \n";
    }
    return false;
}

void ca::RAOR::SetupAndSolveTsp(ca::Path &path,std::vector<size_t> &node_order, size_t start_location_id, size_t end_node_id){
    // start_location_id and end_location_id are global_vp_lists
    // setup the D matrix according to start and end
    int start_id, end_id, fake_id;

    for(int i=0; i<node_order.size();i++){
        if(node_order[i] == start_location_id)
            start_id = i;
        if(node_order[i] == end_node_id)
            end_id = i;
    }
    fake_id = node_order.size();
    //ROS_INFO_STREAM("Setting up D!::"<<node_order.size()<<"::"<<start_id<<"::"<<end_id);
    Eigen::MatrixXd D = DMatrix(node_order, start_id, end_id);
    //ROS_INFO_STREAM("Initializing!");
    tsp_.Initialize(D);
    std::vector<size_t> new_order;
    //ROS_INFO_STREAM("Solving!");
    tsp_.Solve(new_order); //New Order is the index of node_order, so are start_id and end_id
    //ROS_INFO_STREAM("New Order--");
    //for(size_t i=0; i< new_order.size(); i++){
    //    ROS_INFO_STREAM(new_order[i]<<"-->");
    //}
    //ROS_INFO_STREAM("Fixing Order!");
    std::vector<size_t> new_order1 = FixOrder(new_order, start_id, end_id, fake_id);
    path.ResetOrder(new_order1);

}

std::vector<size_t> ca::RAOR::FixOrder(std::vector<size_t> &order, int start_id, int end_id, int fake_id){
    int s_id,e_id, f_id;
    //ROS_INFO_STREAM("TSP order::start_id::end_id::fake_id::"<<start_id<<"::"<<end_id<<"::"<<fake_id);
    for(size_t i=0; i< order.size();i++)
    {
        //ROS_INFO_STREAM(order[i]<<"-->");
        if(order[i] == start_id)
            s_id = i;

        if(order[i] == end_id)
            e_id = i;

        if(order[i] == fake_id)
            f_id = i;

    }

    std::vector<size_t> new_order;
    if(f_id == 0 || f_id==(order.size()-1)){
        if(s_id < e_id){
            for(int i=s_id; i<= (int)e_id; i++){
                new_order.push_back(order[i]);
            }
        }
        else{
            for(int i=s_id; i>=(int)e_id; i--){
                new_order.push_back(order[i]);
            }
        }
    }
    else{

        if(s_id < e_id){
            for(int i=s_id; i>=0; i--){
                new_order.push_back(order[i]);
            }
            for(int i=order.size()-1; i>=e_id; i--){
                new_order.push_back(order[i]);
            }
        }
        else{
            for(int i=s_id; i<order.size(); i++){
                new_order.push_back(order[i]);
            }
            for(int i=0; i<=e_id; i++){
                new_order.push_back(order[i]);
            }
        }
    }

    return new_order;
}

Eigen::MatrixXd ca::RAOR::DMatrix(std::vector<size_t> &points, int start_id, int end_id){
    Eigen::MatrixXd D((points.size()+1),(points.size()+1));
    //ROS_INFO_STREAM("Allocated D");
    size_t fake_id = points.size();
    double distance;
    Eigen::Vector3d p1,p2;
     for(size_t i=0; i<D.rows();i++){
        if(i<points.size()){
            ca::ViewPoint& vp = global_vp_list_[points[i]];
            p1 = vp.vp_pose_.position_;}
        for(size_t j=0; j<D.cols();j++){ // This can be
            if(i==fake_id || j==fake_id){
                D(i,j) = 1000000.0;
                D(j,i) = 1000000.0;
            }
            else{
                ca::ViewPoint& vp = global_vp_list_[points[j]];
                p2 = vp.vp_pose_.position_;
                distance = (p1-p2).norm();
                D(i,j) =distance;
            }
        }
        D(i,i) = 0;
    }

    D(fake_id,start_id) = 0.0000001;
    D(start_id,fake_id) = 0.0000001;
    D(fake_id,end_id) = 0.0000001;
    D(end_id,fake_id) = 0.0000001;
    return D;
}

void ca::RAOR::AddRoutetoRouteList(ca::Path &path){

    /*std::cout<<"\n AddRoutetoRoutelist Route::";
    for(int i=0; i< path.path_.size();i++){
        std::cout<<path.path_[i]<<"->";
    }*/

    if(IsInRouteList(path.path_)){
        //std::cout<<"\n Not adding the route its already there"<<"\n";
        return;
    }
    else{
        //std::cout<<"\n Adding Route"<<"\n";
        path_vec_.push_back(path);
    }
}

void ca::RAOR::AddNodetoRouteList(size_t sampled_node, double budget,
                                  int& new_route_id, int& changed_route_id, ca::FixedGrid2f* grid2, ca::SemanticGridCell* ip_rep){
    new_route_id = -1;
    changed_route_id = -1;
    double benefit = 0;
    Eigen::Vector3d p,v;
    float heading,fov;
    global_vp_list_[sampled_node].get_pose(p, fov, heading);
    double distance;
    double best_benefit = 0;
    int best_route = -1;
    double reward = 0;
    double best_reward = 0;
    for(size_t i=0;i<path_vec_.size();i++){
        ca::Path& path = path_vec_[i];
        if(!IsInRoute(sampled_node,path.path_)){
            distance = path.CostOfAdding(path.path_,global_vp_list_,p);
            if(budget >= (distance + path.path_length_)){
                reward = reward_.RewardFunction(sampled_node,global_vp_list_,path.rep_state_.data());
                benefit = reward/std::max(distance,0.01);
                if(benefit> best_benefit){
                    best_route = i;
                    best_benefit = benefit;
                    best_reward = reward;
                }
            }
        }
    }

    bool added = false;
    if(best_route>=0){
        ca::Path path(global_vp_list_); path.CopyWithoutRepresentation(path_vec_[best_route]);
        path.AddNode(sampled_node,false);
        SetupAndSolveTsp(path,path.path_,start_location_id_, end_node_id_);
        //std::cout<<"Best Route found--";
        //std::cout<<"\n Route::";
        //for(int i=0; i< path.path_.size();i++){
        //    std::cout<<path.path_[i]<<"->";
        //}

        if(!IsInRouteList(path.path_)){
            //std::cout<<"\n Adding the modifications to best route";
            path.setRepresentationState(path_vec_[best_route].grid2_,path_vec_[best_route].rep_state_.data());
            path.UpdateRepresentation();
            path.path_length_ = path.CalculatePathLength(path.path_,global_vp_list_);
            best_reward = best_reward + path.get_path_reward();
            path.set_path_reward(best_reward);
            path_vec_.push_back(path);
            added = true;
            changed_route_id = best_route;
            new_route_id = path_vec_.size()-1;
        }
        //else{
        //    std::cout<<"\n Route exists, moving on!!";
        //}
    }// Yahan loop mein kucch mazze karo kyunki its not working, at least.
    //find the best route, see if it is appropriate, find the second best route, leave when not good enough routes are full

    if(!added){
        ca::Path path(global_vp_list_);
        path.AddNode(start_location_id_,false);
        path.AddNode(end_node_id_,false);
        distance = path.CostOfAdding(path.path_,global_vp_list_,p);
        //std::cout<<"\n Making a new route::";
        //for(size_t i=0; i<path.path_.size(); i++){
        //    std::cout<<path.path_[i]<<"-->";
        //}
        double path_length = path.CalculatePathLength(path.path_,global_vp_list_);
        if((path_length+distance)<= budget){
            path.AddNode(sampled_node,false);
            SetupAndSolveTsp(path,path.path_,start_location_id_, end_node_id_);
            //std::cout<<"\n NewRoute Route::";
            //for(int i=0; i< path.path_.size();i++){
            //    std::cout<<path.path_[i]<<"->";
            //}
            if(!IsInRouteList(path.path_)){
                path.setRepresentationState(grid2,ip_rep);
                path.path_length_ = path.CalculatePathLength(path.path_,global_vp_list_);
                path.UpdateRepresentation();
                path.set_path_reward(reward_.RewardFunction(path.path_,global_vp_list_,ip_rep,path.rep_state_.data()));
                path_vec_.push_back(path);
                //std::cout<<"\n NewRoute Added";
                changed_route_id = path_vec_.size()-1;
                new_route_id = path_vec_.size()-1;
            }
            //else{
            //    std::cout<<"\n It already exists";
            //}
        }
        //else{
        //    std::cout<<"It exceeds budget path length::"<<path_length<<"Distance::"<<distance<<"Budget::"<<budget;
        //}

    }
    return;
}

ca::Path& ca::RAOR::Run(Eigen::Vector3d& start_point, double start_heading, double start_fov,
        size_t end_node_id, FixedGrid2f *grid2, ca::SemanticGridCell* grid_data, double budget){
    sampled_path_.path_.clear();
    path_vec_.clear();

    ca::ViewPoint vp;
            vg_->MakeViewPoint(vp,start_point,start_heading,start_fov);
                    //MakeViewPoint(vp,start_point,start_heading,start_fov);
    global_vp_list_.push_back(vp);

    start_location_id_ = global_vp_list_.size()-1;
    end_node_id_ = end_node_id;
    //sampled_path_.setRepresentationState(ri);
    //sampled_path_.AddNode(start_location_id_,false);
    //sampled_path_.AddNode(end_node_id,false);


    ROS_ERROR_STREAM("List Size2::"<<global_vp_list_.size()<<" Path nodes::"<<best_path_.path_.size());
    size_t dummy;
    double loop_time = 0;
    double path_length=0;
    double path_reward=0;
    double best_reward=0;
    double inner_runtime=0;
    runtime_ = 1;
    inner_runtime_ = 10;
    while(loop_time < runtime_ && ros::ok()){
        loop_time = loop_time+1;
        std::vector<size_t> new_sampled_route= SampleRandomRoute((global_vp_list_.size()-1));
        sampled_path_.AddNodes(new_sampled_route,false);
        ROS_ERROR_STREAM("Outer loop::"<<loop_time);
        inner_runtime = 0;
        while(inner_runtime < inner_runtime_  && ros::ok()){
            std::cout<<"\n---Inner loop number "<< inner_runtime <<"starts, number of routes::"<<path_vec_.size()<<"--\n";
            /*std::cout<<"Route list in the beginning of inner time::\n";
            for(size_t i=0; i<path_vec_.size(); i++){
                ca::Path& path = path_vec_[i];
                std::cout<<"Route::"<<i<<"::";
                for(size_t j=0; j<path.path_.size(); j++){
                    std::cout<<path.path_[j]<<"->";
                }
                std::cout<<"\n";
            }*/
            inner_runtime = inner_runtime+1;
            SetupAndSolveTsp(sampled_path_,sampled_path_.path_,start_location_id_, end_node_id_);
            path_length = sampled_path_.CalculatePathLength(sampled_path_.path_,global_vp_list_);
            sampled_path_.path_length_ = path_length;
            if(path_length <= budget && !IsInRouteList(sampled_path_.path_)){
                ca::Path temp_path(global_vp_list_); temp_path=sampled_path_;// sampled path is not initialized
                temp_path.setRepresentationState(grid2,grid_data);// made lazy
                temp_path.UpdateRepresentation();
                path_reward = reward_.RewardFunction(temp_path.path_,global_vp_list_,grid_data,temp_path.rep_state_.data());
                temp_path.path_length_ = path_length;
                temp_path.set_path_reward(path_reward);
                //std::cout<<"Route list::\n";
                /*for(size_t i=0; i<path_vec_.size(); i++){
                    ca::SafeRAORPath& path = path_vec_[i];
                    std::cout<<"Route::"<<i<<"::";
                    for(size_t j=0; j<path.path_.size(); j++){
                        std::cout<<path.path_[j]<<"->";
                    }
                    std::cout<<"\n";
                }
                std::cout<<"Trying out sampled path, number of routes::"<<path_vec_.size()<<"\n";*/
                AddRoutetoRouteList(temp_path);
            }
            //ROS_INFO_STREAM("Inner loop::"<<inner_runtime);
            size_t sampled_node = SampleNode((global_vp_list_.size()-1));
            FlipBelongingness(sampled_node,sampled_path_);
            int changed_route_id = 0; int new_route_id = 0;
            //std::cout<<"\n Sampled node, number of routes::"<<path_vec_.size()<<"\n";
            AddNodetoRouteList(sampled_node,budget,new_route_id, changed_route_id, grid2,grid_data);
            //std::cout<<"\n Inner loop ends, number of routes::"<<path_vec_.size()<<"\n";
            if(runtime_visualization_){
                ros::Rate vis_loop_rate(100);
                int best_route_id = 0; double best_reward = 0;
                double best_path_length = budget;
                for(size_t i=0; i<path_vec_.size();i++){
                    if(path_vec_[i].path_reward_ > best_reward){
                        best_route_id = i;
                        best_reward = path_vec_[i].path_reward_;
                        best_path_length = path_vec_[i].path_length_;
                    }
                    else if(path_vec_[i].path_reward_ == best_reward &&
                            path_vec_[i].path_length_  < best_path_length){
                        best_route_id = i;
                        best_reward = path_vec_[i].path_reward_;
                        best_path_length = path_vec_[i].path_length_;
                    }
                }
                if(changed_route_id<0){ changed_route_id = best_route_id;}
                if(new_route_id<0){new_route_id = best_route_id;}
                ROS_INFO_STREAM("Visualization Iteration, Num of routes::"<<path_vec_.size());
                ROS_INFO_STREAM("Number of viewpoints::"<<global_vp_list_.size());
                visualization_pub_.publish(GetGraphRuntimeMarkers(sampled_node, new_route_id, best_route_id, changed_route_id));
                ros::spinOnce();
                vis_loop_rate.sleep();
            }
        }
    }

    size_t best_path = 0; best_reward = 0;
    double best_path_length = budget;
    ROS_INFO_STREAM("Number of paths::"<<path_vec_.size());
    for(size_t i=0; i<path_vec_.size();i++){
        if(path_vec_[i].path_reward_ > best_reward){
            best_path = i;
            best_reward = path_vec_[i].path_reward_;
            best_path_length = path_vec_[i].path_length_;
        }
        else if(path_vec_[i].path_reward_ == best_reward &&
                path_vec_[i].path_length_  < best_path_length){
            best_path = i;
            best_reward = path_vec_[i].path_reward_;
            best_path_length = path_vec_[i].path_length_;
        }
    }
    if(path_vec_.size()>0){
        visualization_pub_.publish(GetGraphRuntimeMarkers(0, 0, best_path, 0));
        ROS_INFO_STREAM("Best path::"<<best_path<<"::"<<path_vec_.size());
        //best_path_ = path_vec_[best_path];
        for(size_t i=0; i<global_vp_list_.size();i++){
            ca::ViewPoint& vp =  global_vp_list_[i];
            Eigen::Vector3d & v = vp.vp_pose_.position_;
            double heading = vp.vp_pose_.heading_;
            ROS_INFO_STREAM("VP list3::"<<v.x()<<"::"<<v.y()<<"::"<<v.z()<<"::"<<heading);
        }
        ROS_ERROR_STREAM("Budget::"<<budget<<"Reward::"<<best_reward);
        return path_vec_[best_path];
    }
}


visualization_msgs::MarkerArray ca::RAOR::GetMarkers()
{
    visualization_msgs::MarkerArray ma;

    ca::ViewPoint vp;

    visualization_msgs::Marker m, m_lines;
    m.header.frame_id = frame_;
    //ROS_INFO_STREAM("Frame::"<<ri->get_frame());
    m.header.stamp = ros::Time();
    m.ns = "points";
    m.frame_locked = true;
    m.id = 0;
    m.type = visualization_msgs::Marker::SPHERE_LIST;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = 0;
    m.pose.position.y = 0;
    m.pose.position.z = 0;
    m.pose.orientation.x = 0.0;
    m.pose.orientation.y = 0.0;
    m.pose.orientation.z = 0.0;
    m.pose.orientation.w = 1.0;
    m.scale.x = 1;
    m.scale.y = 1;
    m.scale.z = 1;
    m.color.a = 1.0; // Don't forget to set the alpha!
    m.color.r = 1.0;
    m.color.g = 1.0;
    m.color.b = 1.0;

    m_lines = m;
    m_lines.type = visualization_msgs::Marker::LINE_LIST;
    m_lines.ns = "lines";
    m_lines.scale.x = 0.2;
    std_msgs::ColorRGBA c_blue; c_blue.r=0; c_blue.b=1.0; c_blue.g=0; c_blue.a = 1.0;
    std_msgs::ColorRGBA c_green; c_green.r = 0; c_green.b = 0; c_green.g = 1.0; c_green.a = 1.0;
    geometry_msgs::Point p;
    double line_length = 2.0;
    //Eigen::Vector3d unit_vec(1,0,0);
    for(size_t i=0; i<global_vp_list_.size();i++){
        ca::ViewPoint& vp = global_vp_list_[i];
        Eigen::Vector3d pos; float fov; float heading;
        vp.get_pose(pos,fov,heading);
        geometry_msgs::Point p;
        p.x =pos.x() ; p.y = pos.y(); p.z = pos.z();
        m.points.push_back(p);

        m_lines.points.push_back(p);
        p.x = p.x + line_length*cos(heading);
        p.y = p.y + line_length*sin(heading);
        p.z = p.z;
        m_lines.points.push_back(p);
        if(vp.visible_primitives_.size()>0){
            m.colors.push_back(c_green);
            m_lines.colors.push_back(c_green);
            m_lines.colors.push_back(c_green);
        }
        else{
            m.colors.push_back(c_blue);
            m_lines.colors.push_back(c_blue);
            m_lines.colors.push_back(c_blue);
        }
    }
    ma.markers.push_back(m);
    ma.markers.push_back(m_lines);
    return ma;
}


visualization_msgs::MarkerArray ca::RAOR::GetGraphRuntimeMarkers(size_t sampled_node, size_t new_route_id,
                                                                     size_t best_route_id, size_t changed_route_id)
{
    visualization_msgs::MarkerArray ma;
    ca::ViewPoint vp;

    visualization_msgs::Marker m_viewpoints;
    visualization_msgs::Marker m_lines;
    m_viewpoints.header.frame_id = frame_;
    //ROS_INFO_STREAM("Frame::"<<ri->get_frame());
    m_viewpoints.header.stamp = ros::Time();
    m_viewpoints.ns = "viewpoints";
    m_viewpoints.frame_locked = true;
    m_viewpoints.id = 0;
    m_viewpoints.type = visualization_msgs::Marker::SPHERE_LIST;
    m_viewpoints.action = visualization_msgs::Marker::ADD;
    m_viewpoints.pose.position.x = 0;
    m_viewpoints.pose.position.y = 0;
    m_viewpoints.pose.position.z = 0;
    m_viewpoints.pose.orientation.x = 0.0;
    m_viewpoints.pose.orientation.y = 0.0;
    m_viewpoints.pose.orientation.z = 0.0;
    m_viewpoints.pose.orientation.w = 1.0;
    m_viewpoints.scale.x = 1;
    m_viewpoints.scale.y = 1;
    m_viewpoints.scale.z = 1;
    m_viewpoints.color.a = 1.0; // Don't forget to set the alpha!
    m_viewpoints.color.r = 1.0;
    m_viewpoints.color.g = 1.0;
    m_viewpoints.color.b = 1.0;

    m_lines = m_viewpoints;
    m_lines.type = visualization_msgs::Marker::LINE_LIST;
    m_lines.ns = "lines";
    m_lines.scale.x = 0.2;
    std_msgs::ColorRGBA c_blue; c_blue.r=0; c_blue.b=1.0; c_blue.g=0; c_blue.a = 1.0;
    std_msgs::ColorRGBA c_green; c_green.r = 0; c_green.b = 0; c_green.g = 1.0; c_green.a = 1.0;
    std_msgs::ColorRGBA c_red; c_red.r = 1.0; c_red.b = 0; c_red.g = 0.; c_red.a = 1.0;
    std_msgs::ColorRGBA c_grey; c_grey.r = .1; c_grey.b = 0.1; c_grey.g = 0.1; c_grey.a = 0.5;


    geometry_msgs::Point p;
    double line_length = 2.0;
    //Eigen::Vector3d unit_vec(1,0,0);
    for(size_t i=0; i<global_vp_list_.size();i++){
        ca::ViewPoint& vp = global_vp_list_[i];
        Eigen::Vector3d pos; float fov; float heading;
        vp.get_pose(pos,fov,heading);
        geometry_msgs::Point p;
        p.x =pos.x() ; p.y = pos.y(); p.z = pos.z();
        m_viewpoints.points.push_back(p);

        m_lines.points.push_back(p);
        p.x = p.x + line_length*cos(heading);
        p.y = p.y + line_length*sin(heading);
        p.z = p.z;
        m_lines.points.push_back(p);
        if(sampled_node == i){
            m_viewpoints.colors.push_back(c_red);
            m_lines.colors.push_back(c_red);
            m_lines.colors.push_back(c_red);
        }
        else if(vp.visible_primitives_.size()>0){
            m_viewpoints.colors.push_back(c_green);
            m_lines.colors.push_back(c_green);
            m_lines.colors.push_back(c_green);
        }
        else{
            m_viewpoints.colors.push_back(c_blue);
            m_lines.colors.push_back(c_blue);
            m_lines.colors.push_back(c_blue);
        }
    }
    ma.markers.push_back(m_viewpoints);
    ma.markers.push_back(m_lines);
    m_lines.points.clear();
    m_viewpoints.points.clear();

    m_lines.ns = "routes";
    m_lines.type = visualization_msgs::Marker::LINE_STRIP;
    m_lines.color = c_grey;
    m_lines.color.a = 0.2;
    m_lines.scale.x = 0.5; m_lines.scale.y = 0.1;
    m_lines.scale.z = 0.1;

    std::cout<<"\n---In visualization---\n";
    for(size_t j=0; j< path_vec_.size(); j++){
        Eigen::Vector3d pos; float fov,heading;
        ca::Path & path = path_vec_[j];
        //ROS_INFO_STREAM("Is in Routelist::"<<j<<"::"<<IsInRouteList(path.path_));
        //std::cout<<"\n Route::"<<j<<"\n";
        for(int i=0; i< path.path_.size();i++){
            //std::cout<<path.path_[i]<<"->";
            ca::ViewPoint &vp = path.global_vp_list_[path.path_[i]];
            vp.get_pose(pos,fov,heading);
            p.x = pos.x(); p.y = pos.y(); p.z = pos.z();
            m_lines.points.push_back(p);
        }
        ca::ViewPoint &vp = path.global_vp_list_[path.path_[0]];
        vp.get_pose(pos,fov,heading);
        p.x = pos.x(); p.y = pos.y(); p.z = pos.z();
        m_lines.points.push_back(p);
    }
    ma.markers.push_back(m_lines);

    m_lines.points.clear();
    m_lines.ns = "new_route";
    m_lines.color = c_blue;
    m_lines.scale.x = 0.5; m_lines.scale.y = 0.5;
    m_lines.scale.z = 0.5;
    {
        ca::Path & path = path_vec_[new_route_id];
        for(int i=0; i< path.path_.size();i++){
            Eigen::Vector3d pos; float fov,heading;
            ca::ViewPoint &vp = path.global_vp_list_[path.path_[i]];
            vp.get_pose(pos,fov,heading);
            p.x = pos.x(); p.y = pos.y(); p.z = pos.z();
            m_lines.points.push_back(p);
        }

        Eigen::Vector3d pos; float fov,heading;
        vp = path.global_vp_list_[path.path_[0]];
        vp.get_pose(pos,fov,heading);
        p.x = pos.x(); p.y = pos.y(); p.z = pos.z();
        m_lines.points.push_back(p);
        ma.markers.push_back(m_lines);
    }

    m_lines.points.clear();
    m_lines.ns = "changed_route";
    m_lines.color = c_red;
    {
        ca::Path& path1 = path_vec_[changed_route_id];
        for(int i=0; i< path1.path_.size();i++){
            Eigen::Vector3d pos; float fov,heading;
            ca::ViewPoint &vp = path1.global_vp_list_[path1.path_[i]];
            vp.get_pose(pos,fov,heading);
            p.x = pos.x(); p.y = pos.y(); p.z = pos.z();
            m_lines.points.push_back(p);
        }
        {
            Eigen::Vector3d pos; float fov,heading;
            vp = path1.global_vp_list_[path1.path_[0]];
            vp.get_pose(pos,fov,heading);
            p.x = pos.x(); p.y = pos.y(); p.z = pos.z();
            m_lines.points.push_back(p);
            ma.markers.push_back(m_lines);
        }
    }

    m_lines.points.clear();
    m_lines.ns = "best_route";
    m_lines.color = c_green;
    // size_t best_route_id
    {
        ca::Path& path2 = path_vec_[best_route_id];
        for(int i=0; i< path2.path_.size();i++){
            Eigen::Vector3d pos; float fov,heading;
            ca::ViewPoint &vp = path2.global_vp_list_[path2.path_[i]];
            vp.get_pose(pos,fov,heading);
            p.x = pos.x(); p.y = pos.y(); p.z = pos.z();
            m_lines.points.push_back(p);
        }
        {
            Eigen::Vector3d pos; float fov,heading;
            vp = path2.global_vp_list_[path2.path_[0]];
            vp.get_pose(pos,fov,heading);
            p.x = pos.x(); p.y = pos.y(); p.z = pos.z();
            m_lines.points.push_back(p);
            ma.markers.push_back(m_lines);
        }
    }


    return ma;
}
