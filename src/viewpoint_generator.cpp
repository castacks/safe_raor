#include"safe_raor/viewpoint_generator.h"
#include "ros/ros.h"

//    Where to place the nodes



bool ca::SafeRAORViewPointOnPolarGridInterface::
    Initialize(ca_ri::Polar_2D_Representation* polar_rep, double pitch_angle){
    pitch_angle_ = pitch_angle;
    cached_rays_.clear();
    polar_rep_ = polar_rep;

    Eigen::Vector3d vv(std::cos(pitch_angle),0,std::sin(pitch_angle));
    cached_rays_.push_back(vv);
    initialized_ = true;
    return true;
}

bool ca::SafeRAORViewPointOnPolarGridInterface::
MakeViewPoint(ca::SafeRAORViewPoint &vp , Eigen::Vector3d &pos, Eigen::Vector3d &target, double fov, double range){
    Eigen::Vector3d vv = target -pos; vv.z() = 0;
    vv.normalize(); double heading = std::atan2(vv.y(),vv.x());
    if(std::isnan(heading))
        heading =0;

    Eigen::Vector3d v;
    vp.ResetViewPoint(pos,heading,fov,v);

    if(polar_rep_->IsInCylinder(pos))
        return false;

    Eigen::Quaterniond  q(0.0,0.0,0.0,0.0);
    q.w() = std::cos(0.5*heading); q.z() = std::sin(0.5*heading);
    //ROS_INFO_STREAM("Size of cached rays::"<<cached_rays_.size());
    Eigen::Vector3d end_pos,intersection_pt;
    double radius = 0.9*polar_rep_->GetRadius();
    size_t new_id;
    for(size_t i=0; i<cached_rays_.size(); i++){
        vv = cached_rays_[i];
        if(vv.z()>=0.0){
             vv = q*vv;
            end_pos = pos + range*vv;
            if(polar_rep_->FindIntersection(pos,end_pos,radius,intersection_pt)){
                size_t id;
                if(polar_rep_->GetIDs(intersection_pt,id)){
                    new_id = id;
                    vp.AddVisiblePrimitive(id);
                }
            }
        }
        ROS_INFO_STREAM("Start::"<<pos.x()<<"::"<<pos.y()<<"::"<<pos.z());
        ROS_INFO_STREAM("End::"<<end_pos.x()<<"::"<<end_pos.y()<<"::"<<end_pos.z());
        ROS_INFO_STREAM("Intersection::"<<intersection_pt.x()<<"::"<<intersection_pt.y()<<"::"<<intersection_pt.z());
        ROS_INFO_STREAM("Intersection ID::"<<new_id);
    }
    return true;
}

bool ca::SafeRAORViewPointOnPolarGridInterface::
MakeViewPoint(ca::SafeRAORViewPoint &vp , Eigen::Vector3d &pos, double heading, double fov, double range){
    Eigen::Vector3d v;
    vp.ResetViewPoint(pos,heading,fov,v);

    if(polar_rep_->IsInCylinder(pos))
        return false;

    Eigen::Quaterniond  q(0.0,0.0,0.0,0.0);
    q.w() = std::cos(0.5*heading); q.z() = std::sin(0.5*heading);
    Eigen::Vector3d vv;
    //ROS_INFO_STREAM("Size of cached rays::"<<cached_rays_.size());
    Eigen::Vector3d end_pos,intersection_pt;
    double radius = 0.9*polar_rep_->GetRadius();
    for(size_t i=0; i<cached_rays_.size(); i++){
        vv = cached_rays_[i];
        if(vv.z()>=0.0){
             vv = q*vv;             
            end_pos = pos + range*vv;

            if(polar_rep_->FindIntersection(pos,end_pos,radius,intersection_pt)){
                size_t id;
                if(polar_rep_->GetIDs(intersection_pt,id)){
                    vp.AddVisiblePrimitive(id);
                }
            }
        }       
    }
    return true;
}
