#include "safe_raor/safe_connection.h"
// Heading List::
//0. 3.14 0
//1.-2.09 14
//2.-1.04 28
//3. 0    42
//4. 1.04 56
//5. 2.09 70
//6  0    84
double ca::SafeConnection::FixedHeadingForOptimizeHeading(ca::eml::Trajectory &traj, size_t id, std::vector<size_t> &fixed_heading_ids, int &next_fixed_wp){
    size_t max_id = *std::max_element(fixed_heading_ids.begin(),fixed_heading_ids.end());
    if(traj.xyz.size()<=id || traj.xyz.size()<= max_id ){
        ROS_ERROR_STREAM("FixedHeadingForOptimizeHeading i::"<<id<<" max_id::"<<max_id<<" Traj size::"<<traj.xyz.size());
        std::exit(-1);
    }
    for(size_t i=0; i<fixed_heading_ids.size(); i++){
        if(fixed_heading_ids[i]>=id){
            double heading = traj.rpy[fixed_heading_ids[i]].z();
            next_fixed_wp = fixed_heading_ids[i];
            return heading;
        }
    }
    next_fixed_wp = -1;
    return -1000.;
}

bool ca::SafeConnection::ForwardSimulate(Eigen::Vector3d& pos, Eigen::Vector3d& v0, Eigen::Vector3d& v1, double& theta0, double& theta1, double del_t){
    double eps = 0.00001;
    double v_norm = (v1-v0).norm();
    Eigen::Vector3d vf;
    if(v_norm > eps){
        if(v_norm > (max_acceleration_*del_t)){
            vf = v0 + ((v1-v0)/v_norm)*max_acceleration_*del_t;
        }
        else{
            vf = v0 + (v1-v0);
        }
    }
    pos = pos + 0.5*(v0+vf)*del_t;
    double theta_norm = std::fabs(AngleDiff(theta0,theta1));
    if(theta_norm > (heading_rate_rad_p_s_*del_t))
        theta0 = theta0 + (AngleDiff(theta0,theta1)/theta_norm)*heading_rate_rad_p_s_*del_t;
    else
        theta0 = theta0 + AngleDiff(theta0,theta1);
    v0 = vf;
    theta0  = ConstrainAngle(theta0);
    return true;
}

bool ca::SafeConnection::UpdateAngle(double& theta0, double& theta1, double del_t, double heading_rate){
    double eps = 0.00001;
    double theta_norm = std::fabs(AngleDiff(theta0,theta1));
    if(theta_norm > (heading_rate_rad_p_s_*del_t)){
        //ROS_INFO_STREAM("In IF theta_0::"<<theta0 <<" theta_1::"<<theta1<<" del_t::"<<del_t<<"AngleDiff::"<<AngleDiff(theta0,theta1));
        theta0 = theta0 + (AngleDiff(theta0,theta1)/theta_norm)*heading_rate*del_t;
    }
    else{
        //ROS_INFO_STREAM("In ELSE theta_0::"<<theta0 <<" theta_1::"<<theta1<<" del_t::"<<del_t<<"AngleDiff::"<<AngleDiff(theta0,theta1));
        theta0 = theta0 + AngleDiff(theta0,theta1);
    }

    theta0  = ConstrainAngle(theta0);
    return true;
}


Eigen::Vector3d ca::SafeConnection::UpdateVelocityBangBangWithEMLandHeadingRate(Eigen::Vector3d &p0, Eigen::Vector3d &v0, double theta0,representation_interface::PoseGraphRepresentation &ri,
                               Eigen::Vector3d &tp, Eigen::Vector3d &v_tp, double theta_tp,double max_speed, bool& valid, double delt){
    // theta will take care of itself, lets just do velocity
    valid = true;
    Eigen::Vector3d path_vec = tp-p0;
    double d2turn = path_vec.norm();
    double v0_norm = v0.norm();
    double v_tp_norm = v_tp.norm();

    double acc_needed = (v_tp_norm*v_tp_norm - v0_norm*v0_norm)/(2*d2turn);
    if(std::fabs(acc_needed)> max_acceleration_){
        valid = false;
        std::cerr<<"Eigen::Vector3d ca::SafeConnection::UpdateVelocity -- Dynamics are not feasible";
        exit(-1);
    }

    // handling errors due to discretization.
    double del_dist_acc_max = v0_norm*delt + 0.5*max_acceleration_*delt*delt;
    double dist_post_update = d2turn - del_dist_acc_max;
    double v_post_update = v0_norm + max_acceleration_*delt;
    double acc_needed_next = (v_tp_norm*v_tp_norm - v_post_update*v_post_update)/(2*dist_post_update);
    if(dist_post_update >= 0. && std::fabs(acc_needed_next) < max_acceleration_ ){
        acc_needed = max_acceleration_;
    }
    double v_new_norm = std::min((v0_norm + acc_needed*delt), max_speed);
    v_new_norm = std::max(0.,v_new_norm);
    Eigen::Vector3d v_new = v_new_norm*(path_vec/d2turn);
    Eigen::Vector3d p_new = p0 + (v_new + v0)*0.5*delt;
    // safe speed
    double heading = 0;
    double v_safe = SafeSpeed(p_new,v_new, 0,ri);
    v_new = v_safe*(path_vec/d2turn);
    return v_new;
}


visualization_msgs::MarkerArray ca::SafeConnection::DisplayRepresentationandSafety(representation_interface::PoseGraphRepresentation &ri,
                                                                                   ca::eml::Trajectory &traj, size_t id){
    // traj has global velocity
    //: trajectory doesn
    id = std::min(traj.xyz.size(),id);

    visualization_msgs::MarkerArray ma;
    ca::eml::State q_state = traj.at(id);
    size_t param_sensing_frequency = 2;
    size_t added_nodes = 1;
    if(traj.xyz.size()>0){
        ri.AddSensingPoint(traj.xyz[0],traj.rpy[0](2));
    }
    for(size_t i=0; i<id; i = i+param_sensing_frequency){
        ri.AddSensingPoint(traj.xyz[i],traj.rpy[i](2));
        added_nodes++;
    }

    double safe_velocity = 0.;
    std::vector<ca::eml::Trajectory> safe_traj_vec = sfchk_.returnSafeVelocity(q_state,safe_velocity);
    sfchk_.generateMarkersSafeUnSafeTrajectories(ma,&safe_traj_vec);
    ROS_INFO_STREAM("MA size::"<<ma.markers.size()<<" Trajectory Size::"<<safe_traj_vec.size());
    ma.markers[0].header.frame_id = ri.get_frame();
    //size_t id = 0;

    visualization_msgs::MarkerArray ma1 = ri.GetCylinder(1);
    for(size_t i=0; i<ma1.markers.size(); i++){
        ma.markers.push_back(ma1.markers[i]);
    }
    ma.markers.push_back(ri.GetSectors(1));
    ri.sensing_points_.erase(ri.sensing_points_.end()-added_nodes,ri.sensing_points_.end());

    if(traj.xyz.size()>1){
        visualization_msgs::Marker m;
        m.header.frame_id = ri.get_frame();
        m.id = 0;
        m.ns = "start_end"; m.color.a = 1.0;
        m.color.r = m.color.g = 0.; m.color.b = 1.0;
        m.scale.x = m.scale.y = m.scale.z = 0.2;
        m.action = visualization_msgs::Marker::ADD;
        m.type = visualization_msgs::Marker::SPHERE_LIST;
        geometry_msgs::Point p;
        p.x = traj.xyz[0].x(); p.y = traj.xyz[0].y();  p.z = traj.xyz[0].z();
        m.points.push_back(p);
        size_t end_id = traj.xyz.size()-1;
        p.x = traj.xyz[end_id].x(); p.y = traj.xyz[end_id].y();  p.z = traj.xyz[end_id].z();
        m.points.push_back(p);
        ma.markers.push_back(m);
        m.points.clear();

        m.color.b = m.color.g = 0.; m.color.r = 1.0;
        m.scale.x = m.scale.y = m.scale.z = 0.5;
        m.ns = "start_end_heading";
        m.type = visualization_msgs::Marker::LINE_LIST;
        p.x = traj.xyz[0].x(); p.y = traj.xyz[0].y();  p.z = traj.xyz[0].z();
        m.points.push_back(p);
        p.x = traj.xyz[0].x() + 5*std::cos(traj.rpy[0].z());
        p.y = traj.xyz[0].y() + 5*std::sin(traj.rpy[0].z());
        p.y = traj.xyz[0].y() + 5*std::sin(traj.rpy[0].z());
        p.z = traj.xyz[0].z();
        m.points.push_back(p);
        p.x = traj.xyz[end_id].x(); p.y = traj.xyz[end_id].y();  p.z = traj.xyz[end_id].z();
        m.points.push_back(p);
        p.x = traj.xyz[end_id].x() + 5*std::cos(traj.rpy[end_id].z());
        p.y = traj.xyz[end_id].y() + 5*std::sin(traj.rpy[end_id].z());
        p.z = traj.xyz[end_id].z();
        m.points.push_back(p);
        ma.markers.push_back(m);
        m.points.clear();

        m.color.r = m.color.g = 0.; m.color.b = 1.0;
        m.scale.x = m.scale.y = m.scale.z = 0.2;
        m.ns = "trajectory";
        m.type = visualization_msgs::Marker::LINE_STRIP;
        for(size_t i=0; i<traj.xyz.size();i++){
            p.x = traj.xyz[i].x(); p.y = traj.xyz[i].y();  p.z = traj.xyz[i].z();
            m.points.push_back(p);
        }
        ma.markers.push_back(m);
    }
    return ma;
}

bool ca::SafeConnection::InterpolatePos(Eigen::Vector3d& pos1, Eigen::Vector3d &pos2, double time1, double time2, double q_time, Eigen::Vector3d &ret_pos){
    //std::cout<<"The times time1::"<<time1<<" qtime::"<<q_time<<" time2::"<<time2;
    if(q_time < time1 || q_time > time2){
        return false;
    }
    double k = (q_time - time1)/(time2 - time1);
    ret_pos = (1-k)*pos1 + k*pos2;
    return true;
}


bool ca::SafeConnection::InterpolateHeading(double heading1, double heading2, double t1, double t2, double q_time,
                                            double &ret_heading){
    //std::cout<<"The times time1::"<<time1<<" qtime::"<<q_time<<" time2::"<<time2;
    if(q_time < t1 || q_time > t2){
        return false;
    }
    double k = (q_time - t1)/(t2 - t1);
    ret_heading = (1-k)*heading1 + k*heading2;
    if((heading_rate_rad_p_s_*(q_time-t1)) < std::fabs(ret_heading-heading1)){
        double heading_diff = heading_rate_rad_p_s_*(q_time-t1);
        if((heading1 - heading2)>0)
            ret_heading = heading1 + heading_diff;
        else
            ret_heading = heading1 - heading_diff;
    }

    return true;
}

bool ca::SafeConnection::CreateVSafeVec(representation_interface::PoseGraphRepresentation &ri, std::vector<size_t>& ignore_list, std::vector<double> & s_vec,
                                        Eigen::Vector3d &start_pos, Eigen::Vector3d &dir, double max_speed, std::vector<double> & v_safe_vec){
    v_safe_vec.clear();// Comment this in and you have a function
    v_safe_vec.resize(s_vec.size(),max_speed);
    /*ca::eml::State q_state;
    Eigen::Vector3d vel = dir*max_speed;
    q_state.frame = ri.get_frame();
    ri.SetIgnoreFrameList(ignore_list);
    double safe_speed;
    for(auto i:s_vec){
        current_pos = start_pos + i*dir;
        q_state.SetPose(current_pos);
        q_state.SetVelocity(vel);
        sfchk_.returnSafeVelocity(q_state,safe_speed);
        v_safe_vec.push_back(safe_speed);
    }
    ri.ClearIgnoreFrameList();
    */
    return true;
}


void ca::SafeConnection::TrajectoryFromOptOp(ca::eml::Trajectory &traj, Eigen::Vector3d& start_point, double start_heading, Eigen::Vector3d& end_point, double end_heading, Eigen::Vector3d &center,
                         double radius, std::vector<double> &s_vec, std::vector<double> &v_vec, std::vector<double> &t_vec, std::vector<double> &a_vec, size_t tp0_id, size_t tp_f){
    if(s_vec.size() != v_vec.size() && v_vec.size()<1){
        return;
    }

    Eigen::Vector3d path_dir = end_point -start_point; path_dir.normalize();
    double path_heading = std::atan2(path_dir.y(),path_dir.x());
    Eigen::Vector3d pos = start_point;
    Eigen::Vector3d vel;
    double heading = start_heading;
    double target_heading =  path_heading;
    double del_t;
    double heading_rate;
    if(traj.xyz.size()>0){
        Eigen::Vector3d prev_pos = traj.xyz[traj.xyz.size()-1];
        double t_prev = traj.t[traj.t.size()-1];
        for(size_t i=0; i<t_vec.size(); i++){
            t_vec[i] = t_vec[i] + t_prev;
        }
        double dist = (pos - prev_pos).norm();
        if(dist > 0.001){
            traj.xyz.push_back(pos);
            vel = v_vec[0]*Pos2Dir(pos, center, path_dir);
            traj.v_xyz.push_back(vel);
            traj.rpy.push_back(Eigen::Vector3d(0,0,heading));
            traj.t.push_back(t_vec[0]);            
        }
    }
    else{
        traj.xyz.push_back(pos);
        vel = v_vec[0]*Pos2Dir(pos, center, path_dir);
        traj.v_xyz.push_back(vel);
        traj.rpy.push_back(Eigen::Vector3d(0,0,heading));
        traj.t.push_back(t_vec[0]);
    }
    for(size_t i=1; i<s_vec.size(); i++){
        pos = StoPose(start_point, end_point, radius, center, s_vec[i]);
        vel = v_vec[i]*Pos2Dir(pos, center, path_dir);
        if(i == 0){
            heading =start_heading;
        }
        else if(i <= tp0_id){
            target_heading = path_heading;
            heading_rate = std::fabs(AngleDiff(start_heading,target_heading));
            if(heading_rate>0)
                heading_rate =   heading_rate/t_vec[tp0_id];
        }
        else if(i>= tp_f){
            target_heading = end_heading;
            heading_rate = std::fabs(AngleDiff(path_heading,target_heading));
            if(heading_rate>0)
                heading_rate =   heading_rate/(t_vec[t_vec.size()-1] - t_vec[tp_f]);

        }
        del_t = t_vec[i] - t_vec[i-1];
        UpdateAngle(heading, target_heading, del_t, heading_rate);
        traj.xyz.push_back(pos);
        traj.v_xyz.push_back(vel);
        traj.rpy.push_back(Eigen::Vector3d(0,0,heading));
        traj.t.push_back(t_vec[i]);
    }

}

double ca::SafeConnection::MinSafeTravelTime(SafeRAORViewPoint &start, SafeRAORViewPoint &end,
                                       double max_speed, double heading_rate, Eigen::Vector3d &path_center,
                                       representation_interface::PoseGraphRepresentation &ri,
                                        ca::eml::Trajectory *traj, bool aligned,bool reset_sensing){

    sfchk_.setRepresentationInterface(&ri);
    size_t sensing_location;// Keep track of what sensing nodes you add, you can always delete them later
    double del_t = dynamics_resolution_s_;
    // Initializing start heading and start position
    Eigen::Vector3d start_p; double start_heading;
    float start_heading_fl,dummy;
    Eigen::Vector3d dummy_vec;
    start.get_pose(start_p,dummy,start_heading_fl,dummy_vec);
    start_heading = start_heading_fl;
    double start_speed = dummy_vec.norm();
    // Initializing end heading and end position
    Eigen::Vector3d end_p; double end_heading;
    end.get_pose(end_p,dummy,start_heading_fl,dummy_vec);
    end_heading = start_heading_fl;
    // path_heading is the path heading
    Eigen::Vector3d path_vec = end_p - start_p;
    double path_heading = std::atan2(path_vec.y(),path_vec.x());
    double path_distance = path_vec.norm();
    // A hack to make sure that start point is safe
    Eigen::Vector3d sensing_p = start_p;
    sensing_p.x() = start_p.x() - 2.*std::cos(start_heading);
    sensing_p.y() = start_p.y() - 2.*std::sin(start_heading);
    ri.AddSensingPoint(sensing_p,start_heading);
    sensing_location =1;
    double safe_speed=0;
    //Initilizing pos, heading and velocity to start forward simulating
    Eigen::Vector3d pos = start_p;
    double heading = start_heading;
    Eigen::Vector3d vel = (path_vec/path_vec.norm())*max_speed;
    if(std::fabs(vel.z()) > max_z_speed_){
        vel = (max_z_speed_/std::fabs(vel.z()))*vel;
    }
    //ROS_INFO_STREAM("Start P x::"<<start_p.x()<<" y::"<<start_p.y()<<" z::"<<start_p.z()<<" heading::"<<start_heading
    //                <<" path_heading::"<<path_heading);
    //ROS_INFO_STREAM("End P x::"<<end_p.x()<<" y::"<<end_p.y()<<" z::"<<end_p.z()<<" heading::"<<end_heading);

    Eigen::Vector3d ori_vel = vel;// for reference
    double ori_vel_norm = ori_vel.norm();
    Eigen::Vector3d ori_vel_dir = ori_vel/ori_vel_norm; // for reference
    std::vector<size_t> ignore_list;
    size_t tp0_id = 0;
    size_t tp1_id = 0;
    std::vector<double> opt_s_vec;
    std::vector<double> opt_v_vec;
    std::vector<double> opt_t_vec;
    std::vector<double> opt_a_vec;
    bool found_optimal = false;
    double path_radius = (start_p - path_center).norm();
    // Todo -- ideally nothing needs to be done
    if(aligned){
    }
    else{
        //ROS_INFO_STREAM("In else");
        double s = ArcLength(start_p, end_p, path_radius, path_center);
        Eigen::Vector3d path_dir = path_vec/s;
        double desired_speed = 0.1;
        std::vector<double> v_safe_vec, s_vec, v_vec, t_vec,a_vec;
        ca::eml::State q_unsafe_state;
        Eigen::Vector3d start_point = start_p;
        NextUnsafeState(q_unsafe_state, start_point, start_p, end_p, path_center, path_radius, s,ri,desired_speed,max_speed, v_safe_vec, s_vec);
        size_t max_tp0_pt= GetMaxStartID(v_safe_vec);
        size_t min_tp1_pt =  EndTurningDistance(s, sensor_range_m_,s_vec);
        std::vector<size_t> ri_index_vec;
        CacheRepresentationInterface(ri, start_p, end_p, s_vec, ri_index_vec);
        double min_safe_time = 1e06;
        double min_tp0_time = std::fabs(AngleDiff(start_heading,path_heading))/heading_rate;
        double min_f_time = std::fabs(AngleDiff(path_heading,end_heading))/heading_rate;
        //ROS_INFO_STREAM("min_tp0_time::"<<min_tp0_time <<" min_f_time::"<<min_f_time);
        //ROS_INFO_STREAM("s_vec size::"<<s_vec.size()<<" max_tp0::"<<max_tp0_pt<<" min_tp1::"<<min_tp1_pt);

        for(size_t id_tp0=1; id_tp0 <= max_tp0_pt; id_tp0++){
            //ROS_ERROR_STREAM("In id_tp0::"<<id_tp0<<"start speed::"<<start_speed);
            ignore_list.clear();
            double s_tp0 = s_vec[id_tp0];
            double v_max_tp0 = start_speed;
            if(v_max_tp0 < desired_speed)
                break;
            for(size_t i=0; i<id_tp0;i++)
                ignore_list.push_back(ri_index_vec[i]);
            for(size_t tp_id1=std::max((id_tp0+1),min_tp1_pt); tp_id1<s_vec.size(); tp_id1++){
                //ROS_ERROR_STREAM("In For::"<<tp_id1<<"::"<<id_tp0);
                for(size_t j= tp_id1; j<s_vec.size(); j++)
                    ignore_list.push_back(ri_index_vec[j]);

                CreateVSafeVec(ri, ignore_list, s_vec, start_p, path_dir, max_speed, v_safe_vec);
                double s_tp1 = s_vec[tp_id1] - s_vec[id_tp0];
                bool valid_path = false;

                valid_path = SafeConnect(s, v_max_tp0, min_tp0_time, s_tp0, min_f_time, s_tp1, v_safe_vec[v_safe_vec.size()-1],
                        max_acceleration_, max_speed, s_res_, a_res_, t_vec, a_vec, v_vec, s_vec, v_safe_vec);
                //ROS_INFO_STREAM("s::"<<s<<" v0::"<<v_max_tp0<<" s_tp0::"<<s_tp0<<" tp0_time::"<<min_tp0_time<<" s_tp1::"<<s_tp1
                //                <<"vf::"<<v_safe_vec[v_safe_vec.size()-1]<< "t_f::"<<min_f_time<<" Time::"<<min_safe_time<<" Valid::"<<valid_path);

                if(valid_path && (t_vec[t_vec.size()-1] <= min_safe_time)){
                    //ROS_INFO_STREAM("s_tp0::"<<s_tp0<<" s_tp1::"<<s_tp1<<"vf::"<<v_safe_vec[v_safe_vec.size()-1]
                    //        <<" Time::"<<t_vec[t_vec.size()-1]<<" Valid::"<<valid_path);
                    min_safe_time = t_vec[t_vec.size()-1];
                    opt_s_vec = s_vec;
                    opt_v_vec = v_vec;
                    opt_t_vec = t_vec;
                    opt_a_vec = a_vec;
                    tp0_id = id_tp0;
                    tp1_id = tp_id1;
                    found_optimal = true;
                }
            }
        }

    }

    double min_time;
    if(!found_optimal){
        min_time = -1.;
    }
    else{
        min_time = opt_t_vec[opt_t_vec.size()-1];
    }

    //ROS_INFO_STREAM("")
    //Make a trajectory
    TrajectoryFromOptOp(*traj, start_p, start_heading, end_p, end_heading, path_center, path_radius,
                         opt_s_vec, opt_v_vec, opt_t_vec, opt_a_vec, tp0_id, tp1_id);
    if(reset_sensing){
        //ROS_INFO_STREAM("Reseting sensing::"<<sensing_location);
            ri.sensing_points_.erase(ri.sensing_points_.end()-sensing_location,ri.sensing_points_.end());
    }

    //for(size_t i=0; i<opt_s_vec.size();i++){
    //    ROS_INFO_STREAM("["<<i<<"] s::"<<opt_s_vec[i]<<" v::"<<opt_v_vec[i]<<" t::"<<opt_t_vec[i]
    //                    <<" TP1::"<<opt_s_vec[tp0_id]<<" TP2::"<<opt_s_vec[tp1_id]);
    //}
    /*for(size_t i=0; i<traj->xyz.size();i++){
        ROS_INFO_STREAM("["<<i<<"] x::"<<traj->xyz[i].x()<<" y::"<<traj->xyz[i].y()<<" z::"<<traj->xyz[i].z()
                        <<" vx::"<<traj->v_xyz[i].x()<<" vy::"<<traj->v_xyz[i].y()<<" vz::"<<traj->v_xyz[i].z()<<" heading::"<<traj->rpy[i].z());
    }*/
    return min_time;
}

int ca::SafeConnection::GetMaxStartID(std::vector<double> &v_safe_vec){
    int id=0;
    for(auto i:v_safe_vec){
        if(i<=0)
            return id;
        id++;
    }
    return id;
}

bool ca::SafeConnection::NextUnsafeState(ca::eml::State &q_unsafe_state, Eigen::Vector3d& current_pos, Eigen::Vector3d &wp0, Eigen::Vector3d &wp1,
                                         Eigen::Vector3d &center, double radius,double s,
                                         representation_interface::PoseGraphRepresentation &ri, double desired_speed,
                                         double max_speed, std::vector<double> &v_max_vec, std::vector<double> &s_vec){
    // we assume that the representation_interface does not change during this function call
    //ROS_INFO_STREAM("In next state");
    Eigen::Vector3d dir = wp1 - wp0; dir.normalize();
    double s_res = s_res_;
    Eigen::Vector3d vel = dir*max_speed;
    q_unsafe_state.frame = ri.get_frame();
    if(s_vec.size() == 0)
        GenerateSVec(s_vec, s_res, s);
   double safe_speed = 0.;
    for(auto i:s_vec){
        current_pos = StoPose(wp0, wp1, radius, center, i);
        //ROS_INFO_STREAM("In for::"<<"wp0.x::"<<wp0.x()<<" wp0.y::"<<wp0.y()<<" wp0.z::"<<wp0.z()<<" wp1.x::"<<wp1.x()<<" wp1.y::"<<wp1.y()<<" wp1.z::"<<wp1.z());
        //ROS_INFO_STREAM("c.x::"<<center.x()<<" c.y::"<<center.y()<<" c.z::"<<center.z()<<" radius::"<<radius<<" dist::"<<i);
        //ROS_INFO_STREAM("p.x::"<<current_pos.x()<<" p.y::"<<current_pos.y()<<" p.z::"<<current_pos.z());
        vel = max_speed* Pos2Dir(current_pos, center, dir);
        q_unsafe_state.x=current_pos.x(); q_unsafe_state.y=current_pos.y(); q_unsafe_state.z=current_pos.z();
        q_unsafe_state.roll=0.; q_unsafe_state.pitch=0.; q_unsafe_state.yaw=0.;
        q_unsafe_state.v_x=vel.x();
        q_unsafe_state.v_y=vel.y();
        q_unsafe_state.v_z=vel.z();
        q_unsafe_state.v_roll = 0; q_unsafe_state.v_pitch=0.; q_unsafe_state.v_yaw=0.;
        sfchk_.returnSafeVelocity(q_unsafe_state,safe_speed);
        //ROS_ERROR_STREAM("Safe speed::"<<safe_speed<<" Nominal speed::"<<desired_speed);
        v_max_vec.push_back(safe_speed);
        if((safe_speed -desired_speed) < -0.1 ){
                return true;
        }
    }
    return false;
}


int ca::SafeConnection::EndTurningDistance(double s, double sensor_range, std::vector<double>& s_vec){
    // we assume that the representation_interface does not change during this function call

    double s_dist=  std::max(0., (s-sensor_range+0.1*sensor_range));
    double prev_err = 0.;
    for(size_t i=0; i<s_vec.size(); i++){
        double err = std::fabs(s_dist-s_vec[i]);
        if(i==0)
            prev_err = err;
        if(err>prev_err)
            return i;
        prev_err = err;
    }
    return s_vec.size()-1;
}

// Trying to do heading optimization, which is quite interesting
// 1. there is a path, designated with velocities, now one has to find heading to match that
// 2. I can just check the states for the current speed, or can check for max speed, now checking for max speed is a problem as one might never satisfy it
// 3. So the plan can be to just check for the current state, again not fair, can we check for a nominal max v, yes. So if one has to iteratively improve, one can always do it again.
//4. Ok? ok.

// 1. for th first leg of the journey one cannot do better, so just point to the next unsafe state and compute desired heading
// 2. Now for the second leg of the journey, recompute the best path, assuming everything is aligned, as you can't do better
// 3. Repeat the process

// Now you can do this from the beginning, if you have a reasonable max velocity
// The other consideration is frame rate, the frame rate consideration means if you want a certain heading stay there for the frame rate.
// lets do this


bool ca::SafeConnection::SafeTrajectoryRoute(std::vector<ca::SafeRAORViewPoint> &vp_list,
                                             std::vector<size_t>& order,
                                             representation_interface::PoseGraphRepresentation &ri,
                                             double max_speed, Eigen::Vector3d &path_center,double heading_rate, bool aligned, bool optimize_heading, bool repeat_iteration,
                                             ca::eml::Trajectory &traj){

    size_t num_sensing_points_before = ri.sensing_points_.size();
    //ca::eml::Trajectory traj;
    if(order.size()<2) return false;
    double t = 0.; double del_t = 0.;
    bool reset_sensing  = false;
    int traj_idx = 1;
    int order_idx = 2;
    Eigen::Vector3d safe_position(0.,0.,0.); // Not being used right now, can be used to speed up
    double nominal_vel = max_speed;
    std::vector<size_t> fixed_heading_wp;
    fixed_heading_wp.push_back(0);
    double radius;
    for(size_t i=0; i< (order.size()-1); i++){
        ca::SafeRAORViewPoint &vp1 = vp_list[order[i]];
        ca::SafeRAORViewPoint &vp2 = vp_list[order[i+1]];
        Eigen::Vector3d temp_pos; float dummy_fl; Eigen::Vector3d temp_v;
        vp1.get_pose(temp_pos,dummy_fl,dummy_fl,temp_v);
        radius = (path_center - temp_pos).norm();
        del_t=  MinSafeTravelTime(vp1, vp2, max_speed, heading_rate, path_center,ri, &traj,  aligned, reset_sensing);// aligned is being passed here but it does not do anything, anything at all
                                                        // Trajectory is appended
        fixed_heading_wp.push_back(traj.xyz.size()-1);
        ROS_ERROR_STREAM("Ip traj size::"<<traj.xyz.size());
        if(optimize_heading)
            OptimizeHeading(traj, vp_list, order,
                             traj_idx, order_idx, safe_position,nominal_vel,fixed_heading_wp,heading_rate,ri,path_center,radius);

        t = t+ del_t;
        /*for(size_t i=0; i<fixed_heading_wp.size(); i++){
            ROS_INFO_STREAM("fxh::"<<i<<" id::"<<fixed_heading_wp[i]);
        }
        ROS_ERROR_STREAM("Post OptimizeHeading::"<<traj.xyz.size());

        for(size_t i=0; i<traj.xyz.size();i++){
            ROS_INFO_STREAM("["<<i<<"] x::"<<traj.xyz[i].x()<<" y::"<<traj.xyz[i].y()<<" z::"<<traj.xyz[i].z()
                            <<" vx::"<<traj.v_xyz[i].x()<<" vy::"<<traj.v_xyz[i].y()<<" vz::"<<traj.v_xyz[i].z()<<" heading::"<<traj.rpy[i].z());
        }*/


        ROS_INFO_STREAM("t::"<<t);
    }
    //ri.sensing_points_.erase(ri.sensing_points_.begin()+sensing_points_before,ri.sensing_points_.end());
    // Above line resets sensing, we do not need it right now
    return true;
}

bool ca::SafeConnection::OptimizeHeading(ca::eml::Trajectory &traj, std::vector<ca::SafeRAORViewPoint> &vp_list, std::vector<size_t>& order,
                                         int &traj_idx, int &order_idx, Eigen::Vector3d& safe_position, double nominal_vel, std::vector<size_t> &fixed_heading_id,
                                         double heading_rate,representation_interface::PoseGraphRepresentation &ri, Eigen::Vector3d &center, double radius){
    //ROS_ERROR_STREAM("Optimizing Heading");
    sfchk_.setRepresentationInterface(&ri);
    Eigen::Vector3d unsafe_pos;
    Eigen::Vector3d unsafe_vel;
    double final_heading;

    if(traj_idx < 0){
        ROS_ERROR_STREAM("traj Idx cannot be this small::"<<traj_idx);
        std::exit(-1);
    }

    if(order_idx < 2){
        ROS_ERROR_STREAM("OptimizeHeading order_idx is "<<order_idx);
        std::exit(-1);
    }
    if(traj_idx >= traj.xyz.size()){
        ROS_ERROR_STREAM("OptimizeHeading. Traj Id is greater than than traj size. Traj Idx:: "<<traj_idx<<" Traj Size::"<<traj.xyz.size());
        std::exit(-1);
    }

    Eigen::Vector3d pos,vel,pos1,vel1,dir;
    double heading; double safe_speed;
    double target_heading;
    double current_heading;
    ca::eml::State q_unsafe_state;
    float heading_fl;
    double dummy, s, updated_angle;
    float dummy_fl;
    std::vector<double> v_max_vec,s_vec;

    //ROS_ERROR_STREAM("Sizes::t::"<<traj.t.size()<<" xyz::"<<traj.xyz.size());
    int next_fixed_wp = 0;
    for(size_t i=(traj_idx+1); i<traj.xyz.size(); i++){
        //ROS_INFO_STREAM("i::"<<i<<" traj_idx::"<<traj_idx);
        current_heading = traj.rpy[i-1].z();
        final_heading = target_heading = FixedHeadingForOptimizeHeading(traj, i, fixed_heading_id, next_fixed_wp);

        for(size_t j=(traj_idx+1); j<traj.xyz.size(); j++){
            //ROS_INFO_STREAM("In for j::"<<j<<" traj_idx::"<<traj_idx);
            // Checking if trajectory is unsafe
            pos = traj.xyz[j];
            vel = traj.v_xyz[j];
            heading = traj.rpy[j].z();
            //ROS_INFO_STREAM("Before safe::"<<ri.sensing_points_.size()<<" v_x::"<<vel.x()<<" v_y::"<<vel.y()<<" v_z::"<<vel.z());
            safe_speed =  SafeSpeed(pos, vel, heading, ri);
            //ROS_INFO_STREAM("Beyond safe speed...::"<<safe_speed<<"vel::"<<vel.norm());
            if((safe_speed - vel.norm()) < -0.001){
                unsafe_pos = pos;
                unsafe_vel = vel;
                if(traj_idx < 1){
                    ROS_ERROR_STREAM("This should never have happened, there is some error in trajectory generation as 0 traj id is unsafe.");
                    return false;
                }
                Eigen::Vector3d target_pt = pos + vel*1.5; // This needs changing but currently should work. //TODO Change it over the weekend
                Eigen::Vector3d heading_vec = target_pt -traj.xyz[i];
                target_heading = std::atan2(heading_vec.y(),heading_vec.x());
                //ROS_ERROR_STREAM("Updated target heading.");
                break;
            }
            traj_idx++;
        }

        if((traj_idx+1)>= traj.xyz.size()){
            bool unsafe = false;
            while(!unsafe && order_idx<order.size()){
                s_vec.clear(); v_max_vec.clear(); s_vec.clear();
                //ROS_INFO_STREAM("In while::"<<order_idx);
                ca::SafeRAORViewPoint vp0 = vp_list[order[order_idx-1]];
                ca::SafeRAORViewPoint vp1 = vp_list[order[order_idx]];
                vp0.get_pose(pos,dummy_fl,heading_fl, vel);heading = heading_fl;
                vp1.get_pose(pos1,dummy_fl,heading_fl, vel1); dir = pos1 - pos; s = dir.norm();
                dir.normalize();// One can use current position here to speed it up
                //ROS_INFO_STREAM("RI size::"<<ri.sensing_points_.size());
                //ROS_INFO_STREAM("s::"<<s<<" pos x::"<<pos.x()<<" y::"<<pos.y()<<" z::"<<pos.z()<<" vel::"<<nominal_vel);
                Eigen::Vector3d pp = pos;
                unsafe = NextUnsafeState(q_unsafe_state, pp, pos, pos1,
                                         center, radius, s,ri, nominal_vel, nominal_vel, v_max_vec, s_vec);
                Eigen::Vector3d vel = q_unsafe_state.GetVelocity();
                //ROS_INFO_STREAM("s::"<<s<<" pos x::"<<pp.x()<<" y::"<<pp.y()<<" z::"<<pp.z()<<" vel::"<<vel.norm());
                if(unsafe){
                    //ROS_ERROR_STREAM("Unsafe::");
                    pos = q_unsafe_state.GetPose();
                    vel = q_unsafe_state.GetVelocity();
                    //ROS_INFO_STREAM("s::"<<s<<" dir x::"<<dir.x()<<" y::"<<dir.y()<<" z::"<<dir.z()
                    //                <<" pos x::"<<pos.x()<<" y::"<<pos.y()<<" z::"<<pos.z());
                    Eigen::Vector3d target_pt = pp;
                    Eigen::Vector3d heading_vec = target_pt -traj.xyz[i];
                    target_heading = std::atan2(heading_vec.y(),heading_vec.x());
                    //ROS_ERROR_STREAM("Updated target heading.");
                    break;
                }
                order_idx++;
            }
        }

        if(next_fixed_wp == i)
            target_heading = FixedHeadingForOptimizeHeading(traj, i, fixed_heading_id, next_fixed_wp);

        updated_angle = current_heading;
        double del_t = traj.t[i] - traj.t[i-1];
        UpdateAngle(updated_angle, target_heading, del_t, heading_rate);
        // if updated angle has greater angle diff, then switch the target heading
        double delt2final = traj.t[next_fixed_wp] - traj.t[i+1];
        double max_angle_diff_2end = heading_rate*delt2final;
        double angle_diff = std::fabs(AngleDiff(updated_angle,final_heading));
        if(angle_diff >= max_angle_diff_2end){
            //ROS_INFO_STREAM("Switching to final heading::"<<final_heading<<" heading_id::"<<next_fixed_wp);
            target_heading = final_heading;
            updated_angle = current_heading;
            UpdateAngle(updated_angle, target_heading, del_t, heading_rate);
        }
        traj.rpy[i].z() = updated_angle;
        //ROS_INFO_STREAM("["<<i<<"] current::"<<current_heading<<" final::"<<final_heading<<" Target::"<<target_heading<<" Updated::"<<updated_angle<<" next_fixed_wp::"<<next_fixed_wp);
        //ROS_INFO_STREAM("["<<i<<"] current::"<<current_heading<<" final::"<<final_heading<<" Target::"<<target_heading<<" Updated::"<<updated_angle<<" next_fixed_wp::"<<next_fixed_wp);
        int rep_resolution = 1;
        if(i%rep_resolution==0)
            ri.AddSensingPoint(traj.xyz[i],updated_angle);

        //ROS_ERROR_STREAM("Getting out of for::"<<i<<" traj_idx::"<<traj_idx);
    }
    //ROS_ERROR_STREAM("Out of OptimizeHeading");
    return true;
}

double ca::SafeConnection::SafeSpeed(Eigen::Vector3d& pos, Eigen::Vector3d& vel, double heading,representation_interface::PoseGraphRepresentation &ri){
    ca::eml::State q_state; double safe_speed;
    q_state.SetPose(pos); q_state.SetVelocity(vel);
    Eigen::Vector3d rpy(0,0,heading);q_state.SetRPY(rpy);
    sfchk_.setRepresentationInterface(&ri);
    //ROS_INFO_STREAM("Querying safe velocity::"<<vel.norm());
    sfchk_.returnSafeVelocity(q_state,safe_speed);
    //ROS_INFO_STREAM("Done Querying safe velocity");
    return safe_speed;
}
