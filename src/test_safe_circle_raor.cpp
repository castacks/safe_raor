﻿#include "safe_raor/safe_raor.h"
#include "eml_safety_enforcer/safety_check_ros_interface.h"
#include "emergency_maneuver_library/multirotor_emergency_maneuver_library.h"
#include "pose_graph_representation/pose_graph_representation.h"
#include "ros/ros.h"
#include "informative_path_planner_utils/grid_creator.h"
#include "visualization_msgs/MarkerArray.h"
#include "safe_raor/safe_connection.h"
#include "ca_nav_msgs/PathXYZVPsi.h"
#include "safe_raor/common_utils.h"
#include "rc_status_msgs/RCChannels.h"
#include "nav_msgs/Odometry.h"
#include "std_msgs/String.h"
#include "tf_utils/tf_utils.h"


// The strategy is that you get a viewpointpath
// get parameters from there
//generate the trajectory
// convert viwepointpath to path
// merge all three
bool publish_traj = false;
bool published_traj = false;
bool odo_recvd = false;
nav_msgs::Odometry lookahead_odo;

void RCCallback(const rc_status_msgs::RCChannels::ConstPtr& msg){
    int received_rc_mode = msg->mode;

    if(received_rc_mode == 8000 && !published_traj){
        publish_traj = true;
    }
    else{
        publish_traj = false;
        published_traj = false;
    }
}

void OdometryCallback(const nav_msgs::Odometry::ConstPtr& msg)
{
  lookahead_odo = *msg;
  odo_recvd = true;
}


void StringCallback(const std_msgs::String::ConstPtr& msg)
{
  publish_traj = true;
  published_traj = false;  
}

int main(int argc, char **argv)
{
  ros::init(argc, argv, "talker");
  ros::NodeHandle n("~");
  std::string pose_topic, forced_replan_topic, radio_topic;
  std::string op_path_topic, viz_topic;

  if (!n.getParam("pose_topic", pose_topic)){
    ROS_ERROR_STREAM("Could not get pose_topic parameter.");
    return -1;
  }

  if (!n.getParam("forced_replan_topic", forced_replan_topic)){
    ROS_ERROR_STREAM("Could not get pose_topic parameter.");
    return -1;
  }


  if (!n.getParam("radio_topic", radio_topic)){
    ROS_ERROR_STREAM("Could not get radio_topic parameter.");
    return -1;
  }

  if (!n.getParam("op_path_topic", op_path_topic)){
    ROS_ERROR_STREAM("Could not get op_path_topic parameter.");
    return -1;
  }

  if (!n.getParam("viz_topic", viz_topic)){
    ROS_ERROR_STREAM("Could not get viz_topic parameter.");
    return -1;
  }


  ros::Publisher marker_pub = n.advertise<visualization_msgs::MarkerArray>(viz_topic, 1000);
  ros::Publisher trajectory_pub = n.advertise<ca_nav_msgs::PathXYZVPsi>(op_path_topic, 1000);
  ros::Rate loop_rate(10);  


  ros::Subscriber lookahead_sub = n.subscribe(pose_topic, 1, OdometryCallback);
  ros::Subscriber replan_sub = n.subscribe(forced_replan_topic,1,StringCallback);
  ros::Subscriber rc_sub = n.subscribe(radio_topic,1,RCCallback);


// param center // start point // radius // height
  double center_x;
  if (!n.getParam("center_x", center_x)){
    ROS_ERROR_STREAM("Could not get center_x parameter.");
    return -1;
  }

  double center_y;
  if (!n.getParam("center_y", center_y)){
    ROS_ERROR_STREAM("Could not get center_y parameter.");
    return -1;
  }

  double center_z;
  if (!n.getParam("center_z", center_z)){
    ROS_ERROR_STREAM("Could not get center_z parameter.");
    return -1;
  }

  double start_point_x;
  if (!n.getParam("start_point_x", start_point_x)){
    ROS_ERROR_STREAM("Could not get start_point_x parameter.");
    return -1;
  }

  double start_point_y;
  if (!n.getParam("start_point_y", start_point_y)){
    ROS_ERROR_STREAM("Could not get start_point_y parameter.");
    return -1;
  }

  double start_point_z;
  if (!n.getParam("start_point_z", start_point_z)){
    ROS_ERROR_STREAM("Could not get start_point_z parameter.");
    return -1;
  }


  double param_height;
  if (!n.getParam("height", param_height)){
    ROS_ERROR_STREAM("Could not get height parameter.");
    return -1;
  }

  double path_radius;
  if (!n.getParam("path_radius", path_radius)){
    ROS_ERROR_STREAM("Could not get path_radius parameter.");
    return -1;
  }

  Eigen::Vector3d current_pos(0.,0.,0.);
  double heading = 0.;

  ca_ri::Polar_2D_Representation ri;
  double angular_resolution = CA_DEG2RAD*60;
  double max_radius = 2;
  double arc_size = CA_DEG2RAD*360;
  double arc_center = 0;

  Eigen::Vector3d center;
  int count = 0;
  double height = 0.;
  while(ros::ok()){
      //ROS_INFO_STREAM("Pub traj::"<<publish_traj<<" PubD::"<<published_traj<<" Odo::"<<odo_recvd);
      if(publish_traj && !published_traj && odo_recvd){
          published_traj = true;
          current_pos.x() = lookahead_odo.pose.pose.position.x;
          current_pos.y() = lookahead_odo.pose.pose.position.y;
          current_pos.z() = lookahead_odo.pose.pose.position.z;

          tf::Pose pose;
          tf::poseMsgToTF(lookahead_odo.pose.pose, pose);
          heading = tf::getYaw(pose.getRotation());

          std::vector<ca::SafeRAORViewPoint> vp_list;
          double x_res, y_res;
          x_res = y_res=5;
          center = current_pos + 1.5*path_radius*Eigen::Vector3d(cos(heading),sin(heading),0.);
          center.z() = 0.;
          height = std::fabs(lookahead_odo.pose.pose.position.z);
          ri.SetParams(angular_resolution,max_radius,arc_size,arc_center,center,height,"/world");
          VGPI vg;
          if(!vg.Initialize(&ri,0.0)){
              ROS_ERROR_STREAM("Could not initialize the viewpoint generator");
              exit(-1);
          }
          Eigen::Vector3d radius(path_radius,10.,path_radius + 8.);
          Eigen::Vector3d yaw((heading+M_PI),M_PI/3,(2*M_PI + (heading+M_PI) - M_PI/3));
          height = lookahead_odo.pose.pose.position.z;
          Eigen::Vector3d z(height,10.,(height+0.5));
          //ROS_INFO_STREAM("Height::"<<height<<" z.x()::"<<z.x()<<" z.y()::"<<z.y()<<" z.z()::"<<z.z());
          generate_global_cylinderical_vp_list(&vg, vp_list, radius,yaw, z, center);
          ROS_INFO_STREAM("Vp list size::"<<vp_list.size());

          for(size_t i=0; i<vp_list.size(); i++){
              Eigen::Vector3d pp, vel;
              float fov,heading;
              ca::SafeRAORViewPoint &vp = vp_list[i];
              vp.get_pose(pp,fov,heading,vel);
              ROS_INFO_STREAM("["<<i<<"] Pos::"<<pp.x()<<"::"<<pp.y()<<"::"<<pp.z()
                              <<" heading:"<<heading<<"vx::"<<vel.x()<<" vy::"<<vel.y()<<" vz::"<<vel.z());
              vel = Eigen::Vector3d(3,0,0);
              vp.set_velocity(vel);
          }
          ROS_INFO_STREAM("Vp list size::"<<vp_list.size());
          ca::SafeRAOR raor(&vg,vp_list);
          raor.Initialize(n);

          double budget = 100;

          ca_ri::PoseGraphRepresentation  pose_graph_rep;
          pose_graph_rep.SetParams(100000,100,(M_PI/4.5),22.0,"/world");

          ca::SafeConnection sfc;

          ca::eml::MultiRotorEmergencyManeuverLibrary hl_em;
          hl_em.setParams(n);
          sfc.sfchk_.setAbortPathLibrary(&hl_em);
          sfc.sfchk_.setRepresentationInterface(&pose_graph_rep);
          sfc.sfchk_.setVelocityResolution(0.2);
          sfc.sfchk_.setConsiderOccupied(0.9);
          sfc.sfchk_.setConsiderUnknown(0.1);
          sfc.sfchk_.setUnknownFractionAllowed(0.01);
          sfc.sfchk_.setMinReccomendedSpeed(0.);

          sfc.sensing_resolution_s_ = 1.0;
          sfc.dynamics_resolution_s_ = 1.0;
          sfc.heading_rate_rad_p_s_ = 1.57;
          sfc.max_z_speed_ = 2.0;
          sfc.max_acceleration_ = 1.0;
          sfc.sensor_range_m_ = 15.0;
          sfc.max_acceleration_=1.0;
          sfc.a_res_ = 0.2;
          sfc.s_res_ = 1.0;

          double max_speed = 3.;
          double heading_rate = 1.57;
          bool aligned = false;
          bool optimize_heading  = true;

          std::vector<size_t> order;
          for(size_t i=0; i<vp_list.size();i++){
              order.push_back(i);
          }


          Eigen::Vector3d spos,svel;
          float sheading_fl, sfov_fl;
          ca::SafeRAORViewPoint &vps = vp_list[0];
          vps.get_pose(spos,sfov_fl,sheading_fl,svel);

          spos.x() = spos.x()  - 12.*std::cos(sheading_fl);
          spos.y() = spos.y()  - 12.*std::sin(sheading_fl);

          pose_graph_rep.AddSensingPoint(spos,sheading_fl);
          ROS_INFO_STREAM("size of the representation interface::"<<pose_graph_rep.sensing_points_.size());
          ca::eml::Trajectory traj;
          center.z() = height;
          double time_taken= sfc.SafeTrajectoryRoute(vp_list,order,pose_graph_rep,
                                                      max_speed, center,heading_rate,aligned,optimize_heading,false,traj);
          ROS_INFO_STREAM("Traj size::"<<traj.xyz.size());
          ca_nav_msgs::PathXYZVPsi path_msg = EmlTraj2XYZVPsi(traj);
          ROS_INFO_STREAM("Path msg size::"<<path_msg.waypoints.size());
          trajectory_pub.publish(path_msg);
          visualization_msgs::MarkerArray ma;
          ma = raor.GetMarkers(& ri);
          ma.markers.push_back(pose_graph_rep.GetSectors(0));
          marker_pub.publish(ma);
      }
      ros::spinOnce();
      loop_rate.sleep();
      ++count;
  }

  return 0;
}
