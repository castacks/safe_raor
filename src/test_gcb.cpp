﻿#include "safe_raor/gcb.h"
#include "ros/ros.h"
#include "informative_path_planner_utils/grid_creator.h"
#include "visualization_msgs/MarkerArray.h"

typedef ca::SafeRAORViewPointOnPolarGridInterface VGPI;

namespace ca_ri = ca::representation_interface;

visualization_msgs::Marker MakePathMarker(ca::SafeRAORPath &path, std::string frame){
    visualization_msgs::Marker m;
    m.header.frame_id = frame;
    m.header.stamp = ros::Time();
    m.ns = "path";
    m.frame_locked = true;
    m.id = 0;
    m.type = visualization_msgs::Marker::LINE_STRIP;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = 0.0;
    m.pose.position.y = 0.0;
    m.pose.position.z = 0.0;
    m.pose.orientation.x = 0.0;
    m.pose.orientation.y = 0.0;
    m.pose.orientation.z = 0.0;
    m.pose.orientation.w = 1.0;
    m.scale.x = 0.1;
    m.scale.y = 1;
    m.scale.z = 1;
    m.color.a = 1.0; // Don't forget to set the alpha!
    m.color.r = 0.0;
    m.color.g = 1.0;
    m.color.b = 0.0;
    geometry_msgs::Point p;
    Eigen::Vector3d pos; float heading; float fov;
    for(size_t i=0; i< path.path_.size();i++){
        ca::ViewPoint &vp = path.global_vp_list_[path.path_[i]];
        vp.get_pose(pos,heading,fov);
        p.x = pos.x(); p.y = pos.y(); p.z = pos.z();
        m.points.push_back(p);
    }
    return m;
}


void generate_global_vp_list(VGPI *vg,std::vector<ca::SafeRAORViewPoint> &viewpoint_list, Eigen::Vector2d ori, Eigen::Vector2d x_dir, Eigen::Vector2d y_dir, double x_res, double y_res){

    ca::SafeRAORViewPoint vp;
    float heading = 0;
    float fov = M_PI/8;
    double default_height = 1;
    std::vector<Eigen::Vector2d> grid_points;
    ca::GetGrid(grid_points,ori,x_dir,y_dir,x_res,y_res);
    Eigen::Vector3d v;


    //global_vp_list_.push_back(vp);
    ROS_INFO_STREAM("Grid Size::"<<grid_points.size());
    for(size_t i=0;i<grid_points.size();i++)
    {
        v.x() = grid_points[i].x();
        v.y() = grid_points[i].y();
        v.z() = -default_height;
        double range = 100;
        Eigen::Vector3d center(0,0,0);
        if(vg->MakeViewPoint(vp,v,center,M_PI/8,range))
            viewpoint_list.push_back(vp);
        //vg->MakeViewPoint(vp,v,heading,M_PI/8,range);
    }

}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "talker");
  ros::NodeHandle n;
  ros::Publisher marker_pub = n.advertise<visualization_msgs::MarkerArray>("cylinder_planner", 1000);
  ros::Rate loop_rate(10);

  ca_ri::Polar_2D_Representation ri;
  double angular_resolution = CA_DEG2RAD*60;
  double max_radius = 2;
  double arc_size = CA_DEG2RAD*360;
  double arc_center = 0;
  double height = 2;
  Eigen::Vector3d center(0,0,0);
  ri.SetParams(angular_resolution,max_radius,arc_size,arc_center,center,height,"/world");
  VGPI vg;
  if(!vg.Initialize(&ri,0.0)){
      ROS_ERROR_STREAM("Could not initialize the viewpoint generator");
      exit(-1);
  }

  std::vector<ca::SafeRAORViewPoint> vp_list;
  double x_res, y_res;
  x_res = y_res=5;

  ca::SafeRAORViewPoint vp;
  Eigen::Vector3d end_point(7,-1,-1);
  double range = 100;

  generate_global_vp_list(&vg,vp_list, Eigen::Vector2d(-10,-10), Eigen::Vector2d(10,-10),
                          Eigen::Vector2d(-10,10), x_res, y_res);

  vg.MakeViewPoint(vp,end_point,0,M_PI/8,range);
  vp_list.push_back(vp);

  Eigen::Vector3d start_point(7,-1,-1.1);
  ROS_INFO_STREAM("Vp list size::"<<vp_list.size());
  ca::GeneralizedCostBenefit gcb(&vg,vp_list);

  double budget = 500;
  ca::SafeRAORPath path =
  gcb.GCB(start_point,0,M_PI/8,vp_list.size()-1,&ri,budget);
  ROS_ERROR_STREAM("Out of GCB");
  int count = 0;

  //Eigen::Vector3d end_point(7,-1,-1);

    //ri.FindIntersection()

  visualization_msgs::MarkerArray ma;
  while(ros::ok()){
      ma = gcb.GetMarkers(& gcb.path_.polar_interface_);
      visualization_msgs::Marker m = MakePathMarker(gcb.path_, ri.get_frame());
      ma.markers.push_back(m);
      marker_pub.publish(ma);
      ros::spinOnce();
      loop_rate.sleep();
      ++count;
  }

  return 0;
}
