#include "safe_raor/controller_utils.h"

void DisplayVectors(std::vector<double> & s_vec, std::vector<double> & v_vec, std::string  disp_string){
    ROS_INFO_STREAM(disp_string);
    for(size_t i=0; i<s_vec.size(); i++){
        ROS_INFO("%d :: %0.4f :: %0.4f",(int)i,s_vec[i],v_vec[i]);
    }
}


void BackProjectVelocityAtAcc(std::vector<double>& s_vec, double a, double vf, double v_max,std::vector<double>& v_back_vec){
    v_back_vec.clear();
    double v_back = 0.;
    double s = s_vec[s_vec.size()-1];
    for(size_t i=0;i<s_vec.size();i++){
        v_back = (vf*vf)+(2*a*(s-s_vec[i]));
        if(v_back < 0.)
            v_back = 0.;
        else{
            v_back = std::min(v_max,std::sqrt(v_back));
        }
        v_back_vec.push_back(v_back);
    }
    return;
}


void BackProjectVelocityAtAccMaxTime(std::vector<double>& s_vec, double a, double vf, std::vector<double>& v_back_vec){
    v_back_vec.clear();
    double v_back = 0.;
    double s = s_vec[s_vec.size()-1];
    for(size_t i=0;i<s_vec.size();i++){
        v_back = (vf*vf)-(2*a*(s-s_vec[i]));
        if(v_back <0.)
            v_back = 0.;
        else{
            v_back = std::sqrt(v_back);
        }
        v_back_vec.push_back(v_back);
    }
    return;
}

void ForwardProjectVelocityAtAccMaxTime(std::vector<double>& s_vec, double a, double v0, std::vector<double>& v_fwd_vec){
    v_fwd_vec.clear();
    double v_fwd = 0.;
    for(size_t i=0;i<s_vec.size();i++){
        v_fwd = (v0*v0)-(2*a*(s_vec[i]));
        if(v_fwd < 0.)
            v_fwd = 0.;
        else
            v_fwd = std::sqrt(v_fwd);

        v_fwd_vec.push_back(v_fwd);
    }
    return;
}


void ForwardProjectVelocityAtAcc(std::vector<double>& s_vec, double a, double v0, double v_max, std::vector<double>& v_fwd_vec){
    v_fwd_vec.clear();
    double v_fwd = 0.;
    for(size_t i=0;i<s_vec.size();i++){
        v_fwd = (v0*v0)+(2*a*(s_vec[i]));
        if(v_fwd < 0.)
            v_fwd = 0;
        else
            v_fwd = std::min(v_max, std::sqrt(v_fwd));
        v_fwd_vec.push_back(v_fwd);
    }
    return;
}

void TimeAcceleration(std::vector<double> &s_vec, std::vector<double> &v_vec, std::vector<double>& t_vec, std::vector<double>& a_vec){
    t_vec.clear();
    t_vec.push_back(0);
    a_vec.clear();
    double diff_s;
    double u,v,a;
    double del_t;
    for(size_t i=0; i<(s_vec.size()-1); i++){
        diff_s = s_vec[(i+1)] - s_vec[i];
        u = v_vec[i];
        v = v_vec[(i+1)];
        a = (v*v-u*u)/(2*diff_s);
        if(std::fabs(a) !=0){
            del_t = (v-u)/a;
        }
        else if(v<=0.){
            del_t = 0.;
        }
        else
            del_t = diff_s/v;

        t_vec.push_back(t_vec[i]+del_t);
        a_vec.push_back(a);
    }
    a_vec.push_back(0.);
}

void GenerateSVec(std::vector<double> &s_vec,double s_res, double s){
    for(double s_i=0.; s_i <= s; s_i = s_i + s_res){
        s_vec.push_back(s_i);
    }

    if(s < s_res || (std::fabs(s_vec[(s_vec.size()-1)] -s) > 0.0001)){
        s_vec.push_back(s);
    }
}

int BangBangDoubleIntegrator(double s, double v0, double vf, double a_max, double v_max, double s_res, double a_res, double min_time,
                              std::vector<double>& t_vec, std::vector<double>& a_vec, std::vector<double>& v_vec, std::vector<double>& s_vec, bool strict_min_time){
    int feasible = 0;
    double stopping_distance = (v0*v0)/(2*a_max);
    double max_time = 0;
    if(stopping_distance < s){
        max_time = 100000;
    }else{
        double final_speed = sqrt(v0*v0 - 2*a_max*s);
        max_time = (v0 - final_speed)/a_max;
    }
    if(max_time < min_time && strict_min_time){
        //debug - ROS_ERROR_STREAM("BangBangDoubleIntegrator: Infeasible, min_time::"<<min_time<<" max_time::"<<max_time);
        return -1;
    }

    //DisplayVectors(s_vec, v_vec, "BangBangDoubleIntegrator, check check::");
    for(double s_i=0.; s_i <= s; s_i = s_i + s_res){
        s_vec.push_back(s_i);
    }

    if(s < s_res || (std::fabs(s_vec[(s_vec.size()-1)] -s) > 0.0001)){
        s_vec.push_back(s);
    }

    //DisplayVectors(s_vec, s_vec, "BangBangDoubleIntegrator, SVEC::");
    std::vector<double> v_back_vec;
    std::vector<double> v_fwd_vec;

    std::vector<double> t_strict_vec;
    std::vector<double> a_strict_vec;
    std::vector<double> v_strict_vec;
    std::vector<double> s_strict_vec;

    //debug - ROS_INFO_STREAM("In BangBangDoubleIntegrator--");
    for(double a=a_max; a>=0.; a= a-a_res){
        //ROS_INFO_STREAM("Step 0 - back integration.");
        BackProjectVelocityAtAcc( s_vec, a, vf, v_max,v_back_vec);
        //ROS_INFO_STREAM("Step 1 - fwd integration.");
        ForwardProjectVelocityAtAcc(s_vec, a, v0, v_max, v_fwd_vec);
        //ROS_INFO_STREAM("Step 2 - find the merging point.");
        double merge_index = -1;
        for(size_t i=0;i<s_vec.size();i++){
            if(v_fwd_vec[i] >= v_back_vec[i] && ((v_fwd_vec[i] - v_back_vec[i])<=0.2)){
                merge_index = i;
                break;
            }
        }
        if (merge_index == -1){
            //debug - ROS_ERROR_STREAM("BangBangDoubleIntegrator: Infeasible, vf:"<<vf<<" v0:"<<v0<<" s:"<<s<<" a:"<<a);
            continue;
        }
        //Step 3 - Merging
        //debug - ROS_ERROR_STREAM("Merge Index=="<<merge_index<<"Size::"<<v_fwd_vec.size());
        double copy_index = merge_index+1;
        v_vec = v_fwd_vec;
        std::copy (v_fwd_vec.begin(), v_fwd_vec.begin()+copy_index, v_vec.begin());
        if(merge_index != (v_fwd_vec.size()-1)){
            std::copy (v_back_vec.begin()+copy_index+1, v_back_vec.end(), (v_vec.begin()+copy_index+1));
        }
        //Step4 - Time and Acceleration
        TimeAcceleration(s_vec, v_vec, t_vec, a_vec);
        //debug - ROS_WARN_STREAM("v_vec_0::"<<v_vec[0]<<" v_vec_f::"<<v_vec[(v_vec.size()-1)]<<" s_vec::"<<s_vec[(s_vec.size()-1)]<<" t_vec::"<<t_vec[(t_vec.size()-1)]);
        if((t_vec[t_vec.size()-1] - min_time) >= 0.){
            //DisplayVectors(s_vec, v_vec, "BangBangDoubleIntegrator, checking s_vec::");
            feasible = 1.0;
            return feasible;
        }
        else if(!strict_min_time){
            t_strict_vec = t_vec;
            a_strict_vec = a_vec;
            v_strict_vec = v_vec;
            s_strict_vec = s_vec;
        }
    }

    if(!strict_min_time && t_strict_vec.size()>0){
        t_vec = t_strict_vec;
        a_vec = a_strict_vec;
        v_vec = v_strict_vec;
        s_vec = s_strict_vec;
        feasible = 1.;
        return feasible;
    }
    return feasible;
}

int BangBangDoubleMaxTimeIntegrator(double s, double v0, double vf, double a_max, double v_max, double s_res, double a_res, double min_time,
                              std::vector<double>& t_vec, std::vector<double>& a_vec, std::vector<double>& v_vec, std::vector<double>& s_vec){
    int feasible = 0;
    double stopping_distance = (v0*v0)/(2*a_max);
    double max_time = 0;
    if(stopping_distance < s){
        max_time = 100000;
    }else{
        double final_speed = sqrt(v0*v0 - 2*a_max*s);
        max_time = (v0 - final_speed)/a_max;
    }
    if(max_time < min_time){
        //debug - ROS_ERROR_STREAM("BangBangDoubleMaxTimeIntegrator: Infeasible, min_time::"<<min_time<<" max_time::"<<max_time);
        return -1;
    }

    for(double s_i=0.; s_i <= s; s_i = s_i + s_res){
        s_vec.push_back(s_i);
    }

    if(s < s_res || (s_vec[(s_vec.size()-1)] < s)){
        s_vec.push_back(s);
    }

    std::vector<double> v_back_vec;
    std::vector<double> v_fwd_vec;
    //debug - ROS_INFO_STREAM("Starting for loop in MaxTime integrator.");
    for(double a=0; a<=a_max; a= a+a_res){
        //ROS_INFO_STREAM("Step 0 - back integration.");
        BackProjectVelocityAtAccMaxTime(s_vec, a, vf, v_back_vec);
        //ROS_INFO_STREAM("Step 1 - fwd integration.");
        ForwardProjectVelocityAtAccMaxTime(s_vec, a, v0, v_fwd_vec);
        //ROS_INFO_STREAM("Step 2 - find the merging point.");
        double merge_index = -1;
        for(size_t i=0;i<s_vec.size();i++){
            if(v_fwd_vec[i]<=v_back_vec[i] && ((v_back_vec[i] - v_fwd_vec[i])<=0.2)){
                merge_index = i;
                break;
            }
        }
        if (merge_index == -1){
            //debug - ROS_ERROR_STREAM("BangBangMaxTimeDoubleIntegrator: Infeasible, vf:"<<vf<<" v0:"<<v0<<" s:"<<s<<" a:"<<a);
            continue;
        }
        //ROS_INFO_STREAM("Step 3 - Merging.");
        if(v_back_vec[merge_index] > 0.){
            double copy_index = merge_index + 1;

            v_vec = v_fwd_vec;
            std::copy (v_fwd_vec.begin(), (v_fwd_vec.begin()+copy_index-1), v_vec.begin());
            std::copy ((v_back_vec.begin()+copy_index), v_back_vec.end(), (v_vec.begin()+copy_index));
            //ROS_INFO_STREAM("Step4 - Time and Acceleration.");
            TimeAcceleration(s_vec, v_vec, t_vec, a_vec);
            if((t_vec[t_vec.size()-1] - min_time) >= 0.){
                feasible = 1;
                return feasible;
            }else{
                //debug - ROS_ERROR_STREAM("BangBangMaxTimeDoubleIntegrator: No time constraint, vf:"<<vf<<" v0:"<<v0<<" s:"<<s<<" a:"<<a<<" t::"<<t_vec[t_vec.size()-1]);
            }
        }
        else{
            double time_epsilon = 0.5;
            double time_taken = 0.;
            while (abs(time_taken-min_time)>time_epsilon){
                //debug - ROS_INFO_STREAM("Step 5 - handling 0 velocity case.");
                double dist_left = s-s_vec[merge_index]; // Step1 find the 0 velocity while going forward
                std::vector<double> t_fwd_vec; std::vector<double> a_fwd_vec;
                TimeAcceleration(s_vec, v_fwd_vec, t_fwd_vec, a_fwd_vec);
                double min_time_temp = min_time - t_fwd_vec[merge_index]; // merge_index
                std::vector<double> t_after;
                std::vector<double> a_after;
                std::vector<double> v_after;
                std::vector<double> s_after;
                bool strict_min_time_constraint = true;
                if(v_fwd_vec[merge_index] <= 0.)
                    strict_min_time_constraint = false;

                if(BangBangDoubleIntegrator(dist_left, v_fwd_vec[merge_index], vf, a_max, v_max, s_res, a_res, min_time_temp,
                                              t_after, a_after, v_after, s_after,strict_min_time_constraint) != 1){
                    break;
                }

                //[ t_vec2, a_vec2, v_vec2, s_vec2 ] = bang_bang_double_integrator(dist_left, v_vec1(merge_index),vf, a_max, v_max, s_res, min_time_temp); % Step2 find a time optimal bang bang controller
                double final_time = t_fwd_vec[merge_index] + t_after[(t_after.size()-1)];
                //debug - ROS_INFO("1. Time t1:%.2f, t2:%.2f, time_taken:%.2f, min_time:%.2f \n",t_fwd_vec[merge_index],t_after[(t_after.size()-1)],
                        //final_time,min_time);
                double added_time = 0.;
                if (final_time < min_time  && (v_fwd_vec[merge_index] <= 0.)){
                    added_time = min_time - final_time;
                }
                final_time = final_time + added_time;
                //fprintf('2. Time t1:%.2f, t2:%.2f, time_taken:%.2f, min_time:%.2f \n',t_vec1(end),t_vec2(end),time_taken,min_time);
                if (std::fabs(final_time-min_time)<=time_epsilon){
                    added_time = t_fwd_vec[merge_index] + added_time;
                    //debug - ROS_INFO_STREAM("In here... 1");
                    for(size_t i=0; i<t_after.size();i++){
                        t_after[i] = added_time + t_after[i];
                    }
                    t_vec.resize(s_vec.size());
                    a_vec.resize(s_vec.size());
                    v_vec.clear();
                    v_vec.resize(s_vec.size());
                    double copy_index = merge_index+1;
                    //debug - ROS_INFO_STREAM("v_fwd_vec[0]::"<<v_fwd_vec[0]<<" v_fwd_vec[merge_index]::"<<v_fwd_vec[merge_index]);
                    //debug - ROS_INFO_STREAM("v_after[0]::"<<v_after[0]<<" v_after[merge_index+1]::"<<v_after[(merge_index+1)]);
                    //debug - ROS_INFO_STREAM("In here... final_time::"<<final_time);

                    std::copy(t_fwd_vec.begin(), (t_fwd_vec.begin() + copy_index), t_vec.begin());
                    std::copy(a_fwd_vec.begin(), (a_fwd_vec.begin() + copy_index-1), a_vec.begin());
                    std::copy(v_fwd_vec.begin(), (v_fwd_vec.begin() + copy_index), v_vec.begin());
                    //ROS_INFO_STREAM("v_vec[merge_index-1]::"<<*(v_vec.begin()+merge_index-1)
                    //                <<" v_vec[merge_index]::"<<*(v_vec.begin()+merge_index));

                    std::copy((t_after.begin()+1), t_after.end(),(t_vec.begin()+copy_index+1));
                    std::copy(a_after.begin(), a_after.end(), (a_vec.begin()+copy_index));
                    std::copy((v_after.begin()+1), v_after.end(), (v_vec.begin()+copy_index+1));
                    time_taken = final_time;
                    feasible = 1;

                }
                //fprintf('3. Time t1:%.2f, t2:%.2f, time_taken:%.2f, min_time:%.2f \n',t_vec1(end),t_vec2(end),time_taken,min_time);
                merge_index = merge_index - 1;
           }
            break;
        }
    }
    return feasible;
}

bool Vector2Path(std::vector<double>& x, std::vector<double>& y, nav_msgs::Path &path){
    //nav_msgs::Path path;
    path.header.stamp = ros::Time::now();
    path.header.seq = 0;
    path.header.frame_id = "";
    geometry_msgs::PoseStamped pose;
    pose.header = path.header;
    pose.pose.position.x = 0.;
    pose.pose.position.y = 0.;
    pose.pose.position.z = 0.;

    if(x.size() != y.size()){
        //debug - ROS_WARN_STREAM("Vector2Path:: X size::"<<x.size()<<" Y size::"<<y.size()<<" are not equal.");
        return false;
    }

    for(size_t i=0; i<x.size(); i++){
        pose.header.seq = i;
        pose.pose.position.x = x[i];
        pose.pose.position.y = y[i];
        path.poses.push_back(pose);
    }
    return true;
}


bool MergeTimeVectors(std::vector<double>& t_vec, std::vector<double>& t_vec_tp0, std::vector<double>& t_vec_tp1, std::vector<double>& t_vec_f ){
    double t_offset = t_vec_tp0[t_vec_tp0.size()-1];
    for(auto & i:t_vec_tp1){
        i=i + t_offset;
    }
    t_offset = t_vec_tp1[t_vec_tp1.size()-1];
    for(auto & i:t_vec_f)
        i = i+t_offset;
   //DisplayVectors(s_vec_tp, v_vec_tp, "About to Merge TP::");
   //DisplayVectors(s_vec_f, v_vec_f, "About to Merge F::");
   //debug - ROS_INFO("Merging Time");
   t_vec.resize((t_vec_tp0.size()+t_vec_tp1.size()-1 + t_vec_f.size()-1),0);
   std::copy(t_vec_tp0.begin(),t_vec_tp0.end(),t_vec.begin());
   std::copy(t_vec_tp1.begin()+1, t_vec_tp1.end(), t_vec.begin()+t_vec_tp0.size());
   std::copy(t_vec_f.begin()+1, t_vec_f.end(), t_vec.begin()+t_vec_tp0.size() + t_vec_tp1.size()-1);
    return true;
}

bool MergeDistVectors(std::vector<double>& s_vec, std::vector<double>& s_vec_tp0, std::vector<double>& s_vec_tp1, std::vector<double>& s_vec_f ){
    double s_offset = s_vec_tp0[s_vec_tp0.size()-1];
    for(auto & i:s_vec_tp1){
        i=i + s_offset;
    }
    s_offset = s_vec_tp1[s_vec_tp1.size()-1];
    for(auto & i:s_vec_f)
        i = i+s_offset;
   //DisplayVectors(s_vec_tp, v_vec_tp, "About to Merge TP::");
   //DisplayVectors(s_vec_f, v_vec_f, "About to Merge F::");
   //debug - ROS_INFO("Merging Time");
   s_vec.resize((s_vec_tp0.size()+s_vec_tp1.size()-1 + s_vec_f.size()-1),0);
   std::copy(s_vec_tp0.begin(),s_vec_tp0.end(),s_vec.begin());
   std::copy(s_vec_tp1.begin()+1, s_vec_tp1.end(), s_vec.begin()+s_vec_tp0.size());
   std::copy(s_vec_f.begin()+1, s_vec_f.end(), s_vec.begin()+s_vec_tp0.size() + s_vec_tp1.size()-1);
    return true;
}


bool MergeAccVectors(std::vector<double>& a_vec, std::vector<double>& a_vec_tp0, std::vector<double>& a_vec_tp1, std::vector<double>& a_vec_f ){
   //DisplayVectors(s_vec_tp, v_vec_tp, "About to Merge TP::");
   //DisplayVectors(s_vec_f, v_vec_f, "About to Merge F::");
   //debug - ROS_INFO("Merging Acceleration");
   a_vec.resize((a_vec_tp0.size()-1+a_vec_tp1.size()-1 + a_vec_f.size()),0);
   std::copy(a_vec_tp0.begin(), a_vec_tp0.end()-1, a_vec.begin());
   std::copy(a_vec_tp1.begin(), a_vec_tp1.end()-1, (a_vec.begin() + a_vec_tp0.size()-1));
   std::copy(a_vec_f.begin(), a_vec_f.end(), (a_vec.begin() + a_vec_tp0.size()-1 + a_vec_tp1.size()-1));
   return true;
}


bool MergeVelVectors(std::vector<double>& v_vec, std::vector<double>& v_vec_tp0, std::vector<double>& v_vec_tp1, std::vector<double>& v_vec_f ){
   //DisplayVectors(s_vec_tp, v_vec_tp, "About to Merge TP::");
   //DisplayVectors(s_vec_f, v_vec_f, "About to Merge F::");
   //debug - ROS_INFO("Merging Velocity");
   v_vec.resize((v_vec_tp0.size()+v_vec_tp1.size()-1 + v_vec_f.size()-1),0);
   std::copy(v_vec_tp0.begin(),v_vec_tp0.end(),v_vec.begin());
   std::copy(v_vec_tp1.begin()+1, v_vec_tp1.end(), (v_vec.begin()+v_vec_tp0.size()));
   std::copy(v_vec_f.begin()+1, v_vec_f.end(), (v_vec.begin()+v_vec_tp0.size() + v_vec_tp1.size()-1));
    return true;
}

// Here is the problem.
// there is a path with defined start and end configs and with a max speed
// for the connection to be safe independently, it has to be able to turn in the direction of the path.
// we give 2 turning points and the function return minimum time path
//TODO right now it does not respect v_max_vec
bool SafeConnect(double s, double v0, double min_time_tp0, double s_tp0, double min_time_f, double s_tp1,
                 double vf, double a_max, double v_max, double s_res, double a_res, std::vector<double>& t_vec,
                 std::vector<double>& a_vec, std::vector<double>& v_vec, std::vector<double>& s_vec, std::vector<double>& v_safe_vec){

    double init_v_tp0 = v_max;
    double init_v_tp1 = v_max;
    double init_v_f = vf;
    // say the min max velocities
    double v_tp0_min = 0.2;
    double v_tp1_min = 0.;
    double vf_min = 0.0;
    double v_res = 0.2;
    double min_time_tp1 = 0;
    std::vector<double> t_vec_tp0; std::vector<double> a_vec_tp0; std::vector<double> v_vec_tp0; std::vector<double> s_vec_tp0;
    std::vector<double> t_vec_tp1; std::vector<double> a_vec_tp1; std::vector<double> v_vec_tp1; std::vector<double> s_vec_tp1;
    std::vector<double> t_vec_f; std::vector<double> a_vec_f; std::vector<double> v_vec_f; std::vector<double> s_vec_f;

    int found_v_tp0 = 0;
    int found_v_tp1 = 0;
    int found_v_f = 0;
    double s_f = s - s_tp0 - s_tp1;
    if(s_f<0){
        //debug - ROS_ERROR_STREAM("SafeConnect s_f::"<<s_f<<" is negative. s::"<<s<<" s_tp0::"<<s_tp0<<" s_tp1::"<<s_tp1);
        return false;
    }

    for (double v_tp0 = init_v_tp0; v_tp0 >= v_tp0_min; v_tp0 = v_tp0-v_res){
        for (double v_tp1 = init_v_tp1; v_tp1 >= v_tp1_min; v_tp1 = v_tp1-v_res){
            found_v_tp0 = 0; found_v_tp1=0; found_v_f = 0;
            //debug - ROS_INFO_STREAM("Checking if VTP1 is feasible, since its most efficient as no min_time constraint is applied. \n");
            t_vec_tp1.clear(); a_vec_tp1.clear(); v_vec_tp1.clear(); s_vec_tp1.clear();
            int worked = BangBangDoubleIntegrator(s_tp1, v_tp0, v_tp1, a_max, v_max, s_res, a_res, min_time_tp1, t_vec_tp1, a_vec_tp1, v_vec_tp1, s_vec_tp1);
            if(worked != 1){
                //debug - ROS_WARN_STREAM("TP1 is infeasible, s_tp1::"<<s_tp1 <<" v_tp0::"<<v_tp0<<" v_tp1::"<<v_tp1);
                continue;
            }
            found_v_tp1 = 1;
            //VTP0
            //debug - ROS_INFO_STREAM("Checking if VTP0 is feasible s_tp0::"<<s_tp0<<" v0::"<<v0<<" v_tp0::"<<v_tp0);
            s_vec_tp0.clear(); t_vec_tp0.clear(); a_vec_tp0.clear(); v_vec_tp0.clear();
            worked = BangBangDoubleIntegrator(s_tp0, v0, v_tp0, a_max, v_max, s_res, a_res, min_time_tp0, t_vec_tp0, a_vec_tp0, v_vec_tp0, s_vec_tp0);
            if(worked == -1){
                //debug - ROS_INFO_STREAM("Early termination, since VTP0 is not feasible.");
                return false;
            }
            else if(worked == 1){
                //DisplayVectors(s_vec_tp, v_vec_tp, "MinTime BangBang for TP::");
                //debug - ROS_INFO("VTP0 After bang bang min time v0:%.2f, v_tp:%.2f, time taken:%2f, min_time:%2f \n",v_vec_tp0[0],v_tp0,t_vec_tp0[t_vec_tp0.size()-1],min_time_tp0);
                found_v_tp0=1;
            }
            else{
                //debug - ROS_WARN("Oh no, optimal trajectory TP0 is not feasible, lets try the max time bang for the same velocity. \n");
                //debug - ROS_INFO("VTP0:2 \n");
                s_vec_tp0.clear(); t_vec_tp0.clear(); a_vec_tp0.clear(); v_vec_tp0.clear();
                worked = BangBangDoubleMaxTimeIntegrator(s_tp0, v0, v_tp0, a_max, v_max, s_res, a_res, min_time_tp0,
                                                         t_vec_tp0, a_vec_tp0, v_vec_tp0, s_vec_tp0);
                if(worked == 1){
                    //DisplayVectors(s_vec_tp, v_vec_tp, "MaxTime BangBang for TP::");
                    //debug - ROS_INFO("VTP0:2 After bang bang max time v0:%.2f, v_tp:%.2f, time taken:%2f, min_time:%2f \n",v_vec_tp0[0],v_vec_tp0[v_vec_tp0.size()-1],t_vec_tp0[t_vec_tp0.size()-1],min_time_tp0);
                    found_v_tp0 = 1;
                }
                else{
                    continue;
                }
            }
            //VF
            for (double v_f = init_v_f; v_f>= vf_min; v_f = v_f -v_res){
                s_vec_f.clear(); t_vec_f.clear(); a_vec_f.clear(); v_vec_f.clear();
                //debug - ROS_INFO_STREAM("Checking if VF is feasible s_f::"<<s_f<<" v_tp1::"<<v_tp1<<" v_f::"<<v_f);
                worked = BangBangDoubleIntegrator(s_f, v_tp1, v_f, a_max, v_max, s_res, a_res, min_time_f, t_vec_f, a_vec_f, v_vec_f, s_vec_f);
                if(worked == -1){
                    //debug - ROS_INFO("VTP1 is not feasible, for VF, lets try something else.");
                    found_v_f = 0;
                    break;
                }
                if(worked == 1){
                    //DisplayVectors(s_vec_f, v_vec_f, "MinTime BangBang for F::");
                    //debug - ROS_INFO("VF1 After bang bang min time v_f:%.2f, v_tp:%.2f, time taken:%2f, min_time:%2f \n",v_f,v_tp1,t_vec_f[t_vec_f.size()-1],min_time_f);
                    found_v_f = 1;
                    break;
                }
                //debug - ROS_INFO_STREAM("IN IF VF::"<<v_f);
                //debug - ROS_INFO("Oh no, optimal trajectory  is not feasible, lets try the max time bang for the same velocity.");
                s_vec_f.clear(); t_vec_f.clear(); a_vec_f.clear(); v_vec_f.clear();
                worked = BangBangDoubleMaxTimeIntegrator(s_f, v_tp1, v_f, a_max, v_max, s_res, a_res, min_time_f,
                                                         t_vec_f, a_vec_f, v_vec_f, s_vec_f);
                if(worked){
                    //DisplayVectors(s_vec_f, v_vec_f, "MaxTime BangBang for F::");
                    //debug - ROS_INFO("VF2 After bang bang min time v_f:%.2f, v_tp:%.2f, time taken:%2f, min_time:%2f \n",v_f,v_tp1,
                             //t_vec_f[(t_vec_f.size()-1)],min_time_f);
                    found_v_f = 1;
                    break;
                }
            }
            if(found_v_f==1){
                //Merge
                t_vec.clear(); v_vec.clear(); a_vec.clear(); s_vec.clear();
                MergeTimeVectors(t_vec, t_vec_tp0, t_vec_tp1, t_vec_f );
                MergeAccVectors(a_vec, a_vec_tp0, a_vec_tp1, a_vec_f );
                MergeDistVectors(s_vec, s_vec_tp0, s_vec_tp1, s_vec_f );
                MergeVelVectors(v_vec, v_vec_tp0, v_vec_tp1, v_vec_f );
                return true;
            }
        }
    }

    return false;
}

void checkMerging(){
    std::vector<double> base_numbers;
    base_numbers.push_back(0); base_numbers.push_back(1); base_numbers.push_back(2); base_numbers.push_back(3);
    base_numbers.push_back(4); base_numbers.push_back(5); base_numbers.push_back(6); base_numbers.push_back(7);

    int merge_index = 0;
    for(size_t i=0; i<base_numbers.size();i++){
        if(base_numbers[i] == 5. ){
            merge_index = i;
            break;
        }
    }
    int copy_index = merge_index+1;
    std::vector<double> vec;
    vec.resize(base_numbers.size(),0);
    std::copy(base_numbers.begin(), base_numbers.begin()+copy_index, vec.begin());
    for(auto &i:vec)
        ROS_INFO_STREAM("vec "<<" ::"<<i);

    base_numbers[merge_index] = -10;
    std::copy(base_numbers.begin()+merge_index+1, base_numbers.end(), vec.begin()+merge_index+1);

    for(size_t i=0; i< vec.size(); i++){
        ROS_INFO_STREAM("vec "<<i<<" ::"<<vec[i]);
    }
}
