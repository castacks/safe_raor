#include "safe_raor/controller_utils.h"
int main(int argc, char **argv)
{
  ros::init(argc, argv, "connection_controller");
  ros::NodeHandle n;
  ros::ServiceClient client = n.serviceClient<plot_tool::PlotPath>("/plot_tool/draw_path");

  //ros::Publisher chatter_pub = n.advertise<std_msgs::String>("chatter", 1000);

  ros::Rate loop_rate(1);
  int count = 0;
  std::vector<double> x; x.push_back(0.); x.push_back(1.); x.push_back(1.5);
  std::vector<double> y; y.push_back(0.); y.push_back(1.); y.push_back(0.);
  double s =14.;
  double v0 = 3.;
  double vf = 3.;
  double a_max = 1.;
  double v_max = 3.;
  double s_res = 0.1;
  double a_res = 0.2;
  double min_time = 3.;
  double min_time_tp0 = 1.34;
  double s_tp0 = 4.2;
  double s_tp1 = 5.6;
  double v_tp = 3.0;
  double min_time_f = 1.34;

  std::vector<double> t_vec; std::vector<double> a_vec; std::vector<double> v_vec; std::vector<double> s_vec;
  std::vector<double> v_safe_vec;

  bool opt_turn_pt =  SafeConnect(s, v0, min_time_tp0, s_tp0, min_time_f, s_tp1, vf, a_max,
                                  v_max, s_res, a_res, t_vec, a_vec, v_vec, s_vec,v_safe_vec);

  DisplayVectors(s_vec, v_vec, "Optimized Answer::");

  DisplayVectors(s_vec, a_vec, "ACCCCCC::");

  DisplayVectors(s_vec, t_vec, "TTTT_VEC::");


  //checkMerging();
  while (ros::ok() && count <1)
  {
    plot_tool::PlotPath pp;
    pp.request.series = 0;
    pp.request.append = true;
    pp.request.symbol = 'o';
    pp.request.symbol_size = 3;

    if(!opt_turn_pt){
        ROS_ERROR_STREAM("OptimizeTurningPoints did not work. s_vec::"<<s_vec.size()<<" v_vec::"<<v_vec.size());
    }
    else if(Vector2Path(s_vec, v_vec, pp.request.msg)){
        client.call(pp);
        pp.request.series = 1;
        pp.request.msg.poses.clear();
        if(Vector2Path(s_vec, t_vec, pp.request.msg)){
            //client.call(pp);
            pp.request.series = 2;
            pp.request.msg.poses.clear();
            if(Vector2Path(s_vec, a_vec, pp.request.msg)){
                //client.call(pp);
            }
        }

    }
    else{
        ROS_ERROR_STREAM("s_vec and v_vec are not of equal size.");
    }
    ros::spinOnce();
    loop_rate.sleep();
    ++count;
  }


  return 0;
}
