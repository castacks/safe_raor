#include "safe_raor/common_utils.h"

/*double radius = 4000;
    {
        int idx0 = std::min(std::max(0.0,controlstate.closestIdx-1),(double)path.size()-1);
        int idx1 = std::min(std::max(0.0,controlstate.closestIdx),(double)path.size()-1);
        int idx2 = std::min(std::max(0.0,controlstate.closestIdx+1),(double)path.size()-1);


        State s0 = path.stateAt(idx0);
        State s1 = path.stateAt(idx1);
        State s2 = path.stateAt(idx2);

        double ci = 2*(s2.pose.position_m[1]*(s1.pose.position_m[0]-s0.pose.position_m[0]) + s1.pose.position_m[1]*(s0.pose.position_m[0]-s2.pose.position_m[0]) + s0.pose.position_m[1]*(s2.pose.position_m[0]-s1.pose.position_m[0]));
        double x_ic = (s0.pose.position_m[0]*s0.pose.position_m[0]+s0.pose.position_m[1]*s0.pose.position_m[1])*(s1.pose.position_m[1]-s2.pose.position_m[1]) +
                (s1.pose.position_m[0]*s1.pose.position_m[0]+s1.pose.position_m[1]*s1.pose.position_m[1])*(s2.pose.position_m[1]-s0.pose.position_m[1]) +
                (s2.pose.position_m[0]*s2.pose.position_m[0]+s2.pose.position_m[1]*s2.pose.position_m[1])*(s0.pose.position_m[1]-s1.pose.position_m[1]);

        double y_ic = (s0.pose.position_m[0]*s0.pose.position_m[0]+s0.pose.position_m[1]*s0.pose.position_m[1])*(s2.pose.position_m[0]-s1.pose.position_m[0]) +
                (s1.pose.position_m[0]*s1.pose.position_m[0]+s1.pose.position_m[1]*s1.pose.position_m[1])*(s0.pose.position_m[0]-s2.pose.position_m[0]) +
                (s2.pose.position_m[0]*s2.pose.position_m[0]+s2.pose.position_m[1]*s2.pose.position_m[1])*(s1.pose.position_m[0]-s0.pose.position_m[0]);

//        ROS_INFO("IDX %d %d %d %f",idx0,idx1,idx2,ci);

        if(ci!=0)
        {
            x_ic = x_ic/ci;
            y_ic = y_ic/ci;

            Eigen::Vector2d c,xi,r;
            c[0]=x_ic;
            c[1]=y_ic;

            xi[0]=s1.pose.position_m[0];
            xi[1]=s1.pose.position_m[1];

            r = c - xi;
            radius = r.norm();
//            ROS_INFO("Radius = %f",radius);
        }

    }
*/


double ConstrainAngle(double x){
    x = fmod(x + M_PI,2*M_PI);
    if (x < 0)
        x += 2*M_PI;
    return x - M_PI;
}
double AngleDiff(double a,double b){
    double dif = fmod(b - a + M_PI,2*M_PI);
    if (dif < 0)
        dif += 2*M_PI;
    return dif - M_PI;
}


double StoTheta(double &theta_s, double &theta_e, double radius, double s){
    double theta_diff = AngleDiff(theta_s, theta_e);
    double theta_sign = theta_diff/std::fabs(theta_diff);
    double del_theta = s/radius;
    return ConstrainAngle((theta_s + theta_sign*del_theta));

}

Eigen::Vector3d StoPose(Eigen::Vector3d& start, Eigen::Vector3d &end, double radius, Eigen::Vector3d &center, double s){
    Eigen::Vector3d v0 = start - center;
    Eigen::Vector3d v1 = end - center;
    double theta_s = std::atan2(v0.y(),v0.x());
    double theta_e = std::atan2(v1.y(),v1.x());
    double theta = StoTheta(theta_s, theta_e, radius, s);
    v1 = center + radius*Eigen::Vector3d(std::cos(theta) ,std::sin(theta) ,0.);
    return v1;

}

Eigen::Vector3d Pos2Dir(Eigen::Vector3d &pos, Eigen::Vector3d& center, Eigen::Vector3d& path_dir){

    Eigen::Vector3d v0 = pos - center;
    Eigen::Vector3d z(0,0,-1);
    v0 = v0.cross(z); v0.normalize();
    if(path_dir.dot(v0) > 0) // This assumes start and end are less than 90 degrees apart.
        return v0;
    else
        return -v0;
}


double ArcLength(Eigen::Vector3d& start, Eigen::Vector3d &end, double radius, Eigen::Vector3d &center){
    Eigen::Vector3d v0 = start - center;
    Eigen::Vector3d v1 = end - center;
    double theta_s = std::atan2(v0.y(),v0.x());
    double theta_e = std::atan2(v1.y(),v1.x());
    double del_angle = AngleDiff(theta_s, theta_e);
    return (del_angle*radius);
}

bool RadiusFromWayPoints(Eigen::Vector3d p0, Eigen::Vector3d p1 , Eigen::Vector3d p2, Eigen::Vector3d c, double &radius){
double ci = 2*(p2.y()*(p1.x()-p0.x()) + p1.y()*(p0.x()-p2.x()) + p0.y()*(p2.x()-p1.x()));
double x_ic = (p0.x()*p0.x()+p0.y()*p0.y())*(p1.y()-p2.y()) +
        (p1.x()*p1.x() + p1.y()*p1.y())*(p2.y()-p0.y()) +
        (p2.x()*p2.x() + p2.y()*p2.y())*(p0.y()-p1.y());

double y_ic = (p0.x()*p0.x()+p0.y()*p0.y())*(p2.x()-p1.x()) +
        (p1.x()*p1.x() + p1.y()*p1.y())*(p0.x()-p2.x()) +
        (p2.x()*p2.x() + p2.y()*p2.y())*(p1.x()-p0.x());

    ROS_INFO_STREAM("ci"<<ci);
    if(ci!=0){
        x_ic = x_ic/ci;
        y_ic = y_ic/ci;

        Eigen::Vector3d c,xi,r;
        c.x()=x_ic;
        c.y()=y_ic;
        c.z() = p0.z();

        xi.x()=p1.x();
        xi.y()=p1.y();
        xi.z()=p1.z();

        r = c - xi;
        radius = r.norm();
        ROS_INFO_STREAM("Radius ="<<radius);

    }
    return false;
}

bool ConcatenatePath(ca_nav_msgs::PathXYZVPsi &path0, ca_nav_msgs::PathXYZVPsi &path1){
    for(auto i:path1.waypoints){
        path0.waypoints.push_back(i);
    }
    return true;
}

bool DeConstructGlobalPlannerOp(ca_nav_msgs::PathXYZVViewPoint& ip_path, ca_nav_msgs::PathXYZVPsi& op_path0, ca_nav_msgs::PathXYZVPsi& op_path2,
                                Eigen::Vector3d& car_pos, double& radius, double &height, double &heading){
    size_t id_p0=0; size_t id_p2 = 0;
    for(size_t i=0; i< ip_path.waypoints.size(); i++){
        ca_nav_msgs::XYZVViewPoint &wp= ip_path.waypoints[i];
        if(wp.safety == ca_nav_msgs::XYZVViewPoint::SAFETY_MODE_VIEWPOINT){
            if(i>0){
                id_p0 = i-1;
            }
            car_pos.x() = wp.viewpoint.x; car_pos.y() = wp.viewpoint.y; car_pos.z() = wp.viewpoint.z;
            Eigen::Vector3d p; p.x() = wp.position.x; p.y() = wp.position.y; p.z() = wp.position.z;
            Eigen::Vector3d r_vec = (p-car_pos); r_vec.z() = 0.;
            radius = r_vec.norm();
            height = car_pos.z() - p.z();
            heading = std::atan2((car_pos.y() - p.y()),(car_pos.x() - p.x()));
            for(size_t j=i+1; j < ip_path.waypoints.size();j++){
                ca_nav_msgs::XYZVViewPoint &wp1 = ip_path.waypoints[j];
                //ROS_INFO_STREAM("wp::"<<j<<" wp1.safety::"<<wp1.safety);
                if(wp1.safety != ca_nav_msgs::XYZVViewPoint::SAFETY_MODE_VIEWPOINT){
                    id_p2 = j;
                    break;
                }
            }
            ROS_ERROR_STREAM("convert ip_path0::"<<id_p0);
            ca_nav_msgs::PathXYZVViewPoint vp_ip_path0; vp_ip_path0.header = ip_path.header;
            for(size_t ii=0; ii<id_p0;ii++){
                vp_ip_path0.waypoints.push_back(ip_path.waypoints[ii]);
            }
            if(vp_ip_path0.waypoints.size()>0){
                op_path0 = ca_nav_msgs::ConvertViewPointPath2XYZVPsi(vp_ip_path0);
                op_path0.header = ip_path.header;
            }
            else
                ROS_ERROR_STREAM("Local Planner:: Empty path before the target.");

            ROS_ERROR_STREAM("convert ip_path2::"<<id_p2);
            if(id_p2 == ip_path.waypoints.size() || id_p2 ==0){
                ROS_ERROR_STREAM("Local Planner::Error:: This should have never happened, ip path size is the same as id_p2"<<id_p2<<"::"<<ip_path.waypoints.size());
                std::exit(-1);
            }
            ca_nav_msgs::PathXYZVViewPoint vp_ip_path2; vp_ip_path2.header = ip_path.header;
            for(size_t ii=id_p2; ii<ip_path.waypoints.size();ii++){
                vp_ip_path2.waypoints.push_back(ip_path.waypoints[ii]);
            }
            if(vp_ip_path2.waypoints.size()>1){
                op_path2 = ca_nav_msgs::ConvertViewPointPath2XYZVPsi(vp_ip_path2);
                op_path2.header = ip_path.header;
            }
            else
                ROS_ERROR_STREAM("Local Planner::Error:: Empty path after target.");

            //ROS_INFO_STREAM("Exiting the DeConstructGlobalPlannerOp");
            return true;
        }
    }
    op_path0 = ca_nav_msgs::ConvertViewPointPath2XYZVPsi(ip_path);
    op_path0.header = ip_path.header;
    return false;
}

ca_nav_msgs::PathXYZVPsi EmlTraj2XYZVPsi( ca::eml::Trajectory &traj){
    ca_nav_msgs::PathXYZVPsi path;
    path.header.frame_id = "/world";
    path.header.stamp = ros::Time::now();
    ca_nav_msgs::XYZVPsi wp;
    ca_nav_msgs::XYZVPsi prev_wp;
    double heading_threshold = 0.0001;
    double distance_threshold = 0.00001;
    double velocity_threshold = 0.0001;

//TODO the conversion should threshold heading/position/velocity
    for(size_t i=0; i< traj.xyz.size(); i++){
        wp.vel = traj.v_xyz[i].norm();
        if(!std::isfinite(wp.vel))
            ROS_ERROR_STREAM(i<<"th vel is inf.");

        if(wp.vel<1e-3)
            continue;
        wp.heading = traj.rpy[i].z();
        if(!std::isfinite(wp.heading))
            ROS_ERROR_STREAM(i<<"th heading is inf.");

        wp.position.x = traj.xyz[i].x();
        if(!std::isfinite(wp.position.x))
            ROS_ERROR_STREAM(i<<"th position x is inf.");

        wp.position.y = traj.xyz[i].y();
        if(!std::isfinite(wp.position.y))
            ROS_ERROR_STREAM(i<<"th position y is inf.");

        wp.position.z = traj.xyz[i].z();
        if(!std::isfinite(wp.position.z))
            ROS_ERROR_STREAM(i<<"th position z is inf.");
        /*if(path.waypoints.size()==0){
            wp.position.x = 0.;
            wp.position.y = 0.;
            wp.position.z = 0.;
        }*/
        bool add = false;
        if(heading_threshold < std::fabs(wp.heading - prev_wp.heading)){
            add = true;
        }

        if(add || std::fabs(wp.position.x - prev_wp.position.x) > distance_threshold
               || std::fabs(wp.position.y - prev_wp.position.y) > distance_threshold
               || std::fabs(wp.position.z - prev_wp.position.z) > distance_threshold){
            add = true;
        }

        if(add ||std::fabs(wp.vel - prev_wp.vel)> velocity_threshold){
            add = true;
        }

        if(i==0 || add){
            path.waypoints.push_back(wp);
            prev_wp = wp;
        }
    }
    return path;
}

visualization_msgs::Marker MakePathMarker(ca::SafeRAORPath &path, std::string frame){
    visualization_msgs::Marker m;
    m.header.frame_id = frame;
    m.header.stamp = ros::Time();
    m.ns = "path";
    m.frame_locked = true;
    m.id = 0;
    m.type = visualization_msgs::Marker::LINE_STRIP;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = 0.0;
    m.pose.position.y = 0.0;
    m.pose.position.z = 0.0;
    m.pose.orientation.x = 0.0;
    m.pose.orientation.y = 0.0;
    m.pose.orientation.z = 0.0;
    m.pose.orientation.w = 1.0;
    m.scale.x = 0.1;
    m.scale.y = 1;
    m.scale.z = 1;
    m.color.a = 1.0; // Don't forget to set the alpha!
    m.color.r = 0.0;
    m.color.g = 1.0;
    m.color.b = 0.0;
    geometry_msgs::Point p;
    Eigen::Vector3d pos; float heading; float fov;
    for(size_t i=0; i< path.path_.size();i++){
        ca::ViewPoint &vp = path.global_vp_list_[path.path_[i]];
        vp.get_pose(pos,heading,fov);
        p.x = pos.x(); p.y = pos.y(); p.z = pos.z();
        m.points.push_back(p);
    }
    return m;
}

visualization_msgs::Marker MakeVPListMarker(std::vector<ca::SafeRAORViewPoint> &viewpoint_list, std::string frame){
    visualization_msgs::Marker m;
    m.header.frame_id = frame;
    m.header.stamp = ros::Time();
    m.ns = "vp";
    m.frame_locked = true;
    m.id = 0;
    m.type = visualization_msgs::Marker::SPHERE;
    m.action = visualization_msgs::Marker::ADD;
    m.pose.position.x = 0.0;
    m.pose.position.y = 0.0;
    m.pose.position.z = 0.0;
    m.pose.orientation.x = 0.0;
    m.pose.orientation.y = 0.0;
    m.pose.orientation.z = 0.0;
    m.pose.orientation.w = 1.0;
    m.scale.x = 0.1;
    m.scale.y = 1;
    m.scale.z = 1;
    m.color.a = 1.0; // Don't forget to set the alpha!
    m.color.r = 0.0;
    m.color.g = 1.0;
    m.color.b = 0.0;
    geometry_msgs::Point p;
    Eigen::Vector3d pos; float heading; float fov;
    for(size_t i=0; i< viewpoint_list.size();i++){
        ca::ViewPoint &vp = viewpoint_list[i];
        vp.get_pose(pos,heading,fov);
        p.x = pos.x(); p.y = pos.y(); p.z = pos.z();
        m.points.push_back(p);
    }
    return m;
}

void generate_global_vp_list(VGPI *vg,std::vector<ca::SafeRAORViewPoint> &viewpoint_list, Eigen::Vector2d ori, Eigen::Vector2d x_dir, Eigen::Vector2d y_dir,
                             double x_res, double y_res, int mode, double height, Eigen::Vector3d center){

    ca::SafeRAORViewPoint vp;
    float heading = 0;
    float fov = M_PI/8;
    double default_height = 1;
    std::vector<Eigen::Vector2d> grid_points;
    ca::GetGrid(grid_points,ori,x_dir,y_dir,x_res,y_res);
    Eigen::Vector3d v;


    //global_vp_list_.push_back(vp);
    ROS_INFO_STREAM("Grid Size::"<<grid_points.size());
    for(size_t i=0;i<grid_points.size();i++)
    {
        v.x() = grid_points[i].x();
        v.y() = grid_points[i].y();
        v.z() = -height;
        double range = 100;
        if(mode == 0){
        if(vg->MakeViewPoint(vp,v,center,M_PI/8,range))
            viewpoint_list.push_back(vp);}
        else{
            vg->MakeViewPoint(vp,v,heading,M_PI/8,range);
        }
    }

}


void generate_global_cylinderical_vp_list(VGPI *vg,std::vector<ca::SafeRAORViewPoint> &viewpoint_list,
                                          Eigen::Vector3d radius, Eigen::Vector3d yaw, Eigen::Vector3d z,
                                          Eigen::Vector3d ori){
    double radius_s = radius.x();
    double radius_res = radius.y();
    double radius_e = radius.z();

    double yaw_s = yaw.x();
    double yaw_res = yaw.y();
    double yaw_e = yaw.z();

    double z_s = z.x();
    double z_res = z.y();
    double z_e = z.z();

    ca::SafeRAORViewPoint vp;
    float heading = 0;
    float fov = M_PI/8;
    double default_height = 1;
    std::vector<Eigen::Vector3d> grid_points;
    //ca::GetGrid(grid_points,ori,x_dir,y_dir,x_res,y_res);
    ca::GetCylinderGrid(grid_points,radius_s, radius_e, radius_res, yaw_s,yaw_res, yaw_e,
                        z_s,z_e,z_res,ori);
    Eigen::Vector3d v;

    //global_vp_list_.push_back(vp);
    ROS_INFO_STREAM("Grid Size::"<<grid_points.size());
    for(size_t i=0;i<grid_points.size();i++)
    {
        v = grid_points[i];
        double range = 100;
        if(vg->MakeViewPoint(vp,v,ori,M_PI/8,range))
            viewpoint_list.push_back(vp);
    }

}

void generate_global_vp_list_csv(VGPI *vg, std::vector<ca::SafeRAORViewPoint> &viewpoint_list, std::string csv_file,
                                 int mode, Eigen::Vector3d center){
    ca::SafeRAORViewPoint vp;
    io::CSVReader<6> in(csv_file);
    in.read_header(io::ignore_extra_column, "x", "y", "z", "heading", "fov", "range");
    double x,y,z,heading,fov,range;
    while(in.read_row(x,y,z,heading,fov,range)){
      Eigen::Vector3d v(x,y,z);
      if(mode = 0){
          if(vg->MakeViewPoint(vp,v,center,fov,range))
              viewpoint_list.push_back(vp);
      }
      else{
          vg->MakeViewPoint(vp,v,heading,fov,range);
          viewpoint_list.push_back(vp);
      }
    }
}

ca::eml::Trajectory TestPath2Traj(std::vector<ca::SafeRAORViewPoint> &vp_list,
                   std::vector<size_t>& order,
                   double max_speed){
    ca::eml::Trajectory traj;
    if(order.size()<2) return traj;

    for(size_t i=0; i< order.size(); i++){
        ca::SafeRAORViewPoint& vp = vp_list[order[i]];
        Eigen::Vector3d pos; float dummy; float heading;
        Eigen::Vector3d dummy_v;
        vp.get_pose(pos, dummy, heading, dummy_v);
        traj.xyz.push_back(pos);
        traj.rpy.push_back(Eigen::Vector3d(0,0,heading));
        traj.v_xyz.push_back(Eigen::Vector3d(max_speed,0,0));
    }
    return traj;
}
