#include "safe_raor/reward.h"
#include <iostream>
namespace ca_ri = ca::representation_interface;
double ca::Reward::RewardFunction(size_t new_nodeId,std::vector<ca::SafeRAORViewPoint>& global_vp_list ,ca_ri::RepresentationInterface<double,3> *ri)
{
    ca::SafeRAORViewPoint& vp = global_vp_list[new_nodeId];
    //ROS_INFO_STREAM("In reward function::"<<vp.visible_primitives_.size());
    double reward = 0;
    double value = 0;
    int num_uncovered = 0;
    //std::cout<<"Got some primitives::"<<vp.visible_primitives_.size()<<"\n";
    for(size_t i=0; i<vp.visible_primitives_.size(); i++){
        Eigen::Vector3i vi(vp.visible_primitives_[i],0,0);
        if(ri->GetValue(vi,value)){
            if(value<=0){
                num_uncovered = num_uncovered+1;
            }
        }
    }

    return num_uncovered;
}

double ca::Reward::RewardFunction(ca_ri::RepresentationInterface<double,3> *old_ri,ca_ri::RepresentationInterface<double,3> *new_ri)
{
    size_t i=0;
    Eigen::Vector3i id(0,0,0);
    double old_value=0;
    double new_value=0;
    double reward = 0;
    while(old_ri->GetValue(id,old_value) && new_ri->GetValue(id,new_value) ){
        if(old_value<=0 && new_value>old_value)
            reward = reward + 1;//(new_value - old_value);

        i=i+1;
        id.x() = i;
    }

    return reward;
}
