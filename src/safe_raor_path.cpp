#include "safe_raor/safe_raor_path.h"

bool ca::SafeRAORPath::IsInPath(size_t q, size_t& id){
    std::vector<size_t>::iterator it;

    it = std::find (path_.begin(), path_.end(), q);
    if (it == path_.end())
        return false;

    id = it - path_.begin();
    return true;
}

double ca::SafeRAORPath::CalculatePathLength(std::vector<size_t>&path, std::vector<SafeRAORViewPoint>& global_vp_list){
    path_length_ = 0;
    if(path_.size() < 2 )
        return 0;

    for(size_t i=0; i<(path.size()-1);i++){
       Eigen::Vector3d& v1 = global_vp_list[path[i]].vp_pose_.position_;
       Eigen::Vector3d& v2 = global_vp_list[path[i+1]].vp_pose_.position_;
       Eigen::Vector3d dd = v1-v2;       
       path_length_ += dd.norm();
    }

    return path_length_;

}

bool ca::SafeRAORPath::AddNode(size_t node, bool update_representation){
    size_t dummy;
    if(IsInPath(node,dummy))
        return false;

    if(!update_representation)
        representation_updated_ = false;
    else if(path_.size()==0){
        representation_updated_=true;
    }

    if(update_representation && !representation_updated_ && (path_.size()>0)){
        ROS_ERROR_STREAM("SafeRAORPath::AddNode:: Asked to update the representation mid-way,"
                         <<" this functionality is not supported due to high computation");
        std::exit(-1);
    }

    ca::ViewPoint& vp = global_vp_list_[node];
    path_.push_back(node);
    if(update_representation){
        //std::cout<<"Got some primitives::"<<vp.visible_primitives_.size()<<"\n";
        for(size_t i=0; i<vp.visible_primitives_.size(); i++){
            Eigen::Vector3i vi(vp.visible_primitives_[i],0,0);
            double value;
            polar_interface_.GetValue(vi,value);
            value = value+1;
            polar_interface_.SetID(vp.visible_primitives_[i],value);
        }
    }
    return true;
}

bool ca::SafeRAORPath::DeleteNode(size_t node, bool update_representation){
    size_t dummy;
    if(IsInPath(node, dummy)){
        path_.erase(path_.begin()+dummy);
        if(!update_representation)
            representation_updated_ = false;
        else if(!representation_updated_){
                ROS_ERROR_STREAM("SafeRAORPath::DeleteNode:: Asked to update the representation mid-way,"
                                 <<" this functionality is not supported due to high computation");
                std::exit(-1);
            }

        if(update_representation){
            ca::ViewPoint& vp = global_vp_list_[node];

            for(size_t i=0; i<vp.visible_primitives_.size(); i++){
                Eigen::Vector3i vi(vp.visible_primitives_[i],0,0);
                double value;
                polar_interface_.GetValue(vi,value);
                if(value>0)
                    value = value-1;
                polar_interface_.SetID(vp.visible_primitives_[i],value);
            }
        }
        return true;
    }
return false;
}

bool ca::SafeRAORPath::UpdateRepresentation(){
    if(representation_updated_){
        ROS_ERROR_STREAM("SafeRAORPath::Update Representation -- I am being asked to update a"
                         <<"representation that was already updated, this is a sign of trouble");
        return true;
    }

    //ROS_INFO_STREAM("global_vp_list_size::"<<global_vp_list_.size());
    for(size_t j=0; j<path_.size();j++){
        ca::ViewPoint& vp = global_vp_list_[path_[j]];
        for(size_t i=0; i<vp.visible_primitives_.size(); i++){
            //std::cout<<"Got some primitives::"<<vp.visible_primitives_.size()<<"\n";
            Eigen::Vector3i vi(vp.visible_primitives_[i],0,0);
            double value;
            polar_interface_.GetValue(vi,value);
            value = value+1;
            polar_interface_.SetID(vp.visible_primitives_[i],value);
        }
        //ROS_INFO_STREAM("Path::"<<path_[j]<<"::"<<path_.size()<<"::"<<j);
    }
    representation_updated_ = true;
    //ROS_INFO_STREAM("Outside the update");
    return true;
}

bool ca::SafeRAORPath::AddNodes(std::vector<size_t> ids, bool representation_update){
    size_t dummy;
    for(size_t i=0; i<ids.size(); i++){
        if(!IsInPath(ids[i], dummy)){
            if(representation_update)
                AddNode(ids[i],representation_update);
            else
                path_.push_back(ids[i]);
        }
    }
    representation_updated_ = representation_update;

    return true;
}


bool ca::SafeRAORPath::ResetOrder(std::vector<size_t> &new_order){
    std::vector<size_t> new_path = path_;
    //std::cout<<"\n New Order::";
    for(size_t i=0; i<new_order.size(); i++){
        new_path[i] = path_[new_order[i]];
        //std::cout<<new_path[i]<<"-->";
    }
    //std::cout<<"\n";
    path_ = new_path;
    return true;
}

double ca::SafeRAORPath::DistanceFromPath(std::vector<size_t>&path, std::vector<SafeRAORViewPoint>& global_vp_list, Eigen::Vector3d& p){
    if(path_.size() ==0 )
        return 0;
    Eigen::Vector3d pp = p;
    if(path_.size() == 1){
        Eigen::Vector3d v =
                global_vp_list[path[0]].vp_pose_.position_ - pp;
        return v.norm();
    }


    double d = 1000000.0;
    for(size_t i=0;i<path_.size()-1;i++){
        Eigen::Vector3d v1 =
                global_vp_list[path[i]].vp_pose_.position_;
        Eigen::Vector3d v2 =
                global_vp_list[path[i+1]].vp_pose_.position_;
        d= std::min(d,DistanceFromLineSegment(v1,v2,pp));
    }

    return d;
}

double ca::SafeRAORPath::DistanceFromLineSegment(Eigen::Vector3d p1, Eigen::Vector3d p2, Eigen::Vector3d p){

        Eigen::Vector3d a = p2-p1;
        Eigen::Vector3d b = p-p1;

        // if p1 and p2 are the same
        if(a.norm() < 0.00001){
            return b.norm();
        }

        double d = (a.cross(b)).norm()/a.norm();

        Eigen::Vector3d aa = p1-p2;
        Eigen::Vector3d bb = p-p2;

        double d1 = b.norm();
        double d2 = bb.norm();

        if(a.dot(b) <0 || aa.dot(bb)<0) // Checking for obtuse angles
            d = std::min(d1,d2);

        return d;
}

double ca::SafeRAORPath::CostOfAdding(std::vector<size_t>&path, std::vector<ca::SafeRAORViewPoint>& global_vp_list, Eigen::Vector3d& p){
    return 2*DistanceFromPath(path, global_vp_list, p);
}

ca::SafeRAORPath& ca::SafeRAORPath::operator=(SafeRAORPath& a){
    this->global_vp_list_ = a.global_vp_list_;
    this->path_ = a.path_;
    this->sorted_path_ = a.sorted_path_;
    this->path_length_ = a.path_length_;
    this->path_reward_ = a.path_reward_;
    this->polar_interface_ = a.polar_interface_;
    this->ri_ = &this->polar_interface_;
    this->rep_state_initialized_ = a.rep_state_initialized_;
    this->representation_updated_ = a.representation_updated_;
    return *this;
}

ca::SafeRAORPath::SafeRAORPath(const SafeRAORPath &a):
global_vp_list_(a.global_vp_list_)
{
    //global_vp_list_ = a.global_vp_list_;
    path_ = a.path_;
    sorted_path_ = a.sorted_path_;
    path_length_ = a.path_length_;
    path_reward_ = a.path_reward_;
    polar_interface_ = a.polar_interface_;
    ri_ = &polar_interface_;
    rep_state_initialized_ = a.rep_state_initialized_;
    representation_updated_ = a.representation_updated_;
}
