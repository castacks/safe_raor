﻿#include "safe_raor/safe_raor.h"
#include "eml_safety_enforcer/safety_check_ros_interface.h"
#include "emergency_maneuver_library/multirotor_emergency_maneuver_library.h"
#include "pose_graph_representation/pose_graph_representation.h"
#include "ros/ros.h"
#include "informative_path_planner_utils/grid_creator.h"
#include "visualization_msgs/MarkerArray.h"
#include "safe_raor/safe_connection.h"
#include "ca_nav_msgs/PathXYZVPsi.h"
#include "safe_raor/common_utils.h"
#include "rc_status_msgs/RCChannels.h"
#include "nav_msgs/Odometry.h"
#include "std_msgs/String.h"
#include "tf_utils/tf_utils.h"


bool recv_traj = false;
bool pubd_traj = false;

ca_nav_msgs::PathXYZVViewPoint ip_path;

void PathViewPointCallback(const ca_nav_msgs::PathXYZVViewPoint::ConstPtr& msg){
    ip_path = *msg;
    recv_traj = true;
    pubd_traj = false;
}


int main(int argc, char **argv)
{
  ros::init(argc, argv, "talker");
  ros::NodeHandle n("~");
  std::string op_path_topic, viz_topic, ip_path_topic;

  if (!n.getParam("op_path_topic", op_path_topic)){
    ROS_ERROR_STREAM("Could not get op_path_topic parameter.");
    return -1;
  }

  if (!n.getParam("viz_topic", viz_topic)){
    ROS_ERROR_STREAM("Could not get viz_topic parameter.");
    return -1;
  }

  if (!n.getParam("ip_path_topic", ip_path_topic)){
    ROS_ERROR_STREAM("Could not get ip_path_topic parameter.");
    return -1;
  }

  ros::Publisher marker_pub = n.advertise<visualization_msgs::MarkerArray>(viz_topic, 1000);
  ros::Publisher trajectory_pub = n.advertise<ca_nav_msgs::PathXYZVPsi>(op_path_topic, 1000);
  ros::Subscriber path_sub = n.subscribe(ip_path_topic, 1, PathViewPointCallback);

  ros::Rate loop_rate(10);  


  Eigen::Vector3d current_pos(0.,0.,0.);
  double heading = 0.;

  ca_ri::Polar_2D_Representation ri;
  double angular_resolution = CA_DEG2RAD*60;
  double max_radius = 2;
  double arc_size = CA_DEG2RAD*360;
  double arc_center = 0;

  Eigen::Vector3d center;
  int count = 0;
  double path_height = 0.;
  double path_radius = 0.;
  double path_heading = 0.;

  ca::SafeConnection sfc;

  ca::eml::MultiRotorEmergencyManeuverLibrary hl_em;
  hl_em.setParams(n);
  sfc.sfchk_.setAbortPathLibrary(&hl_em);
  sfc.sfchk_.setVelocityResolution(0.2);
  sfc.sfchk_.setConsiderOccupied(0.9);
  sfc.sfchk_.setConsiderUnknown(0.1);
  sfc.sfchk_.setUnknownFractionAllowed(0.01);
  sfc.sfchk_.setMinReccomendedSpeed(0.);

  sfc.sensing_resolution_s_ = 1.0;
  sfc.dynamics_resolution_s_ = 1.0;
  sfc.heading_rate_rad_p_s_ = 1.57;
  sfc.max_z_speed_ = 2.0;
  sfc.max_acceleration_ = 1.0;
  sfc.sensor_range_m_ = 15.0;
  sfc.max_acceleration_=1.0;
  sfc.a_res_ = 0.2;
  sfc.s_res_ = 1.0;


  while(ros::ok()){
      //ROS_INFO_STREAM("Pub traj::"<<publish_traj<<" PubD::"<<published_traj<<" Odo::"<<odo_recvd);
      ca_ri::PoseGraphRepresentation  pose_graph_rep;
      sfc.sfchk_.setRepresentationInterface(&pose_graph_rep);
      visualization_msgs::MarkerArray ma;

      if(recv_traj && !pubd_traj){
          ca_nav_msgs::PathXYZVPsi op_path0, op_path2;
          if(!DeConstructGlobalPlannerOp(ip_path, op_path0, op_path2, center, path_radius, path_height, path_heading)){
              trajectory_pub.publish(op_path0);
              pubd_traj = true;
          }
          else{
              ROS_INFO_STREAM("Radius::"<<path_radius <<" Height::"<<path_height <<" Heading::"<<path_heading);
              std::vector<ca::SafeRAORViewPoint> vp_list;
              //center = current_pos + 1.5*path_radius*Eigen::Vector3d(cos(path_heading),sin(path_heading),0.);
              ri.SetParams(angular_resolution,max_radius,arc_size,arc_center,center,path_height,"/world");
              VGPI vg;
              if(!vg.Initialize(&ri,0.0)){
                  ROS_ERROR_STREAM("Could not initialize the viewpoint generator");
                  exit(-1);
              }
              Eigen::Vector3d radius(path_radius,10.,path_radius + 8.);
              Eigen::Vector3d yaw((heading+M_PI),M_PI/3,(2*M_PI + (heading+M_PI)));
              Eigen::Vector3d z(-path_height,10.,(-path_height+0.5));
              //ROS_INFO_STREAM("Height::"<<height<<" z.x()::"<<z.x()<<" z.y()::"<<z.y()<<" z.z()::"<<z.z());
              generate_global_cylinderical_vp_list(&vg, vp_list, radius,yaw, z, center);
              ROS_INFO_STREAM("Vp list size::"<<vp_list.size());

              for(size_t i=0; i<vp_list.size(); i++){
                  Eigen::Vector3d pp, vel;
                  float fov,heading;
                  ca::SafeRAORViewPoint &vp = vp_list[i];
                  vp.get_pose(pp,fov,heading,vel);
                  ROS_INFO_STREAM("["<<i<<"] Pos::"<<pp.x()<<"::"<<pp.y()<<"::"<<pp.z()
                                  <<" heading:"<<heading<<"vx::"<<vel.x()<<" vy::"<<vel.y()<<" vz::"<<vel.z());
                  vel = Eigen::Vector3d(3,0,0);
                  vp.set_velocity(vel);
              }
              ROS_INFO_STREAM("Vp list size::"<<vp_list.size());
              ca::SafeRAOR raor(&vg,vp_list);
              raor.Initialize(n);

              double budget = 100;
              ca_ri::PoseGraphRepresentation  pose_graph_rep;
              pose_graph_rep.SetParams(100000,100,(M_PI/4.5),22.0,"/world");

              double max_speed = 3.;
              double heading_rate = 1.57;
              bool aligned = false;
              bool optimize_heading  = true;              
              std::vector<size_t> order;
              for(size_t i=0; i<vp_list.size();i++){
                  order.push_back(i);
              }

              Eigen::Vector3d spos,svel;
              float sheading_fl, sfov_fl;
              ca::SafeRAORViewPoint &vps = vp_list[0];
              vps.get_pose(spos,sfov_fl,sheading_fl,svel);

              spos.x() = spos.x()  - 12.*std::cos(sheading_fl);
              spos.y() = spos.y()  - 12.*std::sin(sheading_fl);

              pose_graph_rep.AddSensingPoint(spos,sheading_fl);
              ROS_INFO_STREAM("size of the representation interface::"<<pose_graph_rep.sensing_points_.size());
              center.z() = center.z() - path_height;
              ca::eml::Trajectory traj;
              double time_taken= sfc.SafeTrajectoryRoute(vp_list,order,pose_graph_rep,
                                                          max_speed, center,heading_rate,aligned,optimize_heading,false,traj);
              ROS_INFO_STREAM("Traj size::"<<traj.xyz.size());
              ca_nav_msgs::PathXYZVPsi path_msg = EmlTraj2XYZVPsi(traj);
              ROS_INFO_STREAM("Path msg size::"<<path_msg.waypoints.size()<<" Op path size::"<<op_path0.waypoints.size());
              if(op_path0.waypoints.size()>0 && path_msg.waypoints.size()>0){
                  ca_nav_msgs::XYZVPsi &wp0 = op_path0.waypoints[op_path0.waypoints.size()-1];
                  ca_nav_msgs::XYZVPsi &wp1 = path_msg.waypoints[0];
                  Eigen::Vector3d p0(wp0.position.x,wp0.position.y, wp0.position.z);
                  Eigen::Vector3d p1(wp1.position.x,wp1.position.y, wp1.position.z);
                  Eigen::Vector3d dir = p1-p0; double distance = dir.norm();
                  ROS_ERROR_STREAM("Local Planner::Distance::"<<distance);
                  if(distance > 15.)
                      distance = distance-7.5;
                  else
                      distance = distance*0.5;
                  dir.normalize();
                  ROS_ERROR_STREAM("Local Planner::Distance::"<<distance);
                  dir = distance*dir;
                  p0 = p0 + dir;
                  double heading = std::atan2(dir.y(),dir.x());
                  ca_nav_msgs::XYZVPsi n_wp;
                  n_wp = wp0; n_wp.position.x = p0.x(); n_wp.position.y = p0.y(); n_wp.position.z = p0.z();
                  n_wp.heading = heading;
                  op_path0.waypoints.push_back(n_wp);
              }
              ConcatenatePath(op_path0, path_msg); ConcatenatePath(op_path0, op_path2);
              op_path0.header = ip_path.header;
              trajectory_pub.publish(op_path0);
              pubd_traj = true;
              ma = raor.GetMarkers(& ri);
          }
          ma.markers.push_back(pose_graph_rep.GetSectors(0));
          marker_pub.publish(ma);
      }
      else if(ip_path.waypoints.size() > 0){
          if(count%10 ==0)
            ROS_WARN_STREAM("Local planner recvd a global trajectory.");
      }
      else{
          if(count%10 ==0)
            ROS_WARN_STREAM("Local planner:: Waiting for trajectory.");
      }
      ros::spinOnce();
      loop_rate.sleep();
      ++count;
  }

  return 0;
}
