﻿#include "safe_raor/safe_raor.h"
#include "eml_safety_enforcer/safety_check_ros_interface.h"
#include "emergency_maneuver_library/multirotor_emergency_maneuver_library.h"
#include "pose_graph_representation/pose_graph_representation.h"
#include "ros/ros.h"
#include "informative_path_planner_utils/grid_creator.h"
#include "visualization_msgs/MarkerArray.h"
#include "safe_raor/safe_connection.h"
#include "ca_nav_msgs/PathXYZVPsi.h"
#include "safe_raor/common_utils.h"


int main(int argc, char **argv)
{
  ros::init(argc, argv, "talker");
  ros::NodeHandle n("~");
  ros::Publisher marker_pub = n.advertise<visualization_msgs::MarkerArray>("cylinder_planner", 1000);
  ros::Publisher trajectory_pub = n.advertise<ca_nav_msgs::PathXYZVPsi>("op_path", 1000);
  ros::Rate loop_rate(10);  


// param center // start point // radius // height
  double center_x;
  if (!n.getParam("center_x", center_x)){
    ROS_ERROR_STREAM("Could not get center_x parameter.");
    return -1;
  }

  double center_y;
  if (!n.getParam("center_y", center_y)){
    ROS_ERROR_STREAM("Could not get center_y parameter.");
    return -1;
  }

  double center_z;
  if (!n.getParam("center_z", center_z)){
    ROS_ERROR_STREAM("Could not get center_z parameter.");
    return -1;
  }

  double start_point_x;
  if (!n.getParam("start_point_x", start_point_x)){
    ROS_ERROR_STREAM("Could not get start_point_x parameter.");
    return -1;
  }

  double start_point_y;
  if (!n.getParam("start_point_y", start_point_y)){
    ROS_ERROR_STREAM("Could not get start_point_y parameter.");
    return -1;
  }

  double start_point_z;
  if (!n.getParam("start_point_z", start_point_z)){
    ROS_ERROR_STREAM("Could not get start_point_z parameter.");
    return -1;
  }


  double height;
  if (!n.getParam("height", height)){
    ROS_ERROR_STREAM("Could not get height parameter.");
    return -1;
  }

  double path_radius;
  if (!n.getParam("path_radius", path_radius)){
    ROS_ERROR_STREAM("Could not get path_radius parameter.");
    return -1;
  }


  ca_ri::Polar_2D_Representation ri;
  double angular_resolution = CA_DEG2RAD*60;
  double max_radius = 2;
  double arc_size = CA_DEG2RAD*360;
  double arc_center = 0;
  Eigen::Vector3d center(center_x,center_y,center_z);
  ri.SetParams(angular_resolution,max_radius,arc_size,arc_center,center,std::fabs(height)+10.,"/world");
  VGPI vg;
  if(!vg.Initialize(&ri,0.0)){
      ROS_ERROR_STREAM("Could not initialize the viewpoint generator");
      exit(-1);
  }

  std::vector<ca::SafeRAORViewPoint> vp_list;
  double x_res, y_res;
  x_res = y_res=5;

  ca::SafeRAORViewPoint vp;
  Eigen::Vector3d start_point(start_point_x,start_point_y,start_point_z);
  Eigen::Vector3d end_point=start_point;
  double range = 1000;

  //generate_global_vp_list(&vg,vp_list, Eigen::Vector2d(-10,-10), Eigen::Vector2d(10,-10),
  //                        Eigen::Vector2d(-10,10), x_res, y_res);

  Eigen::Vector3d radius(path_radius,10.,path_radius + 8.);
  Eigen::Vector3d yaw(0,M_PI/3,(2*M_PI - M_PI/3));
  Eigen::Vector3d z(height,10.,height+0.5);
  generate_global_cylinderical_vp_list(&vg, vp_list, radius,yaw, z, center);
  ROS_INFO_STREAM("Vp list size::"<<vp_list.size());
  vg.MakeViewPoint(vp,end_point,0,M_PI/8,range);
  vp_list.push_back(vp);

  ROS_INFO_STREAM("Vp list size::"<<vp_list.size());
  ca::SafeRAOR raor(&vg,vp_list);
  raor.Initialize(n);

  double budget = 100;
  ca::SafeRAORPath path = raor.Run(start_point,0,M_PI/8,vp_list.size()-1,&ri,budget);
  ROS_ERROR_STREAM("Out of RAOR - there is a bug, there is no way that there are "<< raor.path_vec_.size()<<"paths, sort it out");
  int count = 0;

  ca_ri::PoseGraphRepresentation  pose_graph_rep;
  pose_graph_rep.SetParams(1,1.5,M_PI/3,15.0,"/world");

  ca::SafeConnection sfc;


  ca::eml::MultiRotorEmergencyManeuverLibrary hl_em;
  hl_em.setParams(n);

  sfc.sfchk_.setAbortPathLibrary(&hl_em);
  sfc.sfchk_.setRepresentationInterface(&pose_graph_rep);
  sfc.sfchk_.setVelocityResolution(0.2);
  sfc.sfchk_.setConsiderOccupied(0.9);
  sfc.sfchk_.setConsiderUnknown(0.1);
  sfc.sfchk_.setUnknownFractionAllowed(0.01);
  sfc.sfchk_.setMinReccomendedSpeed(0.);

  sfc.sensing_resolution_s_ = 0.4;
  sfc.dynamics_resolution_s_ = 0.5;
  sfc.heading_rate_rad_p_s_ = 1.0;
  sfc.max_z_speed_ = 2.0;
  sfc.max_acceleration_ = 1.0;

  while(ros::ok() && count<5){
      ros::spinOnce();
      loop_rate.sleep();
      ++count;
  }

  ca::eml::Trajectory traj; //= sfc.ComputeSafeTrajectoryRoute(vp_list,path.path_,pose_graph_rep,1.0,0.5);
  //ca::eml::Trajectory traj  =  TestPath2Traj(vp_list,path.path_,1);
  //Eigen::Vector3d end_point(7,-1,-1);
  ROS_INFO_STREAM("Traj size::"<<traj.xyz.size());
  ca_nav_msgs::PathXYZVPsi path_msg = EmlTraj2XYZVPsi(traj);
  ROS_INFO_STREAM("Path msg size::"<<path_msg.waypoints.size());
    //ri.FindIntersection()
  trajectory_pub.publish(path_msg);
  visualization_msgs::MarkerArray ma;
  while(ros::ok()){
      ma = raor.GetMarkers(& path.polar_interface_);
      //ma = raor.GetMarkers(& ri);
      visualization_msgs::Marker m = MakePathMarker(path, ri.get_frame());
      ma.markers.push_back(m);
      marker_pub.publish(ma);
      ros::spinOnce();
      loop_rate.sleep();
      ++count;
  }

  return 0;
}
