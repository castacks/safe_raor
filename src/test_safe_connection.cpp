#include "eml_safety_enforcer/safety_check_ros_interface.h"
#include "emergency_maneuver_library/multirotor_emergency_maneuver_library.h"
#include "pose_graph_representation/pose_graph_representation.h"
#include "safe_raor/safe_connection.h"
#include "safe_raor/safe_raor.h"


namespace ca_ri=ca::representation_interface;
typedef ca::SafeRAORViewPointOnPolarGridInterface VGPI;

int main(int argc, char **argv)
{
  ros::init(argc, argv, "safety_check");
  ros::NodeHandle n("~");

  ros::Publisher marker_pub = n.advertise<visualization_msgs::MarkerArray>("pose_graph_representation", 1000);
  ros::Rate loop_rate(10);
  int count = 0;
  visualization_msgs::MarkerArray ma;
  //visualization_msgs::Marker m;


  ca_ri::PoseGraphRepresentation  pose_graph_rep;
  pose_graph_rep.SetParams(100000,100,M_PI/2,20.0,"/world");
 //TODO: Add cylinders -- Done
  //pose_graph_rep.AddCylinders(Eigen::Vector3d(0,100002.75,0));
  //pose_graph_rep.AddCylinders(Eigen::Vector3d(0,-100002.75,0));
    pose_graph_rep.AddSensingPoint(Eigen::Vector3d(-1,0.,0.),0.);

//  ma = pose_graph_rep.GetCylinder(0);
//  m = pose_graph_rep.GetSectors(0);
//  ma.markers.push_back(m);

  ca::SafeConnection sfc;


  ca::eml::MultiRotorEmergencyManeuverLibrary hl_em;
  hl_em.setParams(n);

  sfc.sfchk_.setAbortPathLibrary(&hl_em);
  sfc.sfchk_.setRepresentationInterface(&pose_graph_rep);
  sfc.sfchk_.setVelocityResolution(0.2);
  sfc.sfchk_.setConsiderOccupied(0.9);
  sfc.sfchk_.setConsiderUnknown(0.1);
  sfc.sfchk_.setUnknownFractionAllowed(0.01);
  sfc.sfchk_.setMinReccomendedSpeed(0.);

  sfc.sensing_resolution_s_ = 0.4;
  sfc.dynamics_resolution_s_ = 0.4;
  sfc.heading_rate_rad_p_s_ = 1.57;
  sfc.max_z_speed_ = 2.0;
  sfc.max_acceleration_ = 1.0;
  sfc.sensor_range_m_ = 20.0;
  sfc.max_acceleration_=1.0;
  sfc.a_res_ = 0.2;
  sfc.s_res_ = 0.4;

//  double safeVelocity = 0;

//  ca::eml::State q_state;
//  q_state.frame = "/world";
//  q_state.x=5.; q_state.y=0.; q_state.z=-0.3;
//  q_state.roll=0.; q_state.pitch=0.; q_state.yaw=1.57;
//  q_state.v_x =0.; q_state.v_y=10.; q_state.v_z=0.;
//  q_state.v_roll = 0; q_state.v_pitch=0.; q_state.v_yaw=0.;

//  std::vector<ca::eml::Trajectory> trajectoryVector =
//          safetycheck.abortLibrary->getEmergencyManeuverLibraryState(q_state);
//  std::vector<ca::eml::Trajectory> safeTrajectoryVector = safetycheck.returnSafeVelocity(q_state,safeVelocity);

//  ROS_ERROR_STREAM("Trajectory Vector::"<<trajectoryVector.size());
//  ROS_ERROR_STREAM("SafeVelocity::"<<safeVelocity<<"m/s");

//  safetycheck.generateMarkersSafeUnSafeTrajectories(ma,&safeTrajectoryVector,&trajectoryVector);
  ca::SafeRAORViewPoint vp;
  Eigen::Vector3d pos(0,0,0);
  double heading = M_PI/3;
  double fov = M_PI/3;
  Eigen::Vector3d v(0.,0.,0.); v.x() = 3.;
  vp.ResetViewPoint(pos,heading,fov,v);
  ca::SafeRAORViewPoint vp1;
  pos.x() = 0.; pos.y() = 14.; pos.z() = 0.;
  v.x() = 3.;
  vp1.ResetViewPoint(pos,-M_PI/3,fov,v);
  double max_speed = 3.;
  double heading_rate = 1.57;
  ca::eml::Trajectory traj;
  bool aligned = false;
  bool reset_sensing  = false;
  ROS_INFO_STREAM("size of the representation interface::"<<pose_graph_rep.sensing_points_.size());
  double time_taken;// = sfc.MinSafeTravelTime(vp, vp1, max_speed, heading_rate, pose_graph_rep,
                      //                    &traj, aligned, reset_sensing);
  //double time_taken;// = sfc.ComputeMinSafeTravelTimeAligned(vp,vp1,7.0,1.0,pose_graph_rep,&traj,true);
  ROS_INFO_STREAM("size of the trajectory::"<<traj.xyz.size());
  ROS_INFO_STREAM("size of the representation interface::"<<pose_graph_rep.sensing_points_.size());
  size_t i=traj.xyz.size()-1;
  //i = std::max((int)traj.xyz.size()-1,(int)i);
  //ma = sfc.DisplayRepresentationandSafety(pose_graph_rep,traj,i);
  i=0;
  //while(ros::ok())
  {
    //sfcr.process();
     double init_time = ros::Time::now().toSec();
    ROS_INFO_STREAM(time_taken);

    loop_rate.sleep();
    double time_diff = ros::Time::now().toSec() - init_time;
    ROS_INFO_STREAM("Loop Time::"<<time_diff);
    count++;
    marker_pub.publish(ma);
    ros::spinOnce();
    //ma = sfc.DisplayRepresentationandSafety(pose_graph_rep,traj,i);
    i++;
    time_diff = ros::Time::now().toSec() - init_time;
    ROS_INFO_STREAM("Loop Time::"<<time_diff);
  }
  return 0;
  
}
