#ifndef SAFE_RAOR_COMMON_UTILS_H
#define SAFE_RAOR_COMMON_UTILS_H

#include "safe_raor/safe_raor.h"
#include "eml_safety_enforcer/safety_check_ros_interface.h"
#include "emergency_maneuver_library/multirotor_emergency_maneuver_library.h"
#include "pose_graph_representation/pose_graph_representation.h"
#include "ros/ros.h"
#include "informative_path_planner_utils/grid_creator.h"
#include "visualization_msgs/MarkerArray.h"
#include "safe_raor/safe_connection.h"
#include "ca_nav_msgs/PathXYZVPsi.h"
#include "ca_nav_msgs/PathXYZVViewPoint.h"
#include "csv_reader/csv.h"
#include "ca_nav_msgs/trajectory_converter.h"


typedef ca::SafeRAORViewPointOnPolarGridInterface VGPI;

namespace ca_ri = ca::representation_interface;

double ConstrainAngle(double x);
double AngleDiff(double a,double b);
double ArcLength(Eigen::Vector3d& start, Eigen::Vector3d &end, double radius, Eigen::Vector3d &center);
Eigen::Vector3d StoPose(Eigen::Vector3d& start, Eigen::Vector3d &end, double radius, Eigen::Vector3d &center, double s);
Eigen::Vector3d Pos2Dir(Eigen::Vector3d &pos, Eigen::Vector3d& center, Eigen::Vector3d &path_dir);
ca_nav_msgs::PathXYZVPsi EmlTraj2XYZVPsi( ca::eml::Trajectory &traj);
bool ConcatenatePath(ca_nav_msgs::PathXYZVPsi &path0, ca_nav_msgs::PathXYZVPsi &path1);
bool DeConstructGlobalPlannerOp(ca_nav_msgs::PathXYZVViewPoint& ip_path, ca_nav_msgs::PathXYZVPsi& op_path0, ca_nav_msgs::PathXYZVPsi& op_path2, Eigen::Vector3d& car_pos, double& radius, double &height, double &heading);
visualization_msgs::Marker MakePathMarker(ca::SafeRAORPath &path, std::string frame);
visualization_msgs::Marker MakeVPListMarker(std::vector<ca::SafeRAORViewPoint> &viewpoint_list, std::string frame);
void generate_global_vp_list(VGPI *vg, std::vector<ca::SafeRAORViewPoint> &viewpoint_list, Eigen::Vector2d ori, Eigen::Vector2d x_dir,
                             Eigen::Vector2d y_dir, double x_res, double y_res, int mode, double height, Eigen::Vector3d center);

void generate_global_vp_list_csv(VGPI *vg, std::vector<ca::SafeRAORViewPoint> &viewpoint_list, std::string csv_file,
                                 int mode, Eigen::Vector3d center);


void generate_global_cylinderical_vp_list(VGPI *vg,std::vector<ca::SafeRAORViewPoint> &viewpoint_list,
                                          Eigen::Vector3d radius, Eigen::Vector3d yaw, Eigen::Vector3d z,
                                          Eigen::Vector3d ori);

ca::eml::Trajectory TestPath2Traj(std::vector<ca::SafeRAORViewPoint> &vp_list,
                   std::vector<size_t>& order,
                   double max_speed);
#endif
