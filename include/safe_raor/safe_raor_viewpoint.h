#ifndef SAFE_RAOR_VIEWPOINT_POINT_H_8547
#define SAFE_RAOR_VIEWPOINT_POINT_H_8547
#include "informative_path_planner_utils/viewpoint.h"
namespace ca{
//enum ViewPointType { Car, ViewCar, Explore };

enum SafeRAORViewPointType { SAFETY, VIEW_TARGET, GAIN_INFORMATION };

class SafeRAORViewPoint: public ca::ViewPoint{
public:
    Eigen::Vector3d v_;
    ca::SafeRAORViewPointType  viewpoint_type_;
    void get_pose(Eigen::Vector3d& position, float& fov, float &heading, Eigen::Vector3d& v)
        {position = vp_pose_.position_;
         fov= vp_pose_.fov_;
         heading = vp_pose_.heading_;
         v = v_;
        }
    void set_pose(Eigen::Vector3d& position, float heading, float fov, Eigen::Vector3d& v)
        {ResetViewPoint(position,heading,fov,v);}
    void ResetViewPoint(Eigen::Vector3d& position, float heading, float fov, Eigen::Vector3d &v)
        { ClearVisiblePrimitives(); InitializeViewPoint(position,heading,fov,v);}
    void InitializeViewPoint(Eigen::Vector3d& position, float heading , float fov, Eigen::Vector3d &v)
        { ClearVisiblePrimitives();
          vp_pose_.position_ = position;
            vp_pose_.position_ = position;
          vp_pose_.fov_ = fov;
          vp_pose_.heading_ = heading;
            v_ = v;

        }

    void set_velocity(Eigen::Vector3d &v){ v_ = v;}
    Eigen::Vector3d get_velocity(){ return v_;}

    void set_view_point_type(ca::SafeRAORViewPointType vp_type){ viewpoint_type_ = vp_type;}
    ca::SafeRAORViewPointType get_view_point_type(){ return viewpoint_type_;}

    void ClearVisiblePrimitives(){visible_primitives_.clear(); num_rays_primitives_.clear();}
    SafeRAORViewPoint(std::vector<size_t>& visible_primitives, std::vector<int>& num_rays_primitives)
    {
        visible_primitives_ = visible_primitives;
        num_rays_primitives_ = num_rays_primitives;
        target_set_ = false;
        viewpoint_type_ = SafeRAORViewPointType::SAFETY;
    }
    SafeRAORViewPoint()
    {
        visible_primitives_.clear();
        num_rays_primitives_.clear();
        target_set_ = false;
        viewpoint_type_ = SafeRAORViewPointType::SAFETY;
    }
};
}
#endif
