#ifndef SAFE_CONNECTION_SAFE_RAOR_H
#define SAFE_CONNECTION_SAFE_RAOR_H

#include "eml_safety_enforcer/safety_check_base.h"
#include "safe_raor/safe_raor_viewpoint.h"
#include "safe_raor/controller_utils.h"
#include "safe_raor/common_utils.h"
#include "pose_graph_representation/pose_graph_representation.h"
#include "visualization_msgs/MarkerArray.h"
#include <algorithm>

namespace ca{
 namespace ca_ri = ca::representation_interface;
//
    class SafeConnection
    {
        public:
            ca::SafetyCheckBase sfchk_;
            double sensing_resolution_s_;
            double sensor_range_m_;
            double dynamics_resolution_s_;
            double heading_rate_rad_p_s_;
            double max_z_speed_;
            double max_acceleration_;
            double frame_rate_;
            double a_res_;
            double s_res_;

            double FixedHeadingForOptimizeHeading(ca::eml::Trajectory &traj, size_t i, std::vector<size_t> &fixed_heading_ids, int &next_fixed_wp);

            bool UpdateAngle(double& theta0, double& theta1, double del_t, double heading_rate);
            int GetMaxStartID(std::vector<double> &v_safe_vec);

            bool NextUnsafeState(ca::eml::State &q_unsafe_state, Eigen::Vector3d& current_pos, Eigen::Vector3d &wp0, Eigen::Vector3d &wp1,
                                                     Eigen::Vector3d &center, double radius,double s,
                                                     representation_interface::PoseGraphRepresentation &ri, double desired_speed,
                                                     double max_speed, std::vector<double> &v_max_vec, std::vector<double> &s_vec);
            int EndTurningDistance(double s, double sensor_range, std::vector<double>& s_vec);

            void TrajectoryFromOptOp(ca::eml::Trajectory &traj, Eigen::Vector3d& start_point, double start_heading, Eigen::Vector3d& end_point,
                                     double end_heading, Eigen::Vector3d &center, double radius, std::vector<double> &s_vec, std::vector<double> &v_vec,
                                     std::vector<double> &t_vec, std::vector<double> &a_vec, size_t tp0_id, size_t tp_f);

            bool CreateVSafeVec(representation_interface::PoseGraphRepresentation &ri, std::vector<size_t>& ignore_list, std::vector<double> & s_vec,
                                                    Eigen::Vector3d &start_pos, Eigen::Vector3d &dir, double max_speed, std::vector<double> & v_safe_vec);

            double SafetyBasedTargetHeading(SafeRAORViewPoint &next_vp, Eigen::Vector3d &current_pos, ca::eml::State& unsafe_state,
                                        representation_interface::PoseGraphRepresentation &ri,
                                        double max_speed, double heading_rate);

            ca::eml::State TurningPoint(SafeRAORViewPoint &next_vp, Eigen::Vector3d &current_pos, Eigen::Vector3d &current_vel, ca::eml::State& unsafe_state, eml::Trajectory *traj,
                                               representation_interface::PoseGraphRepresentation &ri,
                                               double max_speed, double heading_rate);

            bool SafeTrajectoryRoute(std::vector<ca::SafeRAORViewPoint> &vp_list,
                                                         std::vector<size_t>& order,
                                                         representation_interface::PoseGraphRepresentation &ri,
                                                         double max_speed, Eigen::Vector3d &path_center,double heading_rate, bool aligned, bool optimize_heading, bool repeat_iteration,
                                                         ca::eml::Trajectory &traj);

            bool OptimizeHeading(ca::eml::Trajectory &traj, std::vector<ca::SafeRAORViewPoint> &vp_list, std::vector<size_t>& order,
                                                     int &traj_idx, int &order_idx, Eigen::Vector3d& safe_position, double nominal_vel, std::vector<size_t> &fixed_heading_id,
                                                     double heading_rate,representation_interface::PoseGraphRepresentation &ri, Eigen::Vector3d &center, double radius);

            Eigen::Vector3d UpdateVelocity(Eigen::Vector3d &p0, Eigen::Vector3d &v0, double theta0, representation_interface::PoseGraphRepresentation &ri,
                                           Eigen::Vector3d &tp, Eigen::Vector3d &v_tp, double theta_tp, double max_speed, bool& valid, double delt);

            double SafeSpeed(Eigen::Vector3d& pos, Eigen::Vector3d& vel, double heading,representation_interface::PoseGraphRepresentation &ri);

            double MinSafeTravelTime(SafeRAORViewPoint &start, SafeRAORViewPoint &end, double max_speed, double heading_rate, Eigen::Vector3d &path_center,
                                            representation_interface::PoseGraphRepresentation &ri, ca::eml::Trajectory *traj, bool aligned, bool reset_sensing);


            bool IsPoseFeasible();

            bool ForwardSimulate(Eigen::Vector3d& pos, Eigen::Vector3d& v0, Eigen::Vector3d& v1, double &theta0, double &theta1, double del_t);

            bool InterpolatePos(Eigen::Vector3d& pos1, Eigen::Vector3d &pos2, double time1, double time2,
                                double q_time, Eigen::Vector3d &ret_pos);
            bool InterpolateHeading(double heading1, double heading2, double t1, double t2, double q_time,
                                    double &ret_heading);

            void CacheRepresentationInterface(representation_interface::PoseGraphRepresentation &ri, Eigen::Vector3d &start_pos, Eigen::Vector3d &end_pos,
                                              std::vector<double> &s_vec, std::vector<size_t> &ri_index_vec){
                ri_index_vec.resize(s_vec.size(),0);
            } //Todo

            Eigen::Vector3d UpdateVelocityBangBangWithEMLandHeadingRate(Eigen::Vector3d &p0, Eigen::Vector3d &v0, double theta0,representation_interface::PoseGraphRepresentation &ri,
                                           Eigen::Vector3d &tp, Eigen::Vector3d &v_tp, double theta_tp,double max_speed, bool& valid, double delt);

            double ConstrainAngle(double x){
                x = fmod(x + M_PI,2*M_PI);
                if (x < 0)
                    x += 2*M_PI;
                return x - M_PI;
            }
            double AngleDiff(double a,double b){
                double dif = fmod(b - a + M_PI,2*M_PI);
                if (dif < 0)
                    dif += 2*M_PI;
                return dif - M_PI;
            }

            visualization_msgs::MarkerArray DisplayRepresentationandSafety(representation_interface::PoseGraphRepresentation &ri,
                                           ca::eml::Trajectory &traj, size_t id);

            SafeConnection(){}
            ~SafeConnection(){}
    };

}
#endif
