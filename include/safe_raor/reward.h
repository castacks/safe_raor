#ifndef SAFE_RAOR_REWARD_H
#define SAFE_RAOR_REWARD_H
#include "safe_raor/safe_raor_viewpoint.h"
#include "representation_interface/representation_interface.h"

namespace ca{
namespace ca_ri = ca::representation_interface;
class Reward{
public:
 int max_rays_;// this is 20
 int max_p_; // this is 20
 double exp_base_;// this is 2
 double target_reward_;
 double weight_;

 double RewardFunction(size_t new_nodeId,std::vector<SafeRAORViewPoint>& global_vp_list ,ca_ri::RepresentationInterface<double,3> *ri);
 double RewardFunction(ca_ri::RepresentationInterface<double,3> *old_ri,ca_ri::RepresentationInterface<double,3> *new_ri);
 Reward(){
     max_rays_ = 20;
     max_p_ = 20;
     exp_base_ = 2;
 }
};
}
#endif
