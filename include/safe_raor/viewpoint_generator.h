#ifndef SAFE_RAOR_VIEWPOINT_GEN_H
#define SAFE_RAOR_VIEWPOINT_GEN_H
#include "safe_raor/safe_raor_viewpoint.h"
#include "boost/bind.hpp"
#include "polar_2d_map_representation/polar_2d_map_representation.h"

// the new function will only return what primitives are visible
namespace ca{
namespace ca_ri= ca::representation_interface;
class SafeRAORViewPointOnPolarGridInterface{
public:
    ca_ri::Polar_2D_Representation* polar_rep_;
    std::vector<size_t> return_list_;
    double pitch_angle_;

    std::vector<Eigen::Vector3d> cached_rays_;


    bool initialized_;

    bool MakeViewPoint(ca::SafeRAORViewPoint &vp , Eigen::Vector3d &pos, Eigen::Vector3d &target, double fov, double range);
    bool MakeViewPoint(ca::SafeRAORViewPoint &vp ,Eigen::Vector3d &pos, double heading, double fov, double range);

    SafeRAORViewPointOnPolarGridInterface(){
        initialized_ = false;
    }

    bool Initialize(ca_ri::Polar_2D_Representation* polar_rep, double pitch_angle);
};
}
#endif
