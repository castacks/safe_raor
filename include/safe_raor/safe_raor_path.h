#ifndef SAFE_RAOR_PATH_GEN_H
#define SAFE_RAOR_PATH_GEN_H
//#include "informative_path_planner_utils/viewpoint_generator.h"
#include "safe_raor/safe_raor_viewpoint.h"
#include "polar_2d_map_representation/polar_2d_map_representation.h"
namespace ca{
namespace ca_ri = ca::representation_interface;
class SafeRAORPath
{
public:
    std::vector<SafeRAORViewPoint>& global_vp_list_;
    std::vector<size_t> path_;
    std::vector<size_t> sorted_path_;
    double path_length_;
    double path_reward_;
    ca_ri::RepresentationInterface<double,3>* ri_;
    ca_ri::Polar_2D_Representation polar_interface_;
    bool rep_state_initialized_;
    bool representation_updated_;
    bool IsInPath(size_t q, size_t& id);
    double CalculatePathLength(std::vector<size_t>&path, std::vector<SafeRAORViewPoint>& global_vp_list);
    double DistanceFromPath(std::vector<size_t>&path, std::vector<SafeRAORViewPoint>& global_vp_list, Eigen::Vector3d& p);
    double DistanceFromLineSegment(Eigen::Vector3d p1, Eigen::Vector3d p2, Eigen::Vector3d p);
    double CostOfAdding(std::vector<size_t>&path, std::vector<SafeRAORViewPoint>& global_vp_list, Eigen::Vector3d& p);
    bool AddNode(size_t node, bool update_representation);
    bool DeleteNode(size_t node, bool update_representation);
    bool UpdateRepresentation();
    bool AddNodes(std::vector<size_t> ids, bool representation_update);
    bool ResetOrder(std::vector<size_t> &new_order);
    void set_path_reward(double path_reward){ path_reward_ = path_reward; }
    double get_path_reward(){return path_reward_;}
    void setRepresentationState(ca_ri::Polar_2D_Representation* ri)
    {
       polar_interface_ = *ri;
       ri_ = &polar_interface_;
       rep_state_initialized_ = true;
    }

    SafeRAORPath(std::vector<SafeRAORViewPoint>& global_vp_list):
        global_vp_list_(global_vp_list)
    {
        representation_updated_ = false;
        rep_state_initialized_ = false;
    }

    ~SafeRAORPath(){
        //delete ri_;
    }

    SafeRAORPath(const SafeRAORPath& a);
    SafeRAORPath& operator=(SafeRAORPath& a);
};
}
#endif
