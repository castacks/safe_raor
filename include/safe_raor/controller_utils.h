#ifndef SAFE_RAOR_CONTROLLER_UTILS_H
#define SAFE_RAOR_CONTROLLER_UTILS_H
#include <Eigen/Dense>
#include <vector>
#include <algorithm>
#include <ros/ros.h>
#include "plot_tool/PlotPath.h"

void DisplayVectors(std::vector<double> & s_vec, std::vector<double> & v_vec, std::string  disp_string);
void BackProjectVelocityAtAcc(std::vector<double>& s_vec, double a, double vf, double v_max,std::vector<double>& v_back_vec);
void BackProjectVelocityAtAccMaxTime(std::vector<double>& s_vec, double a, double vf, std::vector<double>& v_back_vec);
void ForwardProjectVelocityAtAccMaxTime(std::vector<double>& s_vec, double a, double v0, std::vector<double>& v_fwd_vec);
void ForwardProjectVelocityAtAcc(std::vector<double>& s_vec, double a, double v0, double v_max, std::vector<double>& v_fwd_vec);
void TimeAcceleration(std::vector<double> &s_vec, std::vector<double> &v_vec, std::vector<double>& t_vec, std::vector<double>& a_vec);
int BangBangDoubleIntegrator(double s, double v0, double vf, double a_max, double v_max, double s_res, double a_res, double min_time,
                              std::vector<double>& t_vec, std::vector<double>& a_vec, std::vector<double>& v_vec, std::vector<double>& s_vec, bool strict_min_time = true);
int BangBangDoubleMaxTimeIntegrator(double s, double v0, double vf, double a_max, double v_max, double s_res, double a_res, double min_time,
                              std::vector<double>& t_vec, std::vector<double>& a_vec, std::vector<double>& v_vec, std::vector<double>& s_vec);
bool Vector2Path(std::vector<double>& x, std::vector<double>& y, nav_msgs::Path &path);
bool MergeTimeVectors(std::vector<double>& t_vec, std::vector<double>& t_vec_tp0, std::vector<double>& t_vec_tp1, std::vector<double>& t_vec_f);
bool MergeDistVectors(std::vector<double>& s_vec, std::vector<double>& s_vec_tp0, std::vector<double>& s_vec_tp1, std::vector<double>& s_vec_f);
bool MergeAccVectors(std::vector<double>& a_vec, std::vector<double>& a_vec_tp0, std::vector<double>& a_vec_tp1, std::vector<double>& a_vec_f );
bool MergeVelVectors(std::vector<double>& v_vec, std::vector<double>& v_vec_tp0, std::vector<double>& v_vec_tp1, std::vector<double>& v_vec_f );
// Here is the problem.
// there is a path with defined start and end configs and with a max speed
// for the connection to be safe independently, it has to be able to turn in the direction of the path.
// we give 2 turning points and the function return minimum time path
bool SafeConnect(double s, double v0, double min_time_tp0, double s_tp0, double min_time_f, double s_tp1,
                 double vf, double a_max, double v_max, double s_res, double a_res, std::vector<double>& t_vec,
                 std::vector<double>& a_vec, std::vector<double>& v_vec, std::vector<double>& s_vec, std::vector<double> &v_safe_vec);

void GenerateSVec(std::vector<double> &s_vec,double s_res, double s);
void checkMerging();
#endif
