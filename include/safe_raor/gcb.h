#ifndef SAFE_RAOR_GCB_H
#define SAFE_RAOR_GCB_H

#include "safe_raor/viewpoint_generator.h"
#include "safe_raor/safe_raor_viewpoint.h"
#include "safe_raor/safe_raor_path.h"
#include "safe_raor/reward.h"
#include "tsp/tsp.h"
#include "visualization_msgs/MarkerArray.h"

namespace ca{
namespace ca_ri = ca::representation_interface;
    class GeneralizedCostBenefit
    {
        public:
            std::vector<SafeRAORViewPoint>& global_vp_list_;
            SafeRAORViewPointOnPolarGridInterface* vg_;
            SafeRAORPath path_;
            double budget_;
            double target_radius_;
            size_t start_location_id_;
            size_t end_node_id_;
            SafeRAORPath GCB(Eigen::Vector3d& start_point, double start_heading, double start_fov,
                    size_t end_node_id, ca_ri::Polar_2D_Representation* ri, double budget);
            Reward reward_;
            Eigen::MatrixXd DMatrix(std::vector<size_t> &points, int start_id, int end_id);
            std::vector<size_t> FixOrder(std::vector<size_t> &order, int start_id, int end_id, int fake_id);
            //bool AddNode(ca::ViewPoint &vp);
            // Distance matrix
            // TSP solver
            TSP tsp_;
            visualization_msgs::MarkerArray GetMarkers(ca_ri::Polar_2D_Representation* ri);
            void SetupAndSolveTsp(std::vector<size_t> &node_order,size_t start_location_id, size_t end_node_id);
            GeneralizedCostBenefit(SafeRAORViewPointOnPolarGridInterface* vg,std::vector<SafeRAORViewPoint> &vp_list);
    };
}
#endif
