#ifndef SAFE_RAOR_RAOR_H
#define SAFE_RAOR_RAOR_H

#include "safe_raor/viewpoint_generator.h"
#include "safe_raor/safe_raor_viewpoint.h"
#include "safe_raor/safe_raor_path.h"
#include "safe_raor/reward.h"
#include "tsp/tsp.h"
#include "visualization_msgs/MarkerArray.h"
#include <cstdlib>

namespace ca{
namespace ca_ri = ca::representation_interface;
    class SafeRAOR
    {
        public:
            std::vector<SafeRAORViewPoint>& global_vp_list_;
            SafeRAORViewPointOnPolarGridInterface* vg_;
            std::vector<SafeRAORPath> path_vec_;
            ca::SafeRAORPath sampled_path_;
            ca::SafeRAORPath best_path_;
            double budget_;
            size_t start_location_id_;
            size_t end_node_id_;
            bool just_csp_;
            double runtime_;
            double inner_runtime_;
            bool runtime_visualization_;
            SafeRAORPath Run(Eigen::Vector3d& start_point, double start_heading, double start_fov,
                    size_t end_node_id, ca_ri::Polar_2D_Representation* ri, double budget);
            Reward reward_;
            Eigen::MatrixXd DMatrix(std::vector<size_t> &points, int start_id, int end_id);
            std::vector<size_t> FixOrder(std::vector<size_t> &order, int start_id, int end_id, int fake_id);
            size_t SampleNode(size_t max_num);
            std::vector<size_t> SampleRandomRoute(size_t max_num);
            bool FlipBelongingness(size_t node, SafeRAORPath &path);
            bool IsInRouteList(std::vector<size_t> &q_route);
            bool IsInRoute(size_t q_id,std::vector<size_t> &q_route){
                std::vector<size_t>::iterator it;
                  it = find (q_route.begin(), q_route.end(), q_id);
                  if (it != q_route.end())
                      return true;
                  else
                      return false;
            }

            void AddNodetoRouteList(size_t sampled_node, double budget, ca_ri::Polar_2D_Representation* ri, int &new_route_id, int &changed_route_id);
            void AddRoutetoRouteList(ca::SafeRAORPath &path);
            ros::Publisher visualization_pub_;
            //bool AddNode(ca::ViewPoint &vp);
            // Distance matrix
            // TSP solver
            TSP tsp_;
            visualization_msgs::MarkerArray GetMarkers(ca_ri::Polar_2D_Representation* ri);
            visualization_msgs::MarkerArray GetGraphRuntimeMarkers(representation_interface::Polar_2D_Representation *ri, size_t sampled_node, size_t new_route_id,
                                                                   size_t best_route_id, size_t changed_route_id);
            void SetupAndSolveTsp(ca::SafeRAORPath &path,std::vector<size_t> &node_order,size_t start_location_id, size_t end_node_id);
            void Initialize(ros::NodeHandle &n){
                 visualization_pub_ = n.advertise<visualization_msgs::MarkerArray>("planner_viz", 1000);
            }

            SafeRAOR(SafeRAORViewPointOnPolarGridInterface* vg,std::vector<SafeRAORViewPoint> &vp_list);
    };
}
#endif
