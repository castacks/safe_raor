#ifndef RAOR_RAOR_H
#define RAOR_RAOR_H

#include "informative_path_planner_utils/viewpoint_generator.h"
#include "informative_path_planner_utils/viewpoint.h"
#include "informative_path_planner_utils/path.h"
#include "informative_path_planner_utils/reward.h"
#include "tsp/tsp.h"
#include "visualization_msgs/MarkerArray.h"
#include <cstdlib>
#include "semantic_grid/semantic_map_datatypes.h"


namespace ca{
//namespace ca_ri = ca::representation_interface;
    class RAOR
    {
        public:
            std::vector<ViewPoint>& global_vp_list_;
            ViewPointGenerator* vg_;
            std::vector<Path> path_vec_;
            ca::Path sampled_path_;
            ca::Path best_path_;
            double budget_;
            size_t start_location_id_;
            size_t end_node_id_;
            bool just_csp_;
            double runtime_;
            double inner_runtime_;
            bool runtime_visualization_;
            std::string frame_;
            Path& Run(Eigen::Vector3d& start_point, double start_heading, double start_fov,
                    size_t end_node_id, FixedGrid2f *grid2, ca::SemanticGridCell* grid_data, double budget);
            Reward reward_;
            Eigen::MatrixXd DMatrix(std::vector<size_t> &points, int start_id, int end_id);
            std::vector<size_t> FixOrder(std::vector<size_t> &order, int start_id, int end_id, int fake_id);
            size_t SampleNode(size_t max_num);
            std::vector<size_t> SampleRandomRoute(size_t max_num);
            bool FlipBelongingness(size_t node, Path &path);
            bool IsInRouteList(std::vector<size_t> &q_route);
            bool IsInRoute(size_t q_id,std::vector<size_t> &q_route){
                std::vector<size_t>::iterator it;
                  it = find (q_route.begin(), q_route.end(), q_id);
                  if (it != q_route.end())
                      return true;
                  else
                      return false;
            }

            void AddNodetoRouteList(size_t sampled_node, double budget,
                                              int& new_route_id, int& changed_route_id, FixedGrid2f *grid2,ca::SemanticGridCell* ip_rep);
            void AddRoutetoRouteList(ca::Path &path);
            ros::Publisher visualization_pub_;
            //bool AddNode(ca::ViewPoint &vp);
            // Distance matrix
            // TSP solver
            TSP tsp_;
            visualization_msgs::MarkerArray GetMarkers();
            visualization_msgs::MarkerArray GetGraphRuntimeMarkers(size_t sampled_node, size_t new_route_id,
                                                                   size_t best_route_id, size_t changed_route_id);
            void SetupAndSolveTsp(ca::Path &path,std::vector<size_t> &node_order,size_t start_location_id, size_t end_node_id);
            void Initialize(ros::NodeHandle &n){
                 visualization_pub_ = n.advertise<visualization_msgs::MarkerArray>("planner_viz", 1000);
            }

            RAOR(ViewPointGenerator* vg,std::vector<ViewPoint> &vp_list);
    };
}
#endif
